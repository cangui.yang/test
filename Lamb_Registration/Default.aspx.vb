﻿
Partial Class _Default
    Inherits System.Web.UI.Page

    Dim runQuery As New SQLAccess
    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        returnToLairageLive(CInt(TextBox_BatchNo.Text))
        '  ReturnLastLotToLairage(TextBox_LotNo.Text, , TextBox_EMPLID.Text, TextBox_EMPLineID.Text)
    End Sub

    Private Function returnToLairageLive(ByVal intBatchno As Integer) As Integer

        Dim ws_Generic As New WS_SheepReturnToLairage.GenericInput
        Dim ws_ws_RTLairage_InfoInput As New WS_SheepReturnToLairage.SheepReturnToLairageInput
        Dim ws_ws_RTLairage_InfoOutput As New WS_SheepReturnToLairage.SheepReturnToLairageOutput
        Dim ws_RTLairage As New WS_SheepReturnToLairage.WS_SheepReturnToLairagePortTypeClient


        ws_Generic.callingLocation = "Meat"
        ws_Generic.channel = "Meat"
        'ws_Generic.clntRefNo_BusId = 993336
        ws_Generic.clntRefNo_BusId = 922225
        ws_Generic.environment = "Live"
        ws_Generic.herdNo = "A04001"
        ws_Generic.password = ""
        ws_Generic.username = "LINS01"

        ws_ws_RTLairage_InfoInput.genericInput = ws_Generic
        ws_ws_RTLairage_InfoInput.pinNo = "003"
        ws_ws_RTLairage_InfoInput.batchNo = intBatchno

        ws_ws_RTLairage_InfoOutput = ws_RTLairage.WS_SheepReturnToLairage(ws_ws_RTLairage_InfoInput)

        If ws_ws_RTLairage_InfoOutput.success = 1 Then

            Return ws_ws_RTLairage_InfoOutput.success

        Else
            MsgBox("Error: " & ws_ws_RTLairage_InfoOutput.error & " ErrorCode: " & ws_ws_RTLairage_InfoOutput.errorCode, "alert alert-danger")
            Return ws_ws_RTLairage_InfoOutput.success
        End If

    End Function

    Sub ReturnLastLotToLairage(ByVal intLotNo As Integer, ByVal intBatchno As Integer, ByVal intEMPLID As Integer, ByVal intEMPLLineID As Integer)

        Dim intResultMoveToAbb As Integer
        'Comment out to verify that no animals have been killed

        intResultMoveToAbb = returnToLairageLive(intBatchno)
        If intResultMoveToAbb = 0 Then
            MsgBox("Return to Lairage Failed. Change Batch Failed.", "alert alert-warning")
            Exit Sub
        End If

        'Updates the EMPL_Line that the batch is now back in the lairage

        runQuery.executeQuery("UPDATE EMPL_Line SET Lotno = 0, MoveToAbb =0 , BATCHTime = NULL, AssignStatus = 0,slaughterqty = 0 WHERE EMPLID = " & intEMPLID & " AND EMPLLineID = " & intEMPLLineID, "AMPSLindenLambConnectionString")
        runQuery.executeQuery("INSERT INTO EMPL_Return_Log (EMPLID, EMPLLineID, Lotno) VALUES (" & intEMPLID & ", " & intEMPLLineID & ", " & intLotNo & ")", "AMPSLindenLambConnectionString")
        MsgBox("Return to Lairage Successfull", "alert alert-success")


    End Sub
End Class
