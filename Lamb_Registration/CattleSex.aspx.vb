﻿Imports System.Data.SqlClient
Partial Class CattleSex
    Inherits System.Web.UI.Page

    <System.Web.Script.Services.ScriptMethod(), System.Web.Services.WebMethod()>
    Public Shared Function SearchEarch(ByVal prefixText As String, ByVal count As Integer) As List(Of String)
        Dim EarTag As List(Of String) = New List(Of String)

        Using cmd As SqlCommand = New SqlCommand("Select top 10 eartag from Killtrans where TransDate>getdate()-2 and replace(eartag,' ','') like '%" & prefixText.Replace(" ", "") & "%'", New SqlConnection(ConfigurationManager.ConnectionStrings("PrimalAmpsBeefConnectionString").ToString))
            cmd.Connection.Open()
            Using reader As SqlDataReader = cmd.ExecuteReader
                While reader.Read
                    EarTag.Add(reader("eartag").ToString)
                End While
            End Using

            cmd.Connection.Close()
            cmd.Dispose()
        End Using



        Return EarTag
    End Function
    Protected Sub TextBox_Eartag_part_TextChanged(sender As Object, e As EventArgs) Handles TextBox_Eartag_part.TextChanged
        If TextBox_Eartag_part.Text.Length >= 7 Then
            Reset()
            Using cmd As SqlCommand = New SqlCommand("Select eartag, convert(int,Months) as Months, convert(varchar(10),TransDate,103) AS TransDate,isnull(CountryOrigin, 'N IRE') as CountryOrigin from Killtrans where TransDate>getdate()-2 and replace(eartag,' ','') like '%" & TextBox_Eartag_part.Text.Replace(" ", "") & "%'", New SqlConnection(ConfigurationManager.ConnectionStrings("PrimalAmpsBeefConnectionString").ToString))
                cmd.Connection.Open()
                Using reader As SqlDataReader = cmd.ExecuteReader
                    While reader.Read
                        '  EarTag.Add(reader("eartag").ToString)
                        Label_Eartag.Text = reader("eartag").ToString
                        Label_Age.Text = reader("Months").ToString
                        Label_DateBookedIn.Text = reader("TransDate")
                        Label_CountryOfOrigin.Text = reader("CountryOrigin").ToString
                    End While
                End Using

                cmd.Connection.Close()
                cmd.Dispose()
            End Using

        End If
    End Sub

    Protected Sub Button_Bull_Click(sender As Object, e As EventArgs) Handles Button_Bull.Click
        Dim age As Integer = 0
        Integer.TryParse(Label_Age.Text, age)
        If age > 0 Then
            Select Case age
                Case 1 To 11
                    'SET AS A as cant put veal through kill line yet.....
                    UpdateCattleSex("A")
                Case 12 To 23
                    UpdateCattleSex("A")
                Case Else
                    UpdateCattleSex("B")
            End Select
        Else
            ShowAlertMessage("Age is not in a correct format!")
        End If

    End Sub
    Protected Sub Button_Steer_Click(sender As Object, e As EventArgs) Handles Button_Steer.Click
        UpdateCattleSex("C")
    End Sub
    Protected Sub Button_Cow_Click(sender As Object, e As EventArgs) Handles Button_Cow.Click
        UpdateCattleSex("D")
    End Sub
    Protected Sub Button_Heifer_Click(sender As Object, e As EventArgs) Handles Button_Heifer.Click
        UpdateCattleSex("E")
    End Sub
    Sub UpdateCattleSex(ByVal sex As String)
        Try
            Dim runQuery As New SQLAccess
            runQuery.executeQuery("UPDATE Killtrans SET Sex ='" & sex & "' where EarTag='" & Label_Eartag.Text & "' and convert(varchar(10),TransDate,103)='" & Label_DateBookedIn.Text & "'", "AMPSLindenLambConnectionString")
            Dim temstr As String = ""
            Select Case sex
                Case "A" Or "B"
                    temstr = "BULL"
                Case "C"
                    temstr = "STEER"
                Case "D"
                    temstr = "COW"
                Case "E"
                    temstr = "HEIFER"
            End Select
            runQuery.executeQuery("Insert into tbl_KillFloor_SexChange_Log (Eartag,LairageSex) values ('" & Label_Eartag.Text & "','" & sex & "')"， "AMPSLindenLambConnectionString")


            ShowAlertMessage("Sex for selected animal '" & Label_Eartag.Text & "' is set to " & temstr)
        Catch ex As Exception
            ShowAlertMessage(ex.Message)
        End Try
    End Sub
    Sub ShowAlertMessage(ByVal Msg As String)
        Label_AlertMessage.Text = Msg


        UpdatePanel_Alert.Update()
        '      ScriptManager.RegisterStartupScript(Button_Cow, GetType(Page), "UpdateDiv", "UpdateAlertClass('" & messageType & "');", True)

        ScriptManager.RegisterStartupScript(Button_Cow, GetType(Page), "showMyModal", "ShowModal('#myModal_AlertMessage');", True)
    End Sub
    Sub Reset()

        Label_Eartag.Text = String.Empty
        Label_Age.Text = String.Empty
        Label_DateBookedIn.Text = String.Empty
        Label_CountryOfOrigin.Text = String.Empty
    End Sub
    Protected Sub Button_Update_Compeleted_Click(sender As Object, e As EventArgs) Handles Button_Update_Compeleted.Click
        Reset()
        TextBox_Eartag_part.Text = String.Empty
        UpdatePanel_SupplierDetails.Update()

    End Sub

End Class
