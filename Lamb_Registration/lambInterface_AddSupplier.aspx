﻿<%@ Page Title="" Language="VB" MasterPageFile="~/LambInterface.master" AutoEventWireup="false" CodeFile="lambInterface_AddSupplier.aspx.vb" Inherits="lambInterface_AddSupplier"  EnableEventValidation="false" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script>
           var controlIDAcceptValue;
                                    function openKeyBoard(evt, Name) {
                                      var i, x, tablinks;
                                      x = document.getElementsByClassName("KeyBoard");
                                      for (i = 0; i < x.length; i++) {
                                         x[i].style.display = "none";
                                      }
                                      tablinks = document.getElementsByClassName("tablink");
                                      for (i = 0; i < x.length; i++) {
                                         tablinks[i].className = tablinks[i].className.replace(" w3-border-red", "");
                                      }
                                      document.getElementById(Name).style.display = "block";
                                      evt.currentTarget.firstElementChild.className += " w3-border-red";
                                    }

                                    function openInformationPanel(evt,panel ) {
                                        var i, x, tablinks;
                                        x = document.getElementsByClassName("informationPanel");
                                        for (i = 0; i < x.length; i++) {
                                            x[i].style.display = "none";
                                        }

                                        document.getElementById(panel).style.display = "block";
                   
                                    }
                                    function KeyPaysetValue(val) {

                                        try {

                                            document.getElementById(controlIDAcceptValue).value = document.getElementById(controlIDAcceptValue).value + val;
                                            document.getElementById(controlIDAcceptValue).focus();
                                        }
                                        catch (err) {
                                            $('#myModal_AlertMessage').show();
                                            //   alert("Please set focus on text box to accept key pad value.");
                                           // document.getElementById('<'%=Label_AlertMessage.ClientID %>').innerText = " Please set focus on text box to accept key pad value."
                                        }

                                    }
                                    function setFocusID(id) {
                                        controlIDAcceptValue = id;
                                    }
                                    function ketPayDelete() {
                                        var currentValue = document.getElementById(controlIDAcceptValue).value;
                                        currentValue = currentValue.substring(0, currentValue.length - 1);
                                        document.getElementById(controlIDAcceptValue).value = currentValue;
                                    }
                                
        </script>

      <style>
    .KeyBoard {display:none;}
        .KeyBoardStyle {
              width:65px;
              height:52px;
              margin-bottom:5px;
              font-size:large;
        }
        .KeyBoardStyle_Alphabet {
              width:50px;
              height:45px;
              font-size:large;
        }
        </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
      <div class="container" style="width:980px; height:500px">
        <div class="row">
            <div class="col-md-8">
                <div id="NewSupplier" class="informationPanel" style="display:block">
                    <asp:UpdatePanel ID="UpdatePanel_SupplierDetails" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>                     
                        <asp:FormView ID="FormView_Supplier" runat="server" DataKeyNames="Supplier" DataSourceID="SqlDataSource_NewSupplier" DefaultMode="Insert" Width="100%">
                            <EditItemTemplate>
                                <asp:Label ID="SupplierLabel1" runat="server" Text='<%# Eval("Supplier") %>' Visible="false" />
                                <table class="table">
                            <tr>
                                <td>Flock No.:</td>
                                <td class="form-inline">
                                    <asp:TextBox ID="TextBox_FlockNo" CssClass="form-control" placeholder="Flock/Herd No."  onClick="setFocusID(this.id)" runat="server" Text='<%# Bind("HerdNo") %>'  ></asp:TextBox>&nbsp;<asp:Button ID="Button10" CssClass="btn btn-info" runat="server" Text="Flock" OnClientClick="openInformationPanel(event,'SupplierSearch');return false;" />
                                    
                                </td>
                            </tr>
                            <tr>
                                <td>Forename:</td>
                                <td class="form-inline">
                                    <asp:TextBox ID="TextBox_ForeName" CssClass="form-control" placeholder="Frist Name"  onClick="setFocusID(this.id)" runat="server" Text='<%# Bind("NAME") %>' ></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>Surname:</td>
                                <td class="form-inline">
                                    <asp:TextBox ID="TextBox_SurName" CssClass="form-control" placeholder="Last Name"  onClick="setFocusID(this.id)" runat="server" Text='<%# Bind("ADMIN_NAME") %>'></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>Address:</td>
                                <td>
                                    <asp:TextBox ID="TextBox_Address1" CssClass="form-control" placeholder="Address 1"   onClick="setFocusID(this.id)" runat="server"  Text='<%# Bind("ADDRESS1") %>'></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>
                                    <asp:TextBox ID="TextBox_Address2" CssClass="form-control" placeholder="Address 2"  onClick="setFocusID(this.id)" runat="server"  Text='<%# Bind("ADDRESS2") %>'></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>
                                    <asp:TextBox ID="TextBox_Address3" CssClass="form-control" placeholder="Address 3"  onClick="setFocusID(this.id)" runat="server" Text='<%# Bind("ADDRESS3") %>'></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>
                                    <asp:TextBox ID="TextBox_Address4" CssClass="form-control"  onClick="setFocusID(this.id)" placeholder="Address 4" runat="server"  Text='<%# Bind("ADDRESS4") %>'></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>Post Code:</td>
                                <td>
                                    <asp:TextBox ID="TextBox_PostCode" CssClass="form-control"   onClick="setFocusID(this.id)" placeholder="Post Code" runat="server" Text='<%# Bind("ADDINFO") %>' ></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>Phone:</td>
                                <td>
                                    <asp:TextBox ID="TextBox_Phone" CssClass="form-control"  onClick="setFocusID(this.id)" placeholder="Phone"  runat="server"  Text='<%# Bind("PHONE") %>' ></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>Email:</td>
                                <td>
                                    <asp:TextBox ID="TextBox_Email" CssClass="form-control"  onClick="setFocusID(this.id)" placeholder="Email address"  runat="server" Text='<%# Bind("EmailAddress") %>'></asp:TextBox>
                                </td>
                            </tr>
                        </table>               
                                <asp:Button ID="Button18" runat="server" Text="Update" CausesValidation="True" CommandName="Update" CssClass="btn btn-danger btn-lg" />
                                <asp:Button ID="Button20" runat="server" Text="Cancel" CausesValidation="False" CommandName="Cancel" CssClass="btn btn-info btn-lg" />
<%--                                <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update" />
                                &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" />--%>
                            </EditItemTemplate>
                            <InsertItemTemplate>            
                             <table class="table">
                            <tr>
                                <td>Flock No.:</td>
                                <td class="form-inline">
                                    <asp:TextBox ID="TextBox_FlockNo" CssClass="form-control" placeholder="Flock/Herd No."  onClick="setFocusID(this.id)" runat="server" Text='<%# Bind("HerdNo") %>'  ></asp:TextBox>&nbsp;<asp:Button ID="Button10" CssClass="btn btn-info" runat="server" Text="Flock" OnClientClick="openInformationPanel(event,'SupplierSearch');return false;" />
                                    
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*" ControlToValidate="TextBox_FlockNo" ForeColor="Red"></asp:RequiredFieldValidator>
                                    
                                </td>
                            </tr>
                            <tr>
                                <td>Forename:</td>
                                <td class="form-inline">
                                    <asp:TextBox ID="TextBox_ForeName" CssClass="form-control" placeholder="Frist Name"  onClick="setFocusID(this.id)" runat="server" Text='<%# Bind("NAME") %>' ></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TextBox_ForeName" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>Surname:</td>
                                <td class="form-inline">
                                    <asp:TextBox ID="TextBox_SurName" CssClass="form-control" placeholder="Last Name"  onClick="setFocusID(this.id)" runat="server" Text='<%# Bind("ADMIN_NAME") %>'></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="TextBox_SurName" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>Address:</td>
                                <td>
                                    <asp:TextBox ID="TextBox_Address1" CssClass="form-control" placeholder="Address 1"   onClick="setFocusID(this.id)" runat="server"  Text='<%# Bind("ADDRESS1") %>'></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="TextBox_Address1" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>
                                    <asp:TextBox ID="TextBox_Address2" CssClass="form-control" placeholder="Address 2"  onClick="setFocusID(this.id)" runat="server"  Text='<%# Bind("ADDRESS2") %>'></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>
                                    <asp:TextBox ID="TextBox_Address3" CssClass="form-control" placeholder="Address 3"  onClick="setFocusID(this.id)" runat="server" Text='<%# Bind("ADDRESS3") %>'></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>
                                    <asp:TextBox ID="TextBox_Address4" CssClass="form-control"  onClick="setFocusID(this.id)" placeholder="Address 4" runat="server"  Text='<%# Bind("ADDRESS4") %>'></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>Post Code:</td>
                                <td>
                                    <asp:TextBox ID="TextBox_PostCode" CssClass="form-control"   onClick="setFocusID(this.id)" placeholder="Post Code" runat="server" Text='<%# Bind("ADDINFO") %>' ></asp:TextBox>
                                </td>
                            </tr>
                               <tr>
                                <td>Country:</td>
                                <td>
                                    <asp:TextBox ID="TextBox_Country" CssClass="form-control"   onClick="setFocusID(this.id)" placeholder="Country" runat="server" Text='<%# Bind("Country") %>' ></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="TextBox_Country" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>Phone:</td>
                                <td>
                                    <asp:TextBox ID="TextBox_Phone" CssClass="form-control"  onClick="setFocusID(this.id)" placeholder="Phone"  runat="server"  Text='<%# Bind("PHONE") %>' ></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>Email:</td>
                                <td>
                                    <asp:TextBox ID="TextBox_Email" CssClass="form-control"  onClick="setFocusID(this.id)" placeholder="Email address"  runat="server" Text='<%# Bind("EmailAddress") %>'></asp:TextBox>
                                </td>
                            </tr>
                                   <tr>
                                <td>Dest:</td>
                                <td>
                                    <asp:TextBox ID="TextBox_Dest" CssClass="form-control"  onClick="setFocusID(this.id)" placeholder="Email address"  runat="server" Text='<%# Bind("EmailAddress") %>'></asp:TextBox>
                                </td>
                            </tr>
                        </table>               
                                <asp:Button ID="Button48" runat="server" Text="Insert" CommandName="Insert" CausesValidation="True" CssClass="btn btn-primary btn-lg" />
                <%--                <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert" />
                                 &nbsp;<asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" />--%>
                            </InsertItemTemplate>                          
                        </asp:FormView>
                           
                        <asp:SqlDataSource ID="SqlDataSource_NewSupplier" runat="server" ConflictDetection="CompareAllValues" ConnectionString="<%$ ConnectionStrings:AMPSLindenLambConnectionString %>" DeleteCommand="DELETE FROM [SuppLier] WHERE [Supplier] = @original_Supplier AND (([NAME] = @original_NAME) OR ([NAME] IS NULL AND @original_NAME IS NULL)) AND (([ADDRESS1] = @original_ADDRESS1) OR ([ADDRESS1] IS NULL AND @original_ADDRESS1 IS NULL)) AND (([ADDRESS2] = @original_ADDRESS2) OR ([ADDRESS2] IS NULL AND @original_ADDRESS2 IS NULL)) AND (([ADDRESS3] = @original_ADDRESS3) OR ([ADDRESS3] IS NULL AND @original_ADDRESS3 IS NULL)) AND (([ADDRESS4] = @original_ADDRESS4) OR ([ADDRESS4] IS NULL AND @original_ADDRESS4 IS NULL)) AND (([ADMIN_NAME] = @original_ADMIN_NAME) OR ([ADMIN_NAME] IS NULL AND @original_ADMIN_NAME IS NULL)) AND (([ADDINFO] = @original_ADDINFO) OR ([ADDINFO] IS NULL AND @original_ADDINFO IS NULL)) AND (([EmailAddress] = @original_EmailAddress) OR ([EmailAddress] IS NULL AND @original_EmailAddress IS NULL)) AND (([HerdNo] = @original_HerdNo) OR ([HerdNo] IS NULL AND @original_HerdNo IS NULL)) AND (([PHONE] = @original_PHONE) OR ([PHONE] IS NULL AND @original_PHONE IS NULL))" InsertCommand="INSERT INTO SuppLier(Supplier, NAME, ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, ADMIN_NAME, ADDINFO, EmailAddress, HerdNo, PHONE, Country) VALUES (@Supplier, @NAME, @ADDRESS1, @ADDRESS2, @ADDRESS3, @ADDRESS4, @ADMIN_NAME, @ADDINFO, @EmailAddress, @HerdNo, @PHONE, @Country)" OldValuesParameterFormatString="original_{0}" SelectCommand="SELECT Supplier, NAME, ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, ADMIN_NAME, ADDINFO, EmailAddress, HerdNo, PHONE, Country FROM SuppLier WHERE (Supplier = @Supplier)" UpdateCommand="UPDATE SuppLier SET NAME = @NAME, ADDRESS1 = @ADDRESS1, ADDRESS2 = @ADDRESS2, ADDRESS3 = @ADDRESS3, ADDRESS4 = @ADDRESS4, ADMIN_NAME = @ADMIN_NAME, ADDINFO = @ADDINFO, EmailAddress = @EmailAddress, HerdNo = @HerdNo, PHONE = @PHONE, Country = @Country WHERE (Supplier = @original_Supplier) AND (NAME = @original_NAME OR NAME IS NULL AND @original_NAME IS NULL) AND (ADDRESS1 = @original_ADDRESS1 OR ADDRESS1 IS NULL AND @original_ADDRESS1 IS NULL) AND (ADDRESS2 = @original_ADDRESS2 OR ADDRESS2 IS NULL AND @original_ADDRESS2 IS NULL) AND (ADDRESS3 = @original_ADDRESS3 OR ADDRESS3 IS NULL AND @original_ADDRESS3 IS NULL) AND (ADDRESS4 = @original_ADDRESS4 OR ADDRESS4 IS NULL AND @original_ADDRESS4 IS NULL) AND (ADMIN_NAME = @original_ADMIN_NAME OR ADMIN_NAME IS NULL AND @original_ADMIN_NAME IS NULL) AND (ADDINFO = @original_ADDINFO OR ADDINFO IS NULL AND @original_ADDINFO IS NULL) AND (EmailAddress = @original_EmailAddress OR EmailAddress IS NULL AND @original_EmailAddress IS NULL) AND (HerdNo = @original_HerdNo OR HerdNo IS NULL AND @original_HerdNo IS NULL) AND (PHONE = @original_PHONE OR PHONE IS NULL AND @original_PHONE IS NULL) AND (Country = @original_Country OR Country IS NULL AND @original_Country IS NULL)">
                            <DeleteParameters>
                                <asp:Parameter Name="original_Supplier" Type="Int32" />
                                <asp:Parameter Name="original_NAME" Type="String" />
                                <asp:Parameter Name="original_ADDRESS1" Type="String" />
                                <asp:Parameter Name="original_ADDRESS2" Type="String" />
                                <asp:Parameter Name="original_ADDRESS3" Type="String" />
                                <asp:Parameter Name="original_ADDRESS4" Type="String" />
                                <asp:Parameter Name="original_ADMIN_NAME" Type="String" />
                                <asp:Parameter Name="original_ADDINFO" Type="String" />
                                <asp:Parameter Name="original_EmailAddress" Type="String" />
                                <asp:Parameter Name="original_HerdNo" Type="String" />
                                <asp:Parameter Name="original_PHONE" Type="String" />
                            </DeleteParameters>
                            <InsertParameters>
                                <asp:Parameter Name="Supplier" Type="Int32" />
                                <asp:Parameter Name="NAME" Type="String" />
                                <asp:Parameter Name="ADDRESS1" Type="String" />
                                <asp:Parameter Name="ADDRESS2" Type="String" />
                                <asp:Parameter Name="ADDRESS3" Type="String" />
                                <asp:Parameter Name="ADDRESS4" Type="String" />
                                <asp:Parameter Name="ADMIN_NAME" Type="String" />
                                <asp:Parameter Name="ADDINFO" Type="String" />
                                <asp:Parameter Name="EmailAddress" Type="String" />
                                <asp:Parameter Name="HerdNo" Type="String" />
                                <asp:Parameter Name="PHONE" Type="String" />
                                <asp:Parameter Name="Country" />
                            </InsertParameters>
                            <SelectParameters>
                                <asp:ControlParameter ControlID="GridView_Modal_Suppliers" Name="Supplier" PropertyName="SelectedValue" />
                            </SelectParameters>
                            <UpdateParameters>
                                <asp:Parameter Name="NAME" Type="String" />
                                <asp:Parameter Name="ADDRESS1" Type="String" />
                                <asp:Parameter Name="ADDRESS2" Type="String" />
                                <asp:Parameter Name="ADDRESS3" Type="String" />
                                <asp:Parameter Name="ADDRESS4" Type="String" />
                                <asp:Parameter Name="ADMIN_NAME" Type="String" />
                                <asp:Parameter Name="ADDINFO" Type="String" />
                                <asp:Parameter Name="EmailAddress" Type="String" />
                                <asp:Parameter Name="HerdNo" Type="String" />
                                <asp:Parameter Name="PHONE" Type="String" />
                                <asp:Parameter Name="Country" />
                                <asp:Parameter Name="original_Supplier" Type="Int32" />
                                <asp:Parameter Name="original_NAME" Type="String" />
                                <asp:Parameter Name="original_ADDRESS1" Type="String" />
                                <asp:Parameter Name="original_ADDRESS2" Type="String" />
                                <asp:Parameter Name="original_ADDRESS3" Type="String" />
                                <asp:Parameter Name="original_ADDRESS4" Type="String" />
                                <asp:Parameter Name="original_ADMIN_NAME" Type="String" />
                                <asp:Parameter Name="original_ADDINFO" Type="String" />
                                <asp:Parameter Name="original_EmailAddress" Type="String" />
                                <asp:Parameter Name="original_HerdNo" Type="String" />
                                <asp:Parameter Name="original_PHONE" Type="String" />
                                <asp:Parameter Name="original_Country" />
                            </UpdateParameters>
                        </asp:SqlDataSource>
                           
                    </ContentTemplate>
                </asp:UpdatePanel>
                   
                </div>
                <div id="SupplierSearch" class="informationPanel" style="display:none">
                     <asp:UpdatePanel ID="UpdatePanel_informationPanel_Supllier" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>

                                        <table style="margin-top:5px">
                                            <tr>
                                                <td>Supplier:&nbsp;</td>
                                                <td><asp:TextBox ID="TextBox_Modal_SupplierKeyword" runat="server" class="KeyPadinputSting form-control" onClick="setFocusID(this.id)"  OnTextChanged="TextBox_Modal_SupplierKeyword_TextChanged" AutoPostBack="True" />                                     
                                                    <ajaxToolkit:AutoCompleteExtender ID="TextBox_Modal_SupplierKeyword_AutoCompleteExtender" runat="server" BehaviorID="TextBox_Modal_SupplierKeyword_AutoCompleteExtender" DelimiterCharacters="" 
                                                        ServiceMethod="SearchCustomers"  MinimumPrefixLength="2" TargetControlID="TextBox_Modal_SupplierKeyword" CompletionInterval="100" EnableCaching="false" CompletionSetCount="10">
                                                    </ajaxToolkit:AutoCompleteExtender>
                                                </td>
                                                <td rowspan="2">
                                                    <asp:Button ID="Button_Search" runat="server" Text="Search" CssClass="btn btn-primary" style="margin-left:10px"/>
                                                    <asp:Button ID="Button17" runat="server" Text="Close" cssclass="btn" style="margin-left:10px"  Width="75px" OnClientClick ="openInformationPanel(event,'NewSupplier');return false;" />
                                                  
                                                </td>
                                            </tr>
                                        </table>
                                        <asp:HiddenField ID="HiddenField_Modal_Supplier" Value="0" runat="server" />
                                        <br />
                                        <div id="Gridview_SupplierDiv" style="height:250px; overflow-y:scroll"  onscroll="setSupplierScrollPosition(this.scrollTop,'#<%=HiddenField_Modal_Supplier.ClientID %>');">                                        
                                            <asp:GridView ID="GridView_Modal_Suppliers" runat="server" AutoGenerateColumns="False" DataKeyNames="Supplier" RowStyle-Height="40px" 
                                                DataSourceID="SqlDataSource_Suppliers" class="w3-table w3-striped w3-bordered w3-hoverable w3-card-4 w3-small" Font-Size="11px"  RowStyle-VerticalAlign="Middle" >
                                                <AlternatingRowStyle VerticalAlign="Middle"  />
                                                <Columns >
                                                    <%--<asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="LinkButton_SupplierName" Text='<%# Eval("NAME") %>' runat="server" ></asp:LinkButton>                                                  
                                                        </ItemTemplate>
                                                    </asp:TemplateField>--%>
                                                    <asp:BoundField DataField="NAME" HeaderText="NAME" SortExpression="NAME" ItemStyle-Height="45px" >
                                                    <ItemStyle Height="45px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="HerdNo" HeaderText="HerdNo" SortExpression="HerdNo" />
                                                    <asp:BoundField DataField="ADDRESS1" HeaderText="ADDRESS1" SortExpression="ADDRESS1" />
                                                    <asp:BoundField DataField="ADDRESS2" HeaderText="ADDRESS2" SortExpression="ADDRESS2" />
                                                </Columns>
                                                <SelectedRowStyle BackColor="#33CCFF" />
                                            </asp:GridView>
                                        </div>
                                        <asp:SqlDataSource ID="SqlDataSource_Suppliers" runat="server" ConnectionString="<%$ ConnectionStrings:AMPSLindenLambConnectionString %>" SelectCommand="SELECT [Supplier], [NAME], [ADDRESS1], [ADDRESS2], [HerdNo], [EmailAddress] FROM [SuppLier] WHERE (([HerdNo] LIKE '%' + @Keyword + '%') OR ([NAME] LIKE '%' + @Keyword + '%'))">
                                            <SelectParameters>                                   
                                                <asp:ControlParameter ControlID="TextBox_Modal_SupplierKeyword" Name="Keyword" PropertyName="Text" Type="String" DefaultValue="" />                                       
                                            </SelectParameters>
                                        </asp:SqlDataSource>
                            
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                </div>
                

            </div>
            <div class="col-md-4">
                 <asp:UpdatePanel ID="UpdatePanel_Keyboard" runat="server" UpdateMode="Always">
                           <ContentTemplate>
                            <div class="w3-row">
                              <a href="#" onclick="openKeyBoard(event, 'NUMBER');">
                                <div class="w3-half tablink w3-bottombar w3-hover-light-grey w3-padding w3-border-red">NUMBER</div>
                              </a>
                              <a href="#" onclick="openKeyBoard(event, 'ALPHABET');">
                                <div class="w3-half tablink w3-bottombar w3-hover-light-grey w3-padding">ALPHABET</div>
                              </a>                         
                            </div>

                            <div id="NUMBER" class="w3-container KeyBoard" style="display:block; height:360px">
                                <div class="row" style="margin-top:10px">
                                    <div class="col-sm-4"><asp:Button ID="Button7" runat="server" Text="7" class="btn btn-info KeyBoardStyle" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-4"><asp:Button ID="Button8" runat="server" Text="8" class="btn btn-info KeyBoardStyle" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-4"><asp:Button ID="Button9" runat="server" Text="9" class="btn btn-info KeyBoardStyle" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4"><asp:Button ID="Button4" runat="server" Text="4" class="btn btn-info KeyBoardStyle" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-4"><asp:Button ID="Button5" runat="server" Text="5" class="btn btn-info KeyBoardStyle" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-4"><asp:Button ID="Button6" runat="server" Text="6" class="btn btn-info KeyBoardStyle" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4"><asp:Button ID="Button1" runat="server" Text="1" class="btn btn-info KeyBoardStyle" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-4"><asp:Button ID="Button2" runat="server" Text="2" class="btn btn-info KeyBoardStyle" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-4"><asp:Button ID="Button3" runat="server" Text="3" class="btn btn-info KeyBoardStyle" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4"><asp:Button ID="Button11" runat="server" Text="0" class="btn btn-info KeyBoardStyle" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-4"><asp:Button ID="Button12" runat="server" Text="F" class="btn btn-info KeyBoardStyle" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-4"><asp:Button ID="Button13" runat="server" Text="DEL" class="btn btn-info KeyBoardStyle" OnClientClick="ketPayDelete();return false;" /></div>
                                </div>
                                   <div class="row">
                                    <div class="col-sm-4"><asp:Button ID="Button14" runat="server" Text="." class="btn btn-info KeyBoardStyle" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-4"><asp:Button ID="Button15" runat="server" Text="-" class="btn btn-info KeyBoardStyle" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-4"><asp:Button ID="Button16" runat="server" Text="@" class="btn btn-info KeyBoardStyle" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                </div>
                                <div class="row" style="margin-top:10px;">
                                    <%--<div class="col-sm-12"><asp:Button ID="Button_Enter" ClientIDMode="Static" runat="server" Text="ENTER" class="btn btn-info KeyBoardStyle" Width="245px" /></div> --%>                                 
                                </div>
                            </div>

                            <div id="ALPHABET" class="w3-container KeyBoard" style="height:360px">
                                <div class="row" style="margin-bottom:5px; margin-top:10px">                                
                                    <div class="col-sm-3"><asp:Button ID="Button19" runat="server" Text="A" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button21" runat="server" Text="B" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button22" runat="server" Text="C" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button23" runat="server" Text="D" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>                                       
                                </div>
                                <div class="row" style="margin-bottom:6px">                                 
                                    <div class="col-sm-3"><asp:Button ID="Button24" runat="server" Text="E" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button25" runat="server" Text="F" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button26" runat="server" Text="G" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button27" runat="server" Text="H" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>           
                                </div>
                                <div class="row" style="margin-bottom:6px">                               
                                    <div class="col-sm-3"><asp:Button ID="Button28" runat="server" Text="I" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button29" runat="server" Text="J" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button30" runat="server" Text="K" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button31" runat="server" Text="L" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>                                      
                                </div>
                                <div class="row" style="margin-bottom:6px">                                 
                                    <div class="col-sm-3"><asp:Button ID="Button32" runat="server" Text="M" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button33" runat="server" Text="N" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button34" runat="server" Text="O" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button35" runat="server" Text="P" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>                                   
                                </div>
                                <div class="row" style="margin-bottom:6px">                                  
                                    <div class="col-sm-3"><asp:Button ID="Button36" runat="server" Text="Q" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button37" runat="server" Text="R" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button38" runat="server" Text="S" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button39" runat="server" Text="T" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>   
                                </div>
                                <div class="row" style="margin-bottom:6px">                                  
                                    <div class="col-sm-3"><asp:Button ID="Button40" runat="server" Text="U" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button41" runat="server" Text="V" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button42" runat="server" Text="W" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button43" runat="server" Text="X" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                </div>
                                <div class="row" style="margin-bottom:10px">     
                                    <div class="col-sm-3"><asp:Button ID="Button44" runat="server" Text="Y" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button45" runat="server" Text="Z" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button46" runat="server" Text=" " class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button47" runat="server" Text="Del" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="ketPayDelete();" /></div>
                                </div>

                            </div>
                           <%-- <table class="w3-table" style="text-align:center; margin-top:5px">
                                <tr>
                                    <td><asp:Button ID="Button_Verify" runat="server" Text="Farm" CssClass="btn btn-success"  Width="120px" Height="50px" /></td>
                                 
                                </tr>
                            </table>      --%>
                               
                           </ContentTemplate>
                       </asp:UpdatePanel>           
            </div>
        </div>
    </div>


      <div class="modal fade" id="myModal_AlertMessage" role="dialog" >
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Alert Message</h4>            
        </div>
        <div class="modal-body" >
            <asp:UpdatePanel ID="UpdatePanel_Alert" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                   <div class="alert-info">
                        <strong>Warning! </strong><asp:Label ID="Label_AlertMessage" runat="server" Text=""></asp:Label>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>        
        </div>
        <div class="modal-footer" style="padding-top:5px;"></div>
      </div>
    </div>
  </div>

    <!-- Modal -->
<%--<div id="myModal"  class="modal fade"  role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Message Box</h4>
      </div>
      <div class="modal-body">
          <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
              <ContentTemplate>
                  <asp:Label ID="Label_AlertMessage" runat="server" Text=""></asp:Label>
              </ContentTemplate>
          </asp:UpdatePanel>
          
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>--%>

   


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Footer" Runat="Server">
</asp:Content>

