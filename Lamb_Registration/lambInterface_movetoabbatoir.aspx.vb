﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Drawing

Partial Class lambInterface_movetoabbatoir
    Inherits System.Web.UI.Page

    Dim runQuery As New SQLAccess
    '  Dim boolAllowManualKill As Boolean = False
    'Dim boolCheckSpecie As Boolean = False
    Protected Sub Button_Search_Click(sender As Object, e As EventArgs) Handles Button_Search.Click
        Dim strSQL As String
        Dim PermitNo As String = TextBox_PermitNo.Text
        Dim EMPLID As Integer
        Dim intCountRecords, intCountUncleared As Integer
        Dim boolAllowManualKill As Boolean = Convert.ToBoolean(Label_boolAllowManualKill.Text)
        Try
            Label_EMPLID.Text = String.Empty
            '     MsgBox(1)
            If TextBox_PermitNo.Text.Length > 0 Then
                strSQL = "SELECT EMPLID FROM EMPL_Header WHERE PermitNo = '" & PermitNo & "' AND permitregistered = 1"
                EMPLID = runQuery.getScalarInt(strSQL)
                Label_EMPLID.Text = EMPLID
                If EMPLID = 0 Then
                    '      MsgBox(2)
                    ShowAlertMessage("Permit No:" & PermitNo & " Not Found")
                    Exit Sub
                Else
                    '     MsgBox(3)
                    If boolAllowManualKill = False Then
                        strSQL = "SELECT COUNT(EMPLID) FROM EMPL_Line WHERE EMPLID = " & EMPLID & " AND TagsVerified = 'True' AND BatchTime IS NULL"
                        '  MsgBox(31)
                    Else
                        strSQL = "SELECT COUNT(EMPLID) FROM EMPL_Line WHERE EMPLID = " & EMPLID & " AND BatchTime IS NULL"
                        '  MsgBox(32)
                    End If
                    intCountRecords = runQuery.getScalarInt(strSQL)
                    If intCountRecords = 0 And boolAllowManualKill = False Then
                        strSQL = "SELECT COUNT(EMPLID) FROM EMPL_Line WHERE EMPLID = " & EMPLID & " AND TagsVerified = 'False' AND BatchTime IS NULL"
                        intCountUncleared = runQuery.getScalarInt(strSQL)
                        ' MsgBox(4)
                        If intCountUncleared > 0 Then
                            ShowAlertMessage("There are 1 or more batches for this permit that are waiting for tags to be cleared.")
                        Else
                            ShowAlertMessage("All species killed for this permit/certificate")
                        End If

                    ElseIf intCountRecords > 1 Then
                        ' MsgBox(5)
                        If boolAllowManualKill = True Then

                            strSQL = "SELECT EMPLID,EMPLLineID,Specie,qty,batchno FROM EMPL_Line WHERE EMPLID = " & EMPLID & " AND BatchTime IS NULL"
                        Else

                            strSQL = "SELECT EMPLID,EMPLLineID,Specie,qty,batchno FROM EMPL_Line WHERE EMPLID = " & EMPLID & " AND TagsVerified = 'True' AND BatchTime IS NULL"
                        End If

                        SqlDataSource_Modal_PickBatch.SelectCommand = strSQL
                        SqlDataSource_Modal_PickBatch.DataBind()
                        GridView_PickBatch.DataBind()
                        UpdatePanel_PickBatch.Update()
                        ScriptManager.RegisterStartupScript(Button_Search, GetType(Page), "myModal_PickBatch", "ShowModal('#myModal_PickBatch');", True)
                        ' MsgBox(6)
                    Else
                        'MsgBox(7)
                        If boolAllowManualKill = True Then
                            strSQL = "SELECT EMPL_Line.EMPLID,EMPL_Line.EMPLLineID, EMPL_Line.Specie,empl_line.lotno,  sex.sex as specieInt, EMPL_Line.Qty,EMPL_Line.NewQty, EMPL_Line.BatchNo, EMPL_Header.FlockNo, Supplier.Supplier as supplierInt,SuppLier.NAME, " &
                                         "isnull(SuppLier.ADDRESS1,'') + ' ' + isnull(SuppLier.ADDRESS2,'') AS addr1, isnull(SuppLier.ADDRESS3,'') + ' ' + isnull(SuppLier.ADDRESS4,'') AS addr2, DeadOnArrival,DeadInPen, isnull(EMPL_Header.QA,''), EMPL_Header.Batchtype, EMPL_Header.Company, " &
                                        " EMPL_Header.CertificateNo, EMPL_Header.FlockId, EMPL_Header.DirectImportFlock, EMPL_Header.PortFlock, SuppLier.GDTGroup " &
                                       "FROM  EMPL_Line INNER JOIN EMPL_Header ON EMPL_Line.EMPLID = EMPL_Header.EMPLID INNER JOIN " &
                                        "SuppLier ON EMPL_Header.Suppno = SuppLier.Supplier INNER JOIN Sex ON sex.ec_code= EMPL_Line.specie WHERE EMPL_Line.EMPLID = " & EMPLID &
                                       " AND BatchTime IS NULL"
                        Else
                            strSQL = "SELECT EMPL_Line.EMPLID,EMPL_Line.EMPLLineID, EMPL_Line.Specie,empl_line.lotno,  sex.sex as specieInt, EMPL_Line.Qty,EMPL_Line.NewQty, EMPL_Line.BatchNo, EMPL_Header.FlockNo, Supplier.Supplier as supplierInt,SuppLier.NAME, " &
                                              "isnull(SuppLier.ADDRESS1,'') + ' ' + isnull(SuppLier.ADDRESS2,'') AS addr1, isnull(SuppLier.ADDRESS3,'') + ' ' + isnull(SuppLier.ADDRESS4,'') AS addr2, DeadOnArrival,DeadInPen, EMPL_Header.Batchtype,  " &
                                              " EMPL_Header.CertificateNo, EMPL_Header.FlockId, EMPL_Header.DirectImportFlock, EMPL_Header.PortFlock, EMPL_Line.company, empl_line.qa, SuppLier.GDTGroup  " &
                                              "FROM  EMPL_Line INNER JOIN EMPL_Header ON EMPL_Line.EMPLID = EMPL_Header.EMPLID INNER JOIN " &
                                              "SuppLier ON EMPL_Header.Suppno = SuppLier.Supplier INNER JOIN Sex ON sex.ec_code= EMPL_Line.specie WHERE EMPL_Line.EMPLID = " & EMPLID &
                                              " AND TagsVerified = 'True' AND BatchTime IS NULL"
                        End If
                        fillFrom(strSQL, EMPLID)
                    End If


                End If

            Else
                Exit Sub
            End If
        Catch ex As Exception
            ShowAlertMessage(ex.Message)
        End Try
    End Sub
    Sub fillFrom(ByVal sqlstring As String, ByVal EMPLID As String)
        'Dim strOrigSpecie As String
        Dim EMPLLineID As String
        Dim daconnect As SqlClient.SqlDataAdapter
        Dim dsDetails As New DataSet
        daconnect = New SqlClient.SqlDataAdapter(sqlstring, New SqlConnection(ConfigurationManager.ConnectionStrings("AMPSLindenLambConnectionString").ToString))
        daconnect.Fill(dsDetails, "SpecieDetails")
        daconnect.Dispose()

        EMPLLineID = dsDetails.Tables(0).Rows(0).Item("EMPLLineID")
        Label_EMPLLineID.Text = EMPLLineID
        Label_Flock.Text = dsDetails.Tables(0).Rows(0).Item("flockno")
        DropDownList_Specie.SelectedIndex = DropDownList_Specie.Items.IndexOf(DropDownList_Specie.Items.FindByValue(dsDetails.Tables(0).Rows(0).Item("specie")))
        DropDownList_Company.SelectedIndex = DropDownList_Company.Items.IndexOf(DropDownList_Company.Items.FindByValue(dsDetails.Tables(0).Rows(0).Item("Company")))

        If dsDetails.Tables(0).Rows(0).Item("QA") = True Then
            CheckBox_QA.Checked = True
        Else
            CheckBox_QA.Checked = False
        End If

        '  strOrigSpecie = dsDetails.Tables(0).Rows(0).Item("specie")
        Label_SupplierName.Text = dsDetails.Tables(0).Rows(0).Item("Name")
        Label_SupplierInt.Text = dsDetails.Tables(0).Rows(0).Item("SupplierInt")
        Label_Address1.Text = dsDetails.Tables(0).Rows(0).Item("addr1")
        Label_Address2.Text = dsDetails.Tables(0).Rows(0).Item("addr2")
        Label_Qty.Text = dsDetails.Tables(0).Rows(0).Item("newqty")
        Label_Qty_DeadInPen.Text = runQuery.getScalarInt("SELECT COUNT(*) FROM EMPL_tags WHERE EMPLID = " & EMPLID & " AND EMPLLineID = " & EMPLLineID & " AND AnimalDead= 'PEN'")
        Label_Qty_DeadOnArrival.Text = runQuery.getScalarInt("SELECT COUNT(*) FROM EMPL_tags WHERE EMPLID = " & EMPLID & " AND EMPLLineID = " & EMPLLineID & " AND AnimalDead= 'DOA'")
        Label_Qty_Slaughter.Text = Label_Qty.Text - Label_Qty_DeadInPen.Text - Label_Qty_DeadOnArrival.Text
        Label_OriginalQty.Text = dsDetails.Tables(0).Rows(0).Item("Qty")
        Label_BatchNo.Text = dsDetails.Tables(0).Rows(0).Item("Batchno")
        Label_intGDTGroup.Text = dsDetails.Tables(0).Rows(0).Item("GDTGroup")
        Label_intSpecie.Text = dsDetails.Tables(0).Rows(0).Item("SpecieInt")
        Label_strOrigSpecie.Text = dsDetails.Tables(0).Rows(0).Item("specie")
        Label_NextLotNo.Text = runQuery.getScalarInt("SELECT isnull(MAX(lotno),0)+1 FROM EMPL_Line WHERE Batchtime > convert(varchar(10),getDate(),120)")

        Label_intMS.Text = runQuery.getScalarInt("SELECT COUNT([EMPL_Line].EMPLID) FROM [EMPL_Line] INNER JOIN EMPL_Header ON [EMPL_Line].emplID =  EMPL_Header.EMPLID " &
                                         " INNER JOIN Supplier ON  EMPL_Header.suppno =  Supplier.supplier WHERE [EMPL_Line].EMPLID = " & EMPLID & " and [EMPL_Line].EMPLLineID = " & EMPLLineID &
                                         " AND [U12M]=1 AND [O30DaysReside] = 1 AND [Farms_3OrLess] =1 AND supplier.dest =  'SFS' AND [EMPL_Line].qa = 1")

        Label_intTesco.Text = runQuery.getScalarInt("SELECT COUNT([EMPL_Line].EMPLID) FROM [EMPL_Line] INNER JOIN EMPL_Header ON [EMPL_Line].emplID =  EMPL_Header.EMPLID " &
                                   " INNER JOIN Supplier ON  EMPL_Header.suppno =  Supplier.supplier WHERE [EMPL_Line].EMPLID = " & EMPLID & " and [EMPL_Line].EMPLLineID = " & EMPLLineID &
                                   " AND [U12M]=1 AND [O30DaysReside] = 1 AND [Farms_3OrLess] =1 AND [EMPL_Line].qa = 1")

        Label_strSFS.Text = runQuery.getScalarInt("SELECT SFS FROM Supplier INNER JOIN EMPL_Header ON  EMPL_Header.suppno =  Supplier.supplier WHERE [EMPL_Header].EMPLID = " & EMPLID)

        If Label_strSFS.Text = "SFS" Then
            Label_intSFS.Text = 1
        Else
            Label_intSFS.Text = 0
        End If

        If dsDetails.Tables(0).Rows(0).Item("lotno") = (CInt(Label_NextLotNo.Text) - 1) And dsDetails.Tables(0).Rows(0).Item("lotno") <> 0 Then
            'Button_SplitBatch.Enabled = True
        ElseIf dsDetails.Tables(0).Rows(0).Item("lotno") = 0 Then
            Button_MovetoAbbatoir.Enabled = True
            Button_Qty_reduce.Enabled = True
            Button_Qty_Plus.Enabled = True
            Button_Qty_Reset.Enabled = True
        Else
            ShowAlertMessage("Error Occured on retriving data")
        End If
        'boolCheckSpecie = True
        Label_boolCheckSpecie.Text = True
        ' added 2016-12-22 by cyang, to ensure the vendor is created
        createInnovaSupplier(CInt(dsDetails.Tables(0).Rows(0).Item("SupplierInt")))
        UpdatePanel_PermitDetails.Update()
    End Sub
    Protected Sub GridView_PickBatch_SelectedIndexChanged(sender As Object, e As EventArgs) Handles GridView_PickBatch.SelectedIndexChanged
        If GridView_PickBatch.SelectedIndex > -1 Then
            Dim EMPLID As String = GridView_PickBatch.SelectedDataKey(0)
            Dim EMPLLineID As String = GridView_PickBatch.SelectedDataKey(1)
            Dim strSQL As String
            strSQL = "SELECT EMPL_Line.EMPLID,EMPL_Line.EMPLLineID, EMPL_Line.Specie, empl_line.lotno, sex.sex as specieInt, EMPL_Line.Qty,EMPL_Line.NewQty, EMPL_Line.BatchNo, EMPL_Header.FlockNo, Supplier.Supplier as supplierInt, SuppLier.NAME, " &
                          "SuppLier.ADDRESS1 + ' ' + SuppLier.ADDRESS2 AS addr1, SuppLier.ADDRESS3 + ' ' + SuppLier.ADDRESS4 AS addr2, DeadOnArrival,DeadInPen, EMPL_Header.Batchtype, " &
                          " EMPL_Header.CertificateNo, EMPL_Header.FlockId, EMPL_Header.DirectImportFlock, EMPL_Header.PortFlock, empl_line.Company, empl_line.qa, SuppLier.GDTGroup " &
                          "FROM  EMPL_Line INNER JOIN EMPL_Header ON EMPL_Line.EMPLID = EMPL_Header.EMPLID INNER JOIN " &
                            "SuppLier ON EMPL_Header.Suppno = SuppLier.Supplier INNER JOIN Sex ON sex.ec_code= EMPL_Line.specie WHERE EMPL_Line.EMPLID = " & EMPLID & " AND EMPLLineID = " & EMPLLineID
            fillFrom(strSQL, EMPLID)
            ScriptManager.RegisterStartupScript(Button_Search, GetType(Page), "myModal_PickBatch", "HideModal('#myModal');", True)
        End If
    End Sub

    Protected Sub Button_Qty_reduce_Click(sender As Object, e As EventArgs) Handles Button_Qty_reduce.Click
        Label_Qty.Text -= 1
    End Sub
    Protected Sub Button_Qty_Plus_Click(sender As Object, e As EventArgs) Handles Button_Qty_Plus.Click
        Label_Qty.Text += 1
    End Sub
    Protected Sub Button_Qty_Reset_Click(sender As Object, e As EventArgs) Handles Button_Qty_Reset.Click
        Label_Qty.Text = Label_OriginalQty.Text
    End Sub

    Protected Sub Button_Submit_Click(sender As Object, e As EventArgs) Handles Button_Submit.Click

        If Button_Verify.Text = "Verify" Then

            If TextBox_Password.Text = "DieLambsDie" Then
                Button_Verify.Text = "Manual"
                Label_boolAllowManualKill.Text = "True"
                'boolAllowManualKill = True
            End If
        Else
            Button_Verify.Text = "Verify"
            ' boolAllowManualKill = False
            Label_boolAllowManualKill.Text = "False"
        End If
        UpdatePanel_PermitDetails.Update()
        ScriptManager.RegisterStartupScript(Button_Submit, GetType(Page), "hideMyModal", "HideModal('#myModal_EnterPassword');", True)
    End Sub

    Protected Sub Button_MovetoAbbatoir_Click(sender As Object, e As EventArgs) Handles Button_MovetoAbbatoir.Click
        Try
            Dim EMPLID As String = Label_EMPLID.Text
            Dim EMPLLineID As String = Label_EMPLLineID.Text
            Dim intAssignStatus As Integer = runQuery.getScalarInt("SELECT AssignStatus FROM EMPL_Line WHERE EMPLID = " & EMPLID & " AND EMPLLineID = " & EMPLLineID)
            Dim intBatchno As Integer
            Dim strSQL As String
            Dim intQA As Integer = 0
            If intAssignStatus < 4 Then

                intBatchno = runQuery.getScalarInt("SELECT batchno FROM EMPL_Line WHERE EMPLID = " & EMPLID & " AND EMPLLineID = " & EMPLLineID)
                If MoveBatchToAbb(intBatchno) = 1 Then
                    If CheckBox_QA.Checked Then
                        intQA = 1
                    End If

                    strSQL = "UPDATE EMPL_Line SET lotno = " & Label_NextLotNo.Text & ",specie = '" & DropDownList_Specie.SelectedItem.Value & "', MoveToAbb =1 , BATCHTime = Getdate() " &
                                  ", company = " & DropDownList_Company.SelectedItem.Value & ", AssignStatus = 4, deadOnArrival =  " & Label_Qty_DeadOnArrival.Text & ", DeadInPen = " & Label_Qty_DeadInPen.Text &
                                  ", slaughterqty = " & Label_Qty_Slaughter.Text & ", MS = " & Label_intMS.Text & ", Tesco = " & Label_intTesco.Text & " WHERE EMPLID = " & EMPLID & " AND EMPLLineID = " & EMPLLineID
                    runQuery.executeQuery(strSQL, "AMPSLindenLambConnectionString")
                    Dim OrderID = Now.Date.ToString("yyMMdd") & CInt(Label_NextLotNo.Text).ToString.PadLeft(3, "0")
                    CreateAMPSPayslip(Label_Qty_Slaughter.Text, OrderID)
                    CreateInnovaLot(Label_Qty_Slaughter.Text, OrderID)
                    Label_NextLotNo.Text = runQuery.getScalarInt("SELECT isnull(MAX(lotno),0)+1 FROM EMPL_Line WHERE Batchtime > convert(varchar(10),getDate(),120)")
                    clearScreen()
                    UpdatePanel_PermitDetails.Update()

                Else
                    ShowAlertMessage("Move to abbatoir failed. Try again. If problem continues contact IT.")
                End If
            End If

        Catch ex As Exception
            ShowAlertMessage(ex.Message)
        End Try
    End Sub
    Function MoveBatchToAbb(ByVal intBatchno As Integer) As Integer
        Try
            Dim ws_Generic As New WS_SheepMoveToAbatLive.GenericInput
            Dim ws_SMTA_InfoInput As New WS_SheepMoveToAbatLive.SheepMoveToAbatInput
            Dim ws_SMTA_InfoOutput As New WS_SheepMoveToAbatLive.SheepMoveToAbatOutput
            Dim ws_SMTA As New WS_SheepMoveToAbatLive.WS_SheepMoveToAbatPortTypeClient

            '  MsgBox(intBatchno)

            ws_Generic.callingLocation = "Meat"
            ws_Generic.channel = "Meat"
            'ws_Generic.clntRefNo_BusId = 993336
            ws_Generic.clntRefNo_BusId = 922225
            ws_Generic.environment = "Live"
            ws_Generic.herdNo = "A04001"
            ws_Generic.password = ""
            ws_Generic.username = "LINS01"

            ws_SMTA_InfoInput.genericInput = ws_Generic
            ws_SMTA_InfoInput.pinNo = "003"

            ws_SMTA_InfoInput.batchNo = intBatchno
            'ws_SMTA_InfoInput.batchNoSpecifie = True
            ws_SMTA_InfoOutput = ws_SMTA.WS_SheepMoveToAbat(ws_SMTA_InfoInput)

            '   MsgBox(ws_SMTA_InfoOutput.success)
            If ws_SMTA_InfoOutput.success = 1 Then

                Return ws_SMTA_InfoOutput.success

            Else

                'MsgBox("Error: " & ws_SMTA_InfoOutput.error & " ErrorCode: " & ws_SMTA_InfoOutput.errorCode)
                ShowAlertMessage("Error: " & ws_SMTA_InfoOutput.error & " ErrorCode: " & ws_SMTA_InfoOutput.errorCode)
                Return ws_SMTA_InfoOutput.success
            End If
        Catch ex As Exception
            ' MsgBox("Error on MoveBatchToAbbatior: " & ex.Message)
            ShowAlertMessage("Error on MoveBatchToAbbatior: " & ex.Message)
        End Try
    End Function

    Private Function CreateAMPSPayslip(ByVal intQuantity As Integer, ByVal OrderID As String) As Integer

        Dim strFQA As String
        Dim intFQA As Integer
        Dim strSlapMark As String
        Dim strDest As String
        'Dim OrderID As Integer

        Dim intCountRecords As Integer
        Dim strSQLPayslips As String
        Try



            If CheckBox_QA.Checked = True Then
                strFQA = "Q"
                intFQA = 1
            Else
                strFQA = ""
                intFQA = 0
            End If


            strSlapMark = runQuery.getScalarString("SELECT slapmark FROM destination WHERE DestNo = '" & DropDownList_Company.SelectedItem.Value & "'", "AMPSLindenLambConnectionString")

            strDest = runQuery.getScalarString("SELECT [Description] FROM destination WHERE DestNo = '" & DropDownList_Company.SelectedItem.Value & "'", "AMPSLindenLambConnectionString")



            '######################################   CREATE ORDERID     ############################################
            'Generate OrderID
            'OrderID = Now.Date.Year.ToString.Substring(2) & Me.checkhas2digits(Now.Date.Month) & Me.checkhas2digits(Now.Date.Day) & Me.checkhas3digits(Me.txtNextLotno.Text)


            '########################################################################################################

            intCountRecords = runQuery.getScalarInt("SELECT COUNT(*) FROM Payslips WHERE OrderID = " & OrderID)

            'If the orderId exists then delete any killtrans records and the payslip record so that it can be added again
            If intCountRecords > 0 Then

                runQuery.executeQuery("DELETE FROM Killtrans WHERE OrderID = " & OrderID, "AMPSLindenLambConnectionString")
                runQuery.executeQuery("DELETE FROM Payslips WHERE OrderID = " & OrderID, "AMPSLindenLambConnectionString")

            End If


            strSQLPayslips = "INSERT INTO Payslips (orderID, lotno, supplierno, total_qty, kill_date, status, ownkill, sex, LambGDTGroup, SFS)  VALUES(" &
                               OrderID & ", " & Label_NextLotNo.Text & ", " & Label_SupplierInt.Text & ", " & intQuantity & ", GetDate(), " &
                               "10, '" & strSlapMark & "','" & DropDownList_Specie.SelectedItem.Value & "', " & Label_intGDTGroup.Text & ", " & Label_intSFS.Text & ")"

            runQuery.executeQuery(strSQLPayslips, "AMPSLindenLambConnectionString")
        Catch ex As Exception
            ShowAlertMessage("Error on CreateAMPSPayslip: " & ex.Message)
        End Try

    End Function

    Private Function CreateInnovaLot(ByVal intQuantity As Integer, ByVal orderID As String) As Integer

        Dim intFQA As Integer
        Dim strFQA As String
        Dim XMLString As String
        Dim strSlapmark As String
        Dim strSQL As String
        Dim strDest As String
        Dim strSFS As String
        Dim intOrderID As Integer
        Dim strSpecie As String
        Dim strKilldate As String
        Dim strSurname As String
        Dim intSeq As Integer

        Try


            strSlapmark = runQuery.getScalarString("SELECT SlapMark FROM Destination WHERE DestNo = " & DropDownList_Company.SelectedItem.Value, "AMPSLindenLambConnectionString")

            'Get dest details

            strDest = runQuery.getScalarString("SELECT [description] FROM destination WHERE destno = " & DropDownList_Company.SelectedItem.Value, "AMPSLindenLambConnectionString")

            'Get FQA details
            If CheckBox_QA.Checked Then
                strFQA = "QA"
            Else
                strFQA = ""
            End If

            'Get supplier name
            strSurname = runQuery.getScalarString("SELECT admin_name FROM supplier WHERE supplier = " & Label_SupplierInt.Text, "AMPSLindenLambConnectionString")

            strKilldate = Now.Date.ToString("yyyy-MM-dd") & "T00:00:00"


            XMLString = "<Objects>"

            XMLString += "<Object DataType=" & Chr(34) & "Marel.Mp5.Process.Activity.LotRecord" & Chr(34) & ">" &
                        "<Properties>" &
                        "<Property name=" & Chr(34) & "Code" & Chr(34) & ">" & orderID & "</Property>" &
                        "<Property name=" & Chr(34) & "ExtCode" & Chr(34) & ">" & orderID & "</Property>" &
                        "<Property name=" & Chr(34) & "Name" & Chr(34) & ">" & orderID & " - " & strSurname & "-" & intQuantity & "</Property>" &
                        "<Property name=" & Chr(34) & "SlDay" & Chr(34) & ">" & strKilldate & "</Property>" &
                        "<Property name=" & Chr(34) & "Description1" & Chr(34) & ">" & DropDownList_Specie.SelectedItem.Text & "</Property>" &
                        "<Property name=" & Chr(34) & "Description2" & Chr(34) & ">" & strDest & "</Property>"

            If strFQA = "QA" Then
                XMLString += "<Property name=" & Chr(34) & "Description3" & Chr(34) & ">" & strFQA & "</Property>"
                XMLString += "<Property name=" & Chr(34) & "qamark1" & Chr(34) & ">1</Property>"
            Else
                XMLString += "<Property name=" & Chr(34) & "Description3" & Chr(34) & " nil=" & Chr(34) & "true" & Chr(34) & "/>"
            End If


            XMLString += "<Property name=" & Chr(34) & "Processor" & Chr(34) & ">" & "#" & Label_SupplierInt.Text & "</Property>" &
                            "<Property name=" & Chr(34) & "Customer" & Chr(34) & ">" & strDest & "</Property>" &
                            "<Property name=" & Chr(34) & "ExpectedCount" & Chr(34) & ">" & intQuantity & "</Property>" &
                            "<Property name=" & Chr(34) & "Dimension1" & Chr(34) & ">" & Label_intMS.Text & "</Property>" &
                            "<Property name=" & Chr(34) & "Dimension2" & Chr(34) & ">" & Label_intTesco.Text & "</Property>" &
                            "<Property name=" & Chr(34) & "Description5" & Chr(34) & ">" & Label_BatchNo.Text & "</Property>" &
                            "<Property name=" & Chr(34) & "SlSequence" & Chr(34) & ">" & Label_NextLotNo.Text & "</Property>" &
                            "</Properties> </Object>"

            'Assigns the OrderID to the lamb scale process unit (prpr0015)
            XMLString += "<Object DataType=" & Chr(34) & "Marel.Mp5.Process.Process.ProcessUnitLotRecord" & Chr(34) & ">" &
                                           "<Properties>" &
                                           "<Property name=" & Chr(34) & "Prunit" & Chr(34) & ">prpr0015" & "</Property>" &
                                           "<Property name=" & Chr(34) & "Lot" & Chr(34) & ">" & orderID & "</Property>" &
                                           "</Properties> </Object>"


            If XMLString <> "<Objects>" Then
                XMLString += "</Objects>"

                strSQL = "INSERT INTO itgr_imports (regTime,importStatus,ImportHandlerCode,Data)  VALUES(getdate(),0,'igih0009','" & XMLString & "')"

                runQuery.executeInnovaNonQuery(strSQL)
            End If
        Catch ex As Exception
            ShowAlertMessage("Error on Create Innova Lot: " & ex.Message)
        End Try
    End Function
    Sub clearScreen()
        Button_MovetoAbbatoir.Enabled = False
        Button_Qty_reduce.Enabled = False
        Button_Qty_Reset.Enabled = False
        Button_Qty_Plus.Enabled = False

        TextBox_PermitNo.Text = String.Empty
        TextBox_Password.Text = String.Empty

        Label_Flock.Text = String.Empty
        Label_SupplierInt.Text = String.Empty
        Label_SupplierName.Text = String.Empty
        Label_Address1.Text = String.Empty
        Label_Address2.Text = String.Empty
        Label_Qty.Text = String.Empty
        Label_OriginalQty.Text = String.Empty
        Label_Qty_DeadInPen.Text = String.Empty
        Label_Qty_DeadOnArrival.Text = String.Empty
        Label_Qty_Slaughter.Text = String.Empty
        Label_BatchNo.Text = String.Empty
        Label_boolAllowManualKill.Text = "False"
        Label_boolCheckSpecie.Text = "False"
        Label_EMPLID.Text = String.Empty
        Label_EMPLLineID.Text = String.Empty
        Label_intMS.Text = String.Empty
        Label_intTesco.Text = String.Empty
        Label_strSFS.Text = String.Empty
        Label_intSFS.Text = String.Empty
        Label_intSpecie.Text = String.Empty
        Label_strOrigSpecie.Text = String.Empty

        Label_intGDTGroup.Text = String.Empty

        UpdatePanel_PermitDetails.Update()



    End Sub
    Sub ShowAlertMessage(ByVal Msg As String)
        Label_AlertMessage.Text = Msg
        UpdatePanel_Alert.Update()
        ScriptManager.RegisterStartupScript(Button_Submit, GetType(Page), "showMyModal", "ShowModal('#myModal_AlertMessage');", True)
    End Sub
    Private Sub checkSpecie()
        Dim boolCheckSpecie As Boolean = Convert.ToBoolean(Label_boolCheckSpecie.Text)
        Dim strOrigSpecie As String = Label_strOrigSpecie.Text
        Dim Specievalue As String = DropDownList_Specie.SelectedItem.Value
        If boolCheckSpecie = True Then

            If strOrigSpecie <> DropDownList_Specie.SelectedItem.Value Then

                If strOrigSpecie = "L" Or strOrigSpecie = "H" Or strOrigSpecie = "LMS" Or strOrigSpecie = "LO" Then

                    If Specievalue <> "L" And Specievalue <> "LMS" And Specievalue <> "H" And Specievalue <> "LO" Then

                        ShowAlertMessage("You cannot change to this Specie")
                        DropDownList_Specie.SelectedIndex = DropDownList_Specie.Items.IndexOf(DropDownList_Specie.Items.FindByValue(strOrigSpecie))
                        UpdatePanel_PermitDetails.Update()
                    End If

                Else
                    'MsgBox("You cannot change to this Specie")
                    ShowAlertMessage("You cannot change to this Specie")
                    DropDownList_Specie.SelectedIndex = DropDownList_Specie.Items.IndexOf(DropDownList_Specie.Items.FindByValue(strOrigSpecie))
                    UpdatePanel_PermitDetails.Update()

                End If

            End If

        End If

    End Sub
    Protected Sub DropDownList_Specie_SelectedIndexChanged(sender As Object, e As EventArgs) Handles DropDownList_Specie.SelectedIndexChanged
        checkSpecie()
    End Sub

    Public Sub createInnovaSupplier(ByVal intSupplierNo As Integer)

        Dim strSQL As String
        Dim daConnect As SqlDataAdapter
        Dim dsSupplier As New DataSet
        Dim XMLString As String
        Dim strName As String
        Dim strAdminName As String
        Dim strAddress1 As String
        Dim strAddress2 As String
        Dim strAddress3 As String
        Dim strPostcode As String
        Dim strFlockNo As String

        Try
            strSQL = "SELECT Supplier, ISNULL(name,'') as name, ISNULL(admin_name,'') as admin_name, ISNULL(address1,'') as address1, ISNULL(address2,'') as address2, " &
                        "ISNULL(address3,'') as address3, ISNULL(addinfo,'') as addinfo, ISNULL(HerdNo,'') as Herdno FROM supplier WHERE supplier = " & intSupplierNo

            daConnect = New SqlClient.SqlDataAdapter(strSQL, ConfigurationManager.ConnectionStrings("AMPSLindenLambConnectionString").ToString)
            daConnect.Fill(dsSupplier, "SupplierDetails")
            daConnect.Dispose()

            strName = dsSupplier.Tables(0).Rows(0).Item("Name")
            strAdminName = dsSupplier.Tables(0).Rows(0).Item("Admin_Name")
            strAddress1 = dsSupplier.Tables(0).Rows(0).Item("Address1")
            strAddress2 = dsSupplier.Tables(0).Rows(0).Item("Address2")
            strAddress3 = dsSupplier.Tables(0).Rows(0).Item("Address3")
            strPostcode = dsSupplier.Tables(0).Rows(0).Item("addinfo")
            strFlockNo = dsSupplier.Tables(0).Rows(0).Item("HerdNo")



            If strName.Trim.Length = 0 Then
                strName = "<Property name=" & Chr(34) & "Name" & Chr(34) & " nil=" & Chr(34) & "true" & Chr(34) & "/>"
            Else
                strName = strName.Replace("&", "and")
                strName = strName.Replace("'", "")
                strName = "<Property name=" & Chr(34) & "Name" & Chr(34) & ">" & strName & "</Property>"
            End If

            If strAdminName.Trim.Length = 0 Then
                strAdminName = "<Property name=" & Chr(34) & "ShName" & Chr(34) & " nil=" & Chr(34) & "true" & Chr(34) & "/>"
            Else
                strAdminName = strAdminName.Replace("&", "and")
                strAdminName = strAdminName.Replace("'", "")
                strAdminName = "<Property name=" & Chr(34) & "ShName" & Chr(34) & ">" & strAdminName & "</Property>"
            End If

            If strAddress1.Trim.Length = 0 Then
                strAddress1 = "<Property name=" & Chr(34) & "Address1" & Chr(34) & " nil=" & Chr(34) & "true" & Chr(34) & "/>"
            Else
                strAddress1 = strAddress1.Replace("&", "and")
                strAddress1 = strAddress1.Replace("'", "")
                strAddress1 = "<Property name=" & Chr(34) & "Address1" & Chr(34) & ">" & strAddress1 & "</Property>"
            End If

            If strAddress2.Trim.Length = 0 Then
                strAddress2 = "<Property name=" & Chr(34) & "Address2" & Chr(34) & " nil=" & Chr(34) & "true" & Chr(34) & "/>"
            Else
                strAddress2 = strAddress2.Replace("&", "and")
                strAddress2 = strAddress2.Replace("'", "")
                strAddress2 = "<Property name=" & Chr(34) & "Address2" & Chr(34) & ">" & strAddress2 & "</Property>"
            End If

            If strAddress3.Trim.Length = 0 Then
                strAddress3 = "<Property name=" & Chr(34) & "City" & Chr(34) & " nil=" & Chr(34) & "true" & Chr(34) & "/>"
            Else
                strAddress3 = strAddress3.Replace("&", "and")
                strAddress3 = strAddress3.Replace("'", "")
                strAddress3 = "<Property name=" & Chr(34) & "City" & Chr(34) & ">" & strAddress3 & "</Property>"
            End If

            If strPostcode.Trim.Length = 0 Then
                strPostcode = "<Property name=" & Chr(34) & "PostCode" & Chr(34) & " nil=" & Chr(34) & "true" & Chr(34) & "/>"
            Else
                strPostcode = strPostcode.Replace("&", "and")
                strPostcode = strPostcode.Replace("'", "")
                strPostcode = "<Property name=" & Chr(34) & "PostCode" & Chr(34) & ">" & strPostcode & "</Property>"
            End If

            XMLString = "<Objects>"

            XMLString += "<Object DataType=" & Chr(34) & "Marel.Mp5.Base.Database.CompanyRecord" & Chr(34) & ">" &
                    "<Properties>" &
                    "<Property name=" & Chr(34) & "Code" & Chr(34) & ">" & "#" & dsSupplier.Tables(0).Rows(0).Item("Supplier") & "</Property>" &
                    "<Property name=" & Chr(34) & "Description5" & Chr(34) & ">" & strFlockNo & "</Property>" &
                    strName &
                    strAdminName &
                    strAddress1 &
                    strAddress2 &
                    strAddress3 &
                    strPostcode &
                    "</Properties></Object></Objects>"

            strSQL = "INSERT INTO itgr_imports (regTime,importStatus,ImportHandlerCode,Data)  VALUES(getdate(),0,'igih0010','" & XMLString & "')"

            runQuery.executeInnovaNonQuery(strSQL)

        Catch ex As Exception
            ShowAlertMessage("Create new Vendor: " & ex.Message)
            '  UpdatePanel1.Update
            '  ScriptManager.RegisterStartupScript(FormView_Supplier, GetType(Page), "showMyModal", "ShowModal('#myModal');", True)
        End Try
    End Sub
End Class
