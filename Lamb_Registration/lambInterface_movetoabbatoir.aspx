﻿<%@ Page Title="" Language="VB" MasterPageFile="~/LambInterface.master" AutoEventWireup="false" CodeFile="lambInterface_movetoabbatoir.aspx.vb" Inherits="lambInterface_movetoabbatoir" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
      <%--  <link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css" />--%>

       <script>
           var controlIDAcceptValue;
                                    function openKeyBoard(evt, Name) {
                                      var i, x, tablinks;
                                      x = document.getElementsByClassName("KeyBoard");
                                      for (i = 0; i < x.length; i++) {
                                         x[i].style.display = "none";
                                      }
                                      tablinks = document.getElementsByClassName("tablink");
                                      for (i = 0; i < x.length; i++) {
                                         tablinks[i].className = tablinks[i].className.replace(" w3-border-red", "");
                                      }
                                      document.getElementById(Name).style.display = "block";
                                      evt.currentTarget.firstElementChild.className += " w3-border-red";
                                    }

                                    function openInformationPanel(evt,panel ) {
                                        var i, x, tablinks;
                                        x = document.getElementsByClassName("informationPanel");
                                        for (i = 0; i < x.length; i++) {
                                            x[i].style.display = "none";
                                        }

                                        document.getElementById(panel).style.display = "block";
                   
                                    }
                                    function KeyPaysetValue(val) {

                                        try {

                                            document.getElementById(controlIDAcceptValue).value = document.getElementById(controlIDAcceptValue).value + val;
                                            document.getElementById(controlIDAcceptValue).focus();
                                        }
                                        catch (err) {
                                            $('#myModal_AlertMessage').show();
                                            //   alert("Please set focus on text box to accept key pad value.");
                                            document.getElementById('<%=Label_AlertMessage.ClientID %>').innerText = " Please set focus on text box to accept key pad value."
                                        }

                                    }
                                    function setFocusID(id) {
                                        controlIDAcceptValue = id;
                                    }
                                    function ketPayDelete() {
                                        var currentValue = document.getElementById(controlIDAcceptValue).value;
                                        currentValue = currentValue.substring(0, currentValue.length - 1);
                                        document.getElementById(controlIDAcceptValue).value = currentValue;
                                    }
                                
        </script>

      <style>
    .KeyBoard {display:none;}
        .KeyBoardStyle {
              width:65px;
              height:52px;
              margin-bottom:5px;
              font-size:large;
        }
        .KeyBoardStyle_Alphabet {
              width:50px;
              height:45px;
              font-size:large;
        }
        </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
 
    <div class="container" style="width:980px; height:500px">
        <div class="row">
            <div class="col-md-8">
                <asp:UpdatePanel ID="UpdatePanel_PermitDetails" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <table class="table">
                            <tr>
                                <th style="width:110px">PermitNo.:
                                    <asp:Label ID="Label_EMPLID" runat="server" Text="" Visible="false" />
                                    <asp:Label ID="Label_EMPLLineID" runat="server" Text="" Visible="false" />
                                    <asp:Label ID="Label_intMS" runat="server" Text="" Visible="false" />
                                    <asp:Label ID="Label_intTesco" runat="server" Text="" Visible="false" />
                                    <asp:Label ID="Label_strSFS" runat="server" Text="" Visible="false" />
                                    <asp:Label ID="Label_intSFS" runat="server" Text="" Visible="false" />
                                    <asp:Label ID="Label_intSpecie" runat="server" Text="" Visible="false" />
                                    <asp:Label ID="Label_boolCheckSpecie" runat="server" Text="" Visible="false" />
                                    <asp:Label ID="Label_intGDTGroup" runat="server" Text="" Visible="false" />
                                    <asp:Label ID="Label_boolAllowManualKill" runat="server" Text="False" Visible="false" />
                                    <asp:Label ID="Label_strOrigSpecie" runat="server" Text="" Visible="false" />
                                    
                                    
                                </th>
                                <td class="form-inline"><asp:TextBox ID="TextBox_PermitNo" class="form-control" runat="server" onClick="setFocusID(this.id)" />&nbsp;<asp:Button CssClass="btn btn-info" ID="Button_Search" runat="server" Text="Search" /></td>
                            </tr>
                            <tr>
                                <th>Flock:</th>
                                <td>
                                   <table class="w3-table w3-border">
                                        <tr>
                                            <td><asp:Label ID="Label_Flock" runat="server" Text="" /></td>
                                            <td><asp:Label ID="Label_SupplierName" runat="server" Text="" /><asp:Label ID="Label_SupplierInt" runat="server" Text=""  Visible="false"/></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td><asp:Label ID="Label_Address1" runat="server" Text="" /></td>
                                        </tr>
                                        <tr>
                                            <td><asp:Label ID="Label_Specie" runat="server" Text="" /></td>
                                            <td><asp:Label ID="Label_Address2" runat="server" Text="" /></td>
                                        </tr>
                                    </table>
                            
                               </td>
                            </tr>
                            <tr>
                                <th>Company:</th>
                                <td><asp:DropDownList ID="DropDownList_Company" runat="server" CssClass="form-control" DataSourceID="SqlDataSource_Company" DataTextField="DescText" DataValueField="DESTNo"></asp:DropDownList>
                                    <asp:SqlDataSource ID="SqlDataSource_Company" runat="server" ConnectionString="<%$ ConnectionStrings:AMPSLindenLambConnectionString %>" SelectCommand="SELECT DESTNo, DescText FROM Destination ORDER BY DescText"></asp:SqlDataSource>
                                </td>
                            </tr>
                            <tr>
                                <th>Specie:</th>
                                <td><asp:DropDownList ID="DropDownList_Specie" runat="server"  CssClass="form-control" DataSourceID="SqlDataSource_Specie" DataTextField="NAME" DataValueField="EC_CODE" ></asp:DropDownList>
                                    <asp:SqlDataSource ID="SqlDataSource_Specie" runat="server" ConnectionString="<%$ ConnectionStrings:AMPSLindenLambConnectionString %>" SelectCommand="SELECT EC_CODE, NAME FROM SEX"></asp:SqlDataSource>
                                </td>
                            </tr>
                            <tr>
                                <td><asp:CheckBox ID="CheckBox_QA" runat="server" Text="QA: " TextAlign="Left" /></td>
                                <td>
                                    <asp:Button ID="Button_Qty_reduce" runat="server" CssClass="btn btn-primary" Text="-" Width="80px" Font-Bold="true"  />
                                    <asp:Button ID="Button_Qty_Reset" runat="server" CssClass="btn btn-danger" Text="Reset" Width="80px"  Font-Bold="true" />
                                    <asp:Button ID="Button_Qty_Plus" runat="server" CssClass="btn btn-success" Text="+" Width="80px"  Font-Bold="true" />
                            
                                &nbsp;<strong><asp:Label ID="Label1" runat="server" Text="Original Qty:" />&nbsp;</strong><asp:Label ID="Label_OriginalQty" CssClass="well well-sm" runat="server" Text="0" style="margin-bottom:3px"  Width="100px"/>
                            
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div class="row">
                                        <div class="col-md-3" style="text-align:center">Qty: <asp:Label ID="Label_Qty" runat="server" CssClass="well well-sm" BorderColor="#00cc66" Text="0" Width="100px" style="margin-bottom:3px" Font-Bold="true" /></div>
                                        <div class="col-md-3" style="text-align:center">Dead on Arrival:<asp:Label ID="Label_Qty_DeadOnArrival" runat="server" CssClass="well well-sm" BorderColor="Red" Text="0" Width="100px"  style="margin-bottom:3px" Font-Bold="true" /></div>
                                        <div class="col-md-3" style="text-align:center">Dead in Pend:<asp:Label ID="Label_Qty_DeadInPen" runat="server" CssClass="well well-sm" BorderColor="Red" Text="0" Width="100px" style="margin-bottom:3px" Font-Bold="true" /></div>
                                        <div class="col-md-3" style="text-align:center">Slaughter Qty:<asp:Label ID="Label_Qty_Slaughter" runat="server" CssClass="well well-sm" BorderColor="Gold" Text="0" Width="100px" style="margin-bottom:3px" Font-Bold="true" /></div>
                                    </div>
                                </td>
                       
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div class="row">
                                        <div class="col-md-6">Batch Number: <asp:Label ID="Label_BatchNo" runat="server" CssClass="well well-sm" style="margin-bottom:3px" Text="0" Width="150px" Font-Bold="true" /></div>
                                        <div class="col-md-6">Next Lot:<asp:Label ID="Label_NextLotNo" runat="server" CssClass="well well-sm" style="margin-bottom:3px" Text="0" Width="150px" Font-Bold="true" /></div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div class="row">
                                        <div class="col-md-6 w3-center"><asp:Button ID="Button_MovetoAbbatoir" runat="server" Text="Move To Abbatoir" CssClass="btn btn-success"  Width="200px" Height="52px" Font-Bold="true" /></div>
                                        <div class="col-md-6 w3-center"><asp:Button ID="Button_Verify" runat="server" Text="Verify" CssClass="btn btn-primary" Width="200px" Height="52px" Font-Bold="true" data-toggle="modal" data-target="#myModal_EnterPassword"  /></div>
                                    </div>
                                </td>
                   
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>

            </div>
            <div class="col-md-4">
                 <asp:UpdatePanel ID="UpdatePanel_Keyboard" runat="server" UpdateMode="Always">
                           <ContentTemplate>

                            <div class="w3-row">
                              <a href="#" onclick="openKeyBoard(event, 'NUMBER');">
                                <div class="w3-half tablink w3-bottombar w3-hover-light-grey w3-padding w3-border-red">NUMBER</div>
                              </a>
                              <a href="#" onclick="openKeyBoard(event, 'ALPHABET');">
                                <div class="w3-half tablink w3-bottombar w3-hover-light-grey w3-padding">ALPHABET</div>
                              </a>
                          
                            </div>

                            <div id="NUMBER" class="w3-container KeyBoard" style="display:block; height:360px">
                                <div class="row" style="margin-top:10px">
                                    <div class="col-sm-4"><asp:Button ID="Button7" runat="server" Text="7" class="btn btn-info KeyBoardStyle" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-4"><asp:Button ID="Button8" runat="server" Text="8" class="btn btn-info KeyBoardStyle" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-4"><asp:Button ID="Button9" runat="server" Text="9" class="btn btn-info KeyBoardStyle" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4"><asp:Button ID="Button4" runat="server" Text="4" class="btn btn-info KeyBoardStyle" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-4"><asp:Button ID="Button5" runat="server" Text="5" class="btn btn-info KeyBoardStyle" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-4"><asp:Button ID="Button6" runat="server" Text="6" class="btn btn-info KeyBoardStyle" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4"><asp:Button ID="Button1" runat="server" Text="1" class="btn btn-info KeyBoardStyle" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-4"><asp:Button ID="Button2" runat="server" Text="2" class="btn btn-info KeyBoardStyle" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-4"><asp:Button ID="Button3" runat="server" Text="3" class="btn btn-info KeyBoardStyle" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4"><asp:Button ID="Button11" runat="server" Text="0" class="btn btn-info KeyBoardStyle" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-4"><asp:Button ID="Button12" runat="server" Text="F" class="btn btn-info KeyBoardStyle" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-4"><asp:Button ID="Button13" runat="server" Text="DEL" class="btn btn-info KeyBoardStyle" OnClientClick="ketPayDelete();return false;" /></div>
                                </div>
                                   <div class="row">
                                    <div class="col-sm-4"><asp:Button ID="Button14" runat="server" Text="." class="btn btn-info KeyBoardStyle" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-4"><asp:Button ID="Button15" runat="server" Text="-" class="btn btn-info KeyBoardStyle" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-4"><asp:Button ID="Button16" runat="server" Text="@" class="btn btn-info KeyBoardStyle" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                </div>
                                <div class="row" style="margin-top:10px;">
                                    <div class="col-sm-12"><asp:Button ID="Button_Enter" ClientIDMode="Static" runat="server" Text="ENTER" class="btn btn-info KeyBoardStyle" Width="245px" /></div>                                  
                                </div>
                            </div>

                            <div id="ALPHABET" class="w3-container KeyBoard" style="height:360px">
                                <div class="row" style="margin-bottom:5px; margin-top:10px">
                                   
                                    <div class="col-sm-3"><asp:Button ID="Button19" runat="server" Text="A" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button21" runat="server" Text="B" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button22" runat="server" Text="C" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button23" runat="server" Text="D" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>      
                                 
                                </div>
                                <div class="row" style="margin-bottom:6px">
                                   
                                    <div class="col-sm-3"><asp:Button ID="Button24" runat="server" Text="E" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button25" runat="server" Text="F" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button26" runat="server" Text="G" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button27" runat="server" Text="H" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>           
                                        
                                 
                                </div>
                                <div class="row" style="margin-bottom:6px">
                                  
                                    <div class="col-sm-3"><asp:Button ID="Button28" runat="server" Text="I" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button29" runat="server" Text="J" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button30" runat="server" Text="K" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button31" runat="server" Text="L" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>    
                                       
                                    
                                </div>
                                <div class="row" style="margin-bottom:6px">
                                  
                                    <div class="col-sm-3"><asp:Button ID="Button32" runat="server" Text="M" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button33" runat="server" Text="N" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button34" runat="server" Text="O" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button35" runat="server" Text="P" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div> 
                                     
                                   
                                </div>
                                <div class="row" style="margin-bottom:6px">
                                  
                                    <div class="col-sm-3"><asp:Button ID="Button36" runat="server" Text="Q" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button37" runat="server" Text="R" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button38" runat="server" Text="S" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button39" runat="server" Text="T" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>

                                    
                                </div>
                                <div class="row" style="margin-bottom:6px">
                                   
                                    <div class="col-sm-3"><asp:Button ID="Button40" runat="server" Text="U" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button41" runat="server" Text="V" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button42" runat="server" Text="W" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button43" runat="server" Text="X" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>

                                </div>
                                <div class="row" style="margin-bottom:10px">     
                                    <div class="col-sm-3"><asp:Button ID="Button44" runat="server" Text="Y" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button45" runat="server" Text="Z" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button46" runat="server" Text=" " class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button47" runat="server" Text="Del" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="ketPayDelete();" /></div>
                                </div>

                            </div>
                           <%-- <table class="w3-table" style="text-align:center; margin-top:5px">
                                <tr>
                                    <td><asp:Button ID="Button_Verify" runat="server" Text="Farm" CssClass="btn btn-success"  Width="120px" Height="50px" /></td>
                                 
                                </tr>
                            </table>      --%>
                               
                           </ContentTemplate>
                       </asp:UpdatePanel>           
            </div>
        </div>
    </div>
    <!--"   " -->
    <div id="myModal_PickBatch" role="dialog"   class="modal fade" >
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Pick Specie </h4>            
        </div>
        <div class="modal-body" >
            <asp:UpdatePanel ID="UpdatePanel_PickBatch" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:Label ID="Label_Modal_PickBatch_New_Update" runat="server" Text="" Visible ="false" ></asp:Label>
                        <asp:GridView ID="GridView_PickBatch" runat="server" DataSourceID="SqlDataSource_Modal_PickBatch" AutoGenerateColumns="False"
                                    ShowHeaderWhenEmpty="True" Caption="Permit Batches" 
                                    CssClass="w3-table w3-striped w3-bordered w3-hoverable w3-card-4 w3-small"
                                    DataKeyNames="EMPLID,EMPLLineID">
                            <Columns>
                                <asp:CommandField ShowSelectButton="True" />
                                <asp:BoundField DataField="EMPLLineID" HeaderText="EMPLLineID" SortExpression="EMPLLineID" />
                                <asp:BoundField DataField="Specie" HeaderText="Specie" SortExpression="Specie" />
                                <asp:BoundField DataField="qty" HeaderText="qty" SortExpression="qty" />
                                <asp:BoundField DataField="batchno" HeaderText="batchno" SortExpression="batchno" />
                            </Columns>
                    </asp:GridView>
                    <asp:SqlDataSource ID="SqlDataSource_Modal_PickBatch" runat="server" ConnectionString="<%$ ConnectionStrings:AMPSLindenLambConnectionString %>" SelectCommand="">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="Label_EMPLID" Name="EMPLID" PropertyName="Text" />
                        </SelectParameters>
                    </asp:SqlDataSource>
<%--                    <div class="form-inline form-group">Destination: <asp:DropDownList ID="DropDownList_Model_PickSpecie_Destination" cssclass="form-control" style="height:auto; font-size:20px;" runat="server" DataSourceID="SqlDataSource_Modal_Destination" DataTextField="DescText" DataValueField="DESTNo" CausesValidation="true" onchange="PickSpecieValidation()"  ></asp:DropDownList>
                       <asp:SqlDataSource ID="SqlDataSource_Modal_Destination" runat="server" ConnectionString="<%$ ConnectionStrings:AMPSLindenLambConnectionString %>" SelectCommand="SELECT DESTNo, DescText FROM Destination ORDER BY DescText"></asp:SqlDataSource>
                        <div id="PickSpecie_modal_Destination_alert" class="alert alert-danger" style="padding-top:3px; padding-bottom:3px; margin-bottom:3px; float:right; display:none">
                            <strong>Warning!</strong> Blank is not allowed in Destination
                        </div>
                    </div>
                    <div>
                           <ul class="w3-ul w3-large"  style="margin-top:10px; margin-bottom:10px" >
                            <li class="w3-hover-red"><asp:CheckBox ID="CheckBox_12months" runat="server" Text=" The animals are less than 12 months old?" onchange="PickSpecieValidation()"  />
                                <div id="PickSpecie_modal_CheckBox12months_alert" class="alert alert-danger" style="padding-top:3px; padding-bottom:3px; margin-bottom:3px; float:right; font-size:15px;display:none">
                                    <strong>Warning!</strong> Required
                                </div>
                            </li>
                            <li class="w3-hover-green"><asp:CheckBox ID="CheckBox_30days" runat="server" Text=" The animals have resided on the last farm 30 days?"  onchange="PickSpecieValidation()"  />
                                 <div id="PickSpecie_modal_CheckBox30days_alert" class="alert alert-danger" style="padding-top:3px; padding-bottom:3px; margin-bottom:3px; float:right; font-size:15px;display:none">
                                    <strong>Warning!</strong> Required
                                </div>
                            </li>
                            <li class="w3-hover-orange"><asp:CheckBox ID="CheckBox_3farm" runat="server" Text=" The animals have been on 3 farms or less?"  onchange="PickSpecieValidation()"  />
                                 <div id="PickSpecie_modal_CheckBox3farm_alert" class="alert alert-danger" style="padding-top:3px; padding-bottom:3px; margin-bottom:3px; float:right; font-size:15px;display:none">
                                    <strong>Warning!</strong> Required
                                </div>
                            </li>
                          </ul>
                    </div>
                      <div class="row" style="margin-bottom:10px">
                           <div class="col-md-4"><asp:Button ID="Button_Modal_PickSpecie_Lamb" CssClass="btn btn-info btn-lg" runat="server" Text="Lamb" Width="200px" Height="60px" CausesValidation="true" ValidationGroup="PickSpecie" OnClientClick="return PickSpecieValidation();" /></div>
                           <div class="col-md-4"><asp:Button ID="Button_Modal_PickSpecie_OrganicLamb" CssClass="btn btn-info btn-lg" runat="server" Text="Organic Lamb" Width="200px" Height="60px"  CausesValidation="true"  /></div>
                           <div class="col-md-4"><asp:Button ID="Button_Modal_PickSpecie_RissLamb" CssClass="btn btn-info btn-lg" runat="server" Text="Rissington Lamb" Width="200px" Height="60px"  CausesValidation="true"  /></div>
                       </div>
                        <div class="row">
                           <div class="col-md-4"><asp:Button ID="Button_Modal_PickSpecie_Hogget" CssClass="btn btn-info btn-lg" runat="server" Text="Hogget" Width="200px" Height="60px"  CausesValidation="true"  /></div>
                           <div class="col-md-4"><asp:Button ID="Button_Modal_PickSpecie_Ram" CssClass="btn btn-info btn-lg" runat="server" Text="Ram" Width="200px" Height="60px"  CausesValidation="true" /></div>
                           <div class="col-md-4"><asp:Button ID="Button_Modal_PickSpecie_Eve" CssClass="btn btn-info btn-lg" runat="server" Text="Eve" Width="200px" Height="60px"  CausesValidation="true"  /></div>
                       </div>
          --%>
                     
                </ContentTemplate>
            </asp:UpdatePanel>
           
        </div>
        <div class="modal-footer" style="padding-top:5px;"></div>
      </div>
    </div>
  </div>
     <div class="modal fade" id="myModal_EnterPassword" role="dialog" >
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Enter Password:</h4>            
        </div>
        <div class="modal-body" >
            <asp:UpdatePanel ID="UpdatePanel_Verify" runat="server">
                <ContentTemplate>
                    <div class="form-inline">
                        <asp:TextBox ID="TextBox_Password" runat="server" CssClass="form-control" TextMode="Password"></asp:TextBox>
                        <asp:Button ID="Button_Submit" CssClass="btn btn-info" runat="server" Text="Submit" />
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>        
        </div>
        <div class="modal-footer" style="padding-top:5px;"></div>
      </div>
    </div>
  </div>
     <div class="modal fade" id="myModal_AlertMessage" role="dialog" >
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Alert Message</h4>            
        </div>
        <div class="modal-body" >
            <asp:UpdatePanel ID="UpdatePanel_Alert" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                   <div class="alert alert-danger">
                        <strong>Warning! </strong><asp:Label ID="Label_AlertMessage" runat="server" Text=""></asp:Label>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>        
        </div>
        <div class="modal-footer" style="padding-top:5px;"></div>
      </div>
    </div>
  </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Footer" Runat="Server">
</asp:Content>

