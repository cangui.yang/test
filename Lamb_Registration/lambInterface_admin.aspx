﻿<%@ Page Title="" Language="VB" MasterPageFile="~/LambInterface.master" AutoEventWireup="false" CodeFile="lambInterface_admin.aspx.vb" Inherits="lambInterface_admin" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script>
        function UpdateAlertClass(value) {
            document.getElementById("DivAlertMessage").className = value;
        }
        function openInformationPanel(panel,Id) {
            var i, x, tablinks;
            x = document.getElementsByClassName(panel);
            for (i = 0; i < x.length; i++) {
                x[i].style.display = "none";
            }

            document.getElementById(Id).style.display = "block";

        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
     <div class="container">
         <asp:UpdatePanel ID="UpdatePanel1" runat="server">
             <ContentTemplate>
                  <div class="row">
                 <div class="col-sm-4">
                        <asp:Button ID="Button_ReturnLastLotToLairage" runat="server" CssClass="btn btn-info btn-lg" Width="350px" Height="100px" Font-Bold="true" style="margin:10px"  Text="Return Last Lot to Lairage" />
                 </div>
                 <div class="col-sm-4"> 
                        <asp:Button ID="Button_ChangeQtyInAnAbbatoir" runat="server" CssClass="btn btn-info btn-lg" Width="350px" Height="100px" Font-Bold="true" style="margin:10px" OnClientClick="return fase" data-toggle="modal" data-target="#Modal_ChangeQtyInAbbatoir"  Text="Change Qty in an Abbatoir" Enabled="False"  />
                 </div>
                 <div class="col-sm-4">
                        <asp:Button ID="Button_RemoveLotFromLairage" runat="server" CssClass="btn btn-info btn-lg" Width="350px" Height="100px" Font-Bold="true" style="margin:10px" data-toggle="modal" data-target="#Modal_RemoveLotFromLairage"  Text="Remove Lot From Lairage" Enabled="False" />
                 </div>
                
            </div>
            <div class="row">
                 <div class="col-sm-4"><asp:Button ID="Button_ViewLairageList" runat="server" CssClass="btn btn-info btn-lg" Width="350px" Height="100px" Font-Bold="true" style="margin:10px" Text="View Lairage List" data-toggle="modal" data-target="#Modal_ViewLairageList" /></div>
                 <div class="col-sm-4"><asp:Button ID="Button_RemovePermitFrom" runat="server" CssClass="btn btn-info btn-lg" Width="350px" Height="100px" Font-Bold="true" style="margin:10px" Text="Remove Permit from System"  data-toggle="modal" data-target="#Modal_RemovePermitFromSystem" /></div>
                 <div class="col-sm-4"><asp:Button ID="Button_ChangePermitDate" runat="server" CssClass="btn btn-info btn-lg" Width="350px" Height="100px" Font-Bold="true" style="margin:10px" Text="Change Permit Date"  data-toggle="modal" data-target="#Modal_ChangePermitDate" /></div>
            </div>   
                
             </ContentTemplate>
         </asp:UpdatePanel>
           
     </div>
     &nbsp;
   <!-- Modal    -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Alert Message</h4>
      </div>
      <div class="modal-body">
          <asp:UpdatePanel ID="UpdatePanel_Alert" runat="server" UpdateMode="Conditional">
              <ContentTemplate>
                  <div id="DivAlertMessage" class="alert alert-danger">
                      <p><asp:Label ID="Label_AlertMessage" runat="server" Text="" /></p>
                  </div>
                  
                  <asp:Label ID="Label_Application" runat="server" Text="" Visible="false"></asp:Label>
                  <asp:Button ID="Button_Alert_Yes" runat="server" Text="Yes" CssClass="btn btn-success btn-lg" Width="100px" Visible="false"  />
                  &nbsp;<asp:Button ID="Button_Alert_No" runat="server" Text="No" CssClass="btn btn-warning btn-lg" Width="100px"  data-dismiss="modal"  Visible="false" />
              </ContentTemplate>           
          </asp:UpdatePanel> 
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

    <!-- Modal class="modal fade" -->
<div id="Modal_ChangeQtyInAbbatoir"  class="modal fade" role="dialog">
  <div class="modal-dialog  modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
         <h4 class="modal-title">Change Qty in Abattoir</h4>
      </div>
      <div class="modal-body">
                  <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                  <ContentTemplate>  
                      <div id="SubmitLotNo_ChangeQtyInAbbatoir" class="informationPanel" style="display:block">

                                    <div class="form-inline">
                                        <asp:TextBox ID="TextBox_Batchno_Modal_ChangeQtyInAbbatoir" runat="server" placeholder="Enter Lot number that requires qty change" Width="400px" CssClass="form-control" TextMode="Number" ></asp:TextBox>
                                        &nbsp;<asp:Button ID="Button_SubmitBatchno_Modal_ChangeQtyInAbbatoir" runat="server" Text="Submit" CssClass="btn btn-info" />
                                   </div>
                                    <asp:Label ID="Label_Alert_ChangeQtyInAbbatoir" runat="server" Text=""></asp:Label>
                      </div>
                      <div id="ChangeAbbQty_ChangeQtyInAbbatoir" class="informationPanel" style="display:block" >
                           <div class="row">
                               <div class="col-sm-3">LotNo:</div>
                               <div class="col-sm-9"><asp:Label ID="Label_LotNo_ChangeQtyInAbbatoir" runat="server"  Text=""></asp:Label></div>
                           </div>
                           <div class="row">
                               <div class="col-sm-3">Original <asp:TextBox ID="TextBox_Original_ChangeQtyInAbbatoir" runat="server" CssClass="form-control"></asp:TextBox></div>
                               <div class="col-sm-3">Current/Terminal <asp:TextBox ID="TextBox_Current__ChangeQtyInAbbatoir" runat="server" CssClass="form-control"></asp:TextBox></div>
                               <div class="col-sm-3">New <asp:TextBox ID="TextBox_NewQty__ChangeQtyInAbbatoir" runat="server" CssClass="form-control"></asp:TextBox></div>
                               <div class="col-sm-3"><asp:Button ID="Button_Plus_ChangeQtyInAbbatoir" runat="server" Text="+" CssClass="btn btn-primary btn-lg"  Width="60px"  />&nbsp;<asp:Button ID="Button_Reduce_ChangeQtyInAbbatoir" CssClass="btn btn-success btn-lg" runat="server" Width="60px" Text="-" /></div>
                           </div><br />
                           <asp:Button ID="Button_Update_ChangeQtyInAbbatoir" runat="server" Text="Update" CssClass="btn btn-primary btn-lg" />
                      </div>
             
                  </ContentTemplate>
              </asp:UpdatePanel>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
    
<div id="Modal_RemoveLotFromLairage" class="modal fade" role="dialog">
  <div class="modal-dialog  modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
         <h4 class="modal-title">Change Qty in Abattoir</h4>
      </div>
      <div class="modal-body">
                  <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                  <ContentTemplate>  
                   
                                    <div class="form-inline">
                                        <asp:TextBox ID="TextBox_BatchNo_RemoveLotFromLairage" runat="server" placeholder="Enter batchno to zero qty and move to Abattoir" Width="400px" CssClass="form-control" TextMode="Number" ></asp:TextBox>
                                        &nbsp;<asp:Button ID="Button_BatchNo_RemoveLotFromLairage" runat="server" Text="Submit" CssClass="btn btn-info" />
                                   </div>
                                    <%--<asp:Label ID="Label1" runat="server" Text=""></asp:Label>--%>
                                     
                  </ContentTemplate>
              </asp:UpdatePanel>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
 
<!--   -->
<div id="Modal_ViewLairageList" class="modal fade" role="dialog">
  <div class="modal-dialog  modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
         <h4 class="modal-title">View LairageList</h4>
      </div>
      <div class="modal-body">
                  <asp:UpdatePanel ID="UpdatePanel_ViewLairageList" runat="server" UpdateMode="Conditional">
                  <ContentTemplate>  
                 <div class="form-inline">
                     <asp:Label ID="Label1" runat="server" Text="Book Date:"></asp:Label>
                     &nbsp;<asp:TextBox ID="TextBox_Date_ViewLairageList" CssClass="form-control" runat="server"></asp:TextBox>
                     <ajaxToolkit:CalendarExtender ID="TextBox_Date_ViewLairageList_CalendarExtender" runat="server" BehaviorID="TextBox_Date_ViewLairageList_CalendarExtender" TargetControlID="TextBox_Date_ViewLairageList" Format="dd/MM/yyyy" />
                     &nbsp;<asp:Button ID="Button_LoadViewLairageList" runat="server" CssClass="btn btn-primary" Text="Get Lairage List" />
                 </div>
                      <asp:GridView ID="GridView_ViewLairageList" runat="server"></asp:GridView>
                  </ContentTemplate>
              </asp:UpdatePanel>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

    <!---->
    <div id="Modal_RemovePermitFromSystem"  class="modal fade" role="dialog">
  <div class="modal-dialog  modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
         <h4 class="modal-title">Remove Permit From Our System</h4>
      </div>
      <div class="modal-body">
                  <asp:UpdatePanel ID="UpdatePanel_RemovePermitFromSystem" runat="server" UpdateMode="Conditional">
                  <ContentTemplate>  
                 <div class="form-inline">
                     <asp:Label ID="Label2" runat="server" Text="Permit Number:"></asp:Label>
                     &nbsp;<asp:TextBox ID="TextBox_PermitNo_RemovePermitFromSystem" CssClass="form-control" runat="server" Width="300px" placeholder="Enter permit that you want to delete"></asp:TextBox>              
                     &nbsp;<asp:Button ID="Button_Submit_RemovePermitFromSystem" runat="server" CssClass="btn btn-primary" Text="Submit" />
                 </div>                 
                      <asp:Label ID="Label_Alert_RemovePermitFromSystem" runat="server" Text=""></asp:Label>
                  </ContentTemplate>
              </asp:UpdatePanel>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!--     -->
<div id="Modal_ChangePermitDate" class="modal fade" role="dialog">
  <div class="modal-dialog  modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
         <h4 class="modal-title">Change Permit Date</h4>
      </div>
      <div class="modal-body">
                  <asp:UpdatePanel ID="UpdatePanel_ChangePermitDate" runat="server" UpdateMode="Conditional">
                  <ContentTemplate>  
                 <div class="form-inline">
                     <asp:Label ID="Label3" runat="server" Text="Permit Number:"></asp:Label>
                     &nbsp;<asp:TextBox ID="TextBox_PermitNo_ChangePermitDate" CssClass="form-control" runat="server" Width="300px" placeholder="Enter permit number"></asp:TextBox>              
                     <asp:TextBox ID="TextBox_Modal_NewPermitdate_ChangePermitDate" runat="server" CssClass="form-control" TextMode="Date"></asp:TextBox>
                     <ajaxToolkit:CalendarExtender ID="TextBox_Modal_NewPermitdate_CalendarExtender" runat="server" BehaviorID="TextBox_Modal_NewPermitdate_CalendarExtender" TargetControlID="TextBox_Modal_NewPermitdate_ChangePermitDate" Format="dd/MM/yyyy" />
                     &nbsp;<asp:Button ID="Button_Modal_ChangePermitDate" runat="server" CssClass="btn btn-primary" Text="Submit" />
                 </div>                 
                      <asp:Label ID="Label_ChangePermitDateAlert" runat="server" Text=""></asp:Label>
                  </ContentTemplate>
              </asp:UpdatePanel>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Footer" Runat="Server">
</asp:Content>

