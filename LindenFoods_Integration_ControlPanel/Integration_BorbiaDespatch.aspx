﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" CodeFile="Integration_BorbiaDespatch.aspx.vb" Inherits="Integration_BorbiaDespatch" %>
<%--<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="ajaxToolkit" %>--%>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">

    <div class="panel panel-Info">
  <div class="panel-heading"><h3 class="panel-title">Integration Details</h3></div>
  <div class="panel-body">
      <table class="table table-bordered">
          <tr>
              <td style="text-align: justify"><b>Hosting Server:</b></td>
              <td>LF-SQL01\UTILITY1, Integration Services Catalogs</td>
          </tr>
          <tr>
              <td style="text-align: justify"><b>SSISDB Project:</b></td>
              <td>BordbiaDespatchUpload</td>
          </tr>
          <tr>
              <td style="text-align: justify"><b>Integration Package:</b></td>
              <td style="text-align: left">
                  <ul>
                      <li>Integration_BorbiaDespatch_Retail</li>
                      <li>Integration_BorbiaDespatch_Primal</li>
                  </ul>
              </td>
          </tr>
          <tr>
              <td style="text-align: justify"><b>SQL Database:</b></td>
              <td>[LF-SQL01\UTILITY1].LindenITWeb</td>
          </tr>
          <tr>
              <td style="text-align: justify"><b>Table Ref:</b></td>
              <td>
                  <ul>
                      <li>tbl_BordBiaDespatch_Certificate</li>
                      <li>tbl_BordbiaDespatchUploadConfig</li>
                  </ul>
              </td>
          </tr>
           <tr>
              <td style="text-align: justify"><b>Stored Procedures</b></td>
              <td>
                  <ul>
                      <li>sp_BordBiaDespatchAutomation_Primal</li>
                      <li>sp_BordBiaDespatchAutomation_Retail</li>
                  </ul>
               </td>
          </tr>
          <tr>
              <td style="text-align: justify"><b>SQL Server Agent Job:</b></td>
              <td>
                  <ul>
                      <li>Integration_BorbiaDespatch_Retail</li>
                      <li>Integration_BorbiaDespatch_Primal</li>
                  </ul>
              </td>
          </tr>

           <tr>
              <td style="text-align: justify; height: 66px;"><b>Data Source:</b></td>
              <td style="height: 66px">
                  <ul>
                      <li>[LF7].SI.dbo.vw_BordBiaDespatchStocks</li>
                      <li>[LFRET-SVR-INNOVA].Innova.dbo.vw_BordBiaDespatchStocks</li>
                      <li>Certification (<asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="\\lf-public\Granville-Public\BordBia Cert\">\\lf-public\Granville-Public\BordBia Cert\</asp:HyperLink>
                          )</li>
                  </ul>
               </td>
          </tr>
          <tr>
              <td><strong>Add New Setting:</strong></td>
              <td>
                  <table>
                      <tr>
                          <td>Site:</td>
                          <td>
                              <asp:DropDownList ID="DropDownList_Site" runat="server" AutoPostBack="True">
                                  <asp:ListItem>Primal</asp:ListItem>
                                  <asp:ListItem>Retail</asp:ListItem>
                                  <asp:ListItem>Kettyle</asp:ListItem>
                              </asp:DropDownList>
                          </td>
                          <td>ConfigType</td>
                          <td>
                              <asp:DropDownList ID="DropDownList_Type" runat="server" AutoPostBack="True">
                                  <asp:ListItem>Beef</asp:ListItem>
                              </asp:DropDownList>
                          </td>
                      </tr>
                       <tr>
                          <td>ConfigItem:</td>
                          <td>
                              <asp:DropDownList ID="DropDownList_Item" runat="server" AutoPostBack="True">
                                  <asp:ListItem>RunNumber</asp:ListItem>
                                  <asp:ListItem>CustomerCode</asp:ListItem>
                              </asp:DropDownList>
                           </td>
                            <td>ConfigValue:</td>
                          <td>
                              <asp:TextBox ID="TextBox_ConfigValue" runat="server"></asp:TextBox>
                           </td>
                      </tr>
                      <tr>
                          <td></td>
                          <td></td>
                            <td>ConfigValueExt1:</td>
                          <td>
                              <asp:TextBox ID="TextBox_ConfigValueExt1" runat="server"></asp:TextBox>
                          </td>
                      </tr>
                      <tr>
                          <td colspan="4" style="text-align:center">
                              <asp:Button ID="Button_Submit" runat="server" Text="Submit" />
                          </td>
                      </tr>
                      </table>

              </td>
          </tr>

      </table>
     </div>
</div>

    <div class="panel panel-Success">
  <div class="panel-heading"><h3 class="panel-title">Retail <asp:Button ID="Button_RetailUpdate" runat="server" Text="Run Borbia Upload" /></h3></div>
  <div class="panel-body">

      <asp:GridView ID="GridView1" runat="server" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="Id" DataSourceID="SqlDataSource_Bordbia_RetailSetup" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="4" ForeColor="Black" GridLines="Horizontal" EmptyDataText="Not Yet Setup ">
          <Columns>
              <asp:CommandField ShowEditButton="True" ShowDeleteButton="True" />
      
              <asp:BoundField DataField="ConfigItem" HeaderText="ConfigItem" SortExpression="ConfigItem" />
              <asp:BoundField DataField="ConfigValue" HeaderText="ConfigValue" SortExpression="ConfigValue" />
              <asp:BoundField DataField="ConfigType" HeaderText="ConfigType" SortExpression="ConfigType" />
              <asp:BoundField DataField="CreatedDateTime" HeaderText="CreatedDateTime" SortExpression="CreatedDateTime" />
              <asp:BoundField DataField="Site" HeaderText="Site" SortExpression="Site" />
              <asp:BoundField DataField="ConfigValueExt1" HeaderText="ConfigValueExt1" SortExpression="ConfigValueExt1" />
          </Columns>
          <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
          <HeaderStyle BackColor="#333333" Font-Bold="True" ForeColor="White" />
          <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right" />
          <SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
          <SortedAscendingCellStyle BackColor="#F7F7F7" />
          <SortedAscendingHeaderStyle BackColor="#4B4B4B" />
          <SortedDescendingCellStyle BackColor="#E5E5E5" />
          <SortedDescendingHeaderStyle BackColor="#242121" />
      </asp:GridView>
      <asp:SqlDataSource ID="SqlDataSource_Bordbia_RetailSetup" runat="server" ConnectionString="<%$ ConnectionStrings:LindenITWebConnectionString %>" DeleteCommand="DELETE FROM [tbl_BordbiaDespatchUploadConfig] WHERE [Id] = @Id" InsertCommand="INSERT INTO [tbl_BordbiaDespatchUploadConfig] ([ConfigItem], [ConfigValue], [ConfigType], [CreatedDateTime], [Site], [ConfigValueExt1]) VALUES (@ConfigItem, @ConfigValue, @ConfigType, @CreatedDateTime, @Site, @ConfigValueExt1)" SelectCommand="SELECT [Id], [ConfigItem], [ConfigValue], [ConfigType], [CreatedDateTime], [Site], [ConfigValueExt1] FROM [tbl_BordbiaDespatchUploadConfig] WHERE ([Site] = @Site) ORDER BY [ConfigType]" UpdateCommand="UPDATE [tbl_BordbiaDespatchUploadConfig] SET [ConfigItem] = @ConfigItem, [ConfigValue] = @ConfigValue, [ConfigType] = @ConfigType, [CreatedDateTime] = @CreatedDateTime, [Site] = @Site, [ConfigValueExt1] = @ConfigValueExt1 WHERE [Id] = @Id">
          <DeleteParameters>
              <asp:Parameter Name="Id" Type="Int32" />
          </DeleteParameters>
          <InsertParameters>
              <asp:Parameter Name="ConfigItem" Type="String" />
              <asp:Parameter Name="ConfigValue" Type="String" />
              <asp:Parameter Name="ConfigType" Type="String" />
              <asp:Parameter Name="CreatedDateTime" Type="DateTime" />
              <asp:Parameter Name="Site" Type="String" />
              <asp:Parameter Name="ConfigValueExt1" Type="String" />
          </InsertParameters>
          <SelectParameters>
              <asp:Parameter DefaultValue="Retail" Name="Site" Type="String" />
          </SelectParameters>
          <UpdateParameters>
              <asp:Parameter Name="ConfigItem" Type="String" />
              <asp:Parameter Name="ConfigValue" Type="String" />
              <asp:Parameter Name="ConfigType" Type="String" />
              <asp:Parameter Name="CreatedDateTime" Type="DateTime" />
              <asp:Parameter Name="Site" Type="String" />
              <asp:Parameter Name="ConfigValueExt1" Type="String" />
              <asp:Parameter Name="Id" Type="Int32" />
          </UpdateParameters>
      </asp:SqlDataSource>

  </div>
</div>
<div class="panel panel-Info">
  <div class="panel-heading"><h3 class="panel-title">Primal <asp:Button ID="Button_PrimalUpdate" runat="server" Text="Run Borbia Upload" /></h3></div>
  <div class="panel-body">
      <asp:GridView ID="GridView2" runat="server" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="4" ForeColor="Black" GridLines="Horizontal" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="Id" DataSourceID="SqlDataSource_Primal" EmptyDataText="Not Yet Setup ">
          <Columns>
              <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
              <asp:BoundField DataField="ConfigItem" HeaderText="ConfigItem" SortExpression="ConfigItem" />
              <asp:BoundField DataField="ConfigValue" HeaderText="ConfigValue" SortExpression="ConfigValue" />
              <asp:BoundField DataField="ConfigType" HeaderText="ConfigType" SortExpression="ConfigType" />
              <asp:BoundField DataField="CreatedDateTime" HeaderText="CreatedDateTime" SortExpression="CreatedDateTime" />
              <asp:BoundField DataField="Site" HeaderText="Site" SortExpression="Site" />
              <asp:BoundField DataField="ConfigValueExt1" HeaderText="ConfigValueExt1" SortExpression="ConfigValueExt1" />
          </Columns>
          <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
          <HeaderStyle BackColor="#333333" Font-Bold="True" ForeColor="White" />
          <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right" />
          <SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
          <SortedAscendingCellStyle BackColor="#F7F7F7" />
          <SortedAscendingHeaderStyle BackColor="#4B4B4B" />
          <SortedDescendingCellStyle BackColor="#E5E5E5" />
          <SortedDescendingHeaderStyle BackColor="#242121" />
      </asp:GridView>
      <asp:SqlDataSource ID="SqlDataSource_Primal" runat="server" ConnectionString="<%$ ConnectionStrings:LindenITWebConnectionString %>" DeleteCommand="DELETE FROM [tbl_BordbiaDespatchUploadConfig] WHERE [Id] = @Id" InsertCommand="INSERT INTO [tbl_BordbiaDespatchUploadConfig] ([ConfigItem], [ConfigValue], [ConfigType], [CreatedDateTime], [Site], [ConfigValueExt1]) VALUES (@ConfigItem, @ConfigValue, @ConfigType, @CreatedDateTime, @Site, @ConfigValueExt1)" SelectCommand="SELECT [Id], [ConfigItem], [ConfigValue], [ConfigType], [CreatedDateTime], [Site], [ConfigValueExt1] FROM [tbl_BordbiaDespatchUploadConfig] WHERE ([Site] = @Site) ORDER BY [ConfigType]" UpdateCommand="UPDATE [tbl_BordbiaDespatchUploadConfig] SET [ConfigItem] = @ConfigItem, [ConfigValue] = @ConfigValue, [ConfigType] = @ConfigType, [CreatedDateTime] = @CreatedDateTime, [Site] = @Site, [ConfigValueExt1] = @ConfigValueExt1 WHERE [Id] = @Id">
          <DeleteParameters>
              <asp:Parameter Name="Id" Type="Int32" />
          </DeleteParameters>
          <InsertParameters>
              <asp:Parameter Name="ConfigItem" Type="String" />
              <asp:Parameter Name="ConfigValue" Type="String" />
              <asp:Parameter Name="ConfigType" Type="String" />
              <asp:Parameter Name="CreatedDateTime" Type="DateTime" />
              <asp:Parameter Name="Site" Type="String" />
              <asp:Parameter Name="ConfigValueExt1" Type="String" />
          </InsertParameters>
          <SelectParameters>
              <asp:Parameter DefaultValue="Primal" Name="Site" Type="String" />
          </SelectParameters>
          <UpdateParameters>
              <asp:Parameter Name="ConfigItem" Type="String" />
              <asp:Parameter Name="ConfigValue" Type="String" />
              <asp:Parameter Name="ConfigType" Type="String" />
              <asp:Parameter Name="CreatedDateTime" Type="DateTime" />
              <asp:Parameter Name="Site" Type="String" />
              <asp:Parameter Name="ConfigValueExt1" Type="String" />
              <asp:Parameter Name="Id" Type="Int32" />
          </UpdateParameters>
      </asp:SqlDataSource>
    </div>
</div>
<div class="panel panel-Info">
  <div class="panel-heading"><h3 class="panel-title">Kettyle </h3></div>
  <div class="panel-body">
        <asp:GridView ID="GridView3" runat="server" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="4" ForeColor="Black" GridLines="Horizontal" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="Id" DataSourceID="SqlDataSource_Kettyle" EmptyDataText="Not Yet Setup ">
          <Columns>
              <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
              <asp:BoundField DataField="ConfigItem" HeaderText="ConfigItem" SortExpression="ConfigItem" />
              <asp:BoundField DataField="ConfigValue" HeaderText="ConfigValue" SortExpression="ConfigValue" />
              <asp:BoundField DataField="ConfigType" HeaderText="ConfigType" SortExpression="ConfigType" />
              <asp:BoundField DataField="CreatedDateTime" HeaderText="CreatedDateTime" SortExpression="CreatedDateTime" />
              <asp:BoundField DataField="Site" HeaderText="Site" SortExpression="Site" />
              <asp:BoundField DataField="ConfigValueExt1" HeaderText="ConfigValueExt1" SortExpression="ConfigValueExt1" />
          </Columns>
          <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
          <HeaderStyle BackColor="#333333" Font-Bold="True" ForeColor="White" />
          <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right" />
          <SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
          <SortedAscendingCellStyle BackColor="#F7F7F7" />
          <SortedAscendingHeaderStyle BackColor="#4B4B4B" />
          <SortedDescendingCellStyle BackColor="#E5E5E5" />
          <SortedDescendingHeaderStyle BackColor="#242121" />
      </asp:GridView>
      <asp:SqlDataSource ID="SqlDataSource_Kettyle" runat="server" ConnectionString="<%$ ConnectionStrings:LindenITWebConnectionString %>" DeleteCommand="DELETE FROM [tbl_BordbiaDespatchUploadConfig] WHERE [Id] = @Id" InsertCommand="INSERT INTO [tbl_BordbiaDespatchUploadConfig] ([ConfigItem], [ConfigValue], [ConfigType], [CreatedDateTime], [Site], [ConfigValueExt1]) VALUES (@ConfigItem, @ConfigValue, @ConfigType, @CreatedDateTime, @Site, @ConfigValueExt1)" SelectCommand="SELECT [Id], [ConfigItem], [ConfigValue], [ConfigType], [CreatedDateTime], [Site], [ConfigValueExt1] FROM [tbl_BordbiaDespatchUploadConfig] WHERE ([Site] = @Site) ORDER BY [ConfigType]" UpdateCommand="UPDATE [tbl_BordbiaDespatchUploadConfig] SET [ConfigItem] = @ConfigItem, [ConfigValue] = @ConfigValue, [ConfigType] = @ConfigType, [CreatedDateTime] = @CreatedDateTime, [Site] = @Site, [ConfigValueExt1] = @ConfigValueExt1 WHERE [Id] = @Id">
          <DeleteParameters>
              <asp:Parameter Name="Id" Type="Int32" />
          </DeleteParameters>
          <InsertParameters>
              <asp:Parameter Name="ConfigItem" Type="String" />
              <asp:Parameter Name="ConfigValue" Type="String" />
              <asp:Parameter Name="ConfigType" Type="String" />
              <asp:Parameter Name="CreatedDateTime" Type="DateTime" />
              <asp:Parameter Name="Site" Type="String" />
              <asp:Parameter Name="ConfigValueExt1" Type="String" />
          </InsertParameters>
          <SelectParameters>
              <asp:Parameter DefaultValue="Kettyle" Name="Site" Type="String" />
          </SelectParameters>
          <UpdateParameters>
              <asp:Parameter Name="ConfigItem" Type="String" />
              <asp:Parameter Name="ConfigValue" Type="String" />
              <asp:Parameter Name="ConfigType" Type="String" />
              <asp:Parameter Name="CreatedDateTime" Type="DateTime" />
              <asp:Parameter Name="Site" Type="String" />
              <asp:Parameter Name="ConfigValueExt1" Type="String" />
              <asp:Parameter Name="Id" Type="Int32" />
          </UpdateParameters>
      </asp:SqlDataSource>
  </div>
</div>
</asp:Content>

