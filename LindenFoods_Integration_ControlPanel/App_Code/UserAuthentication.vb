﻿Imports Microsoft.VisualBasic
Imports System.Security.Principal

Public Module UserAuthentication
    Function getGroupsList(username As String) As List(Of String)
        Dim result As New List(Of String)
        Dim wi As WindowsIdentity = New WindowsIdentity(username)
        For Each group As IdentityReference In wi.Groups
            Try

                result.Add(group.Translate(GetType(NTAccount)).ToString())
                '    MsgBox(group.Translate(GetType(NTAccount)).ToString())
                '   MsgBox(group.Translate(GetType(NTAccount)).ToString())
            Catch ex As Exception
                ' MsgBox(ex.Message)
            End Try        Next        result.Sort()
        Return result
    End Function
    Function isAuthentication(username As String, Groupstr As String) As Boolean
        Dim l As New List(Of String)
        l = getGroupsList(username)
        '   MsgBox(1)
        ' If l.Contains("LINDEN\TravelBooking") Then
        ' MsgBox("TravelBooking")
        ' End If
        Return l.Contains("LINDEN\" & Groupstr)

    End Function
End Module
