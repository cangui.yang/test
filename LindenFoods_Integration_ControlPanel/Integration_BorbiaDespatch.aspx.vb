﻿Imports System.Data

Partial Class Integration_BorbiaDespatch
    Inherits System.Web.UI.Page

    Protected Sub Button_Submit_Click(sender As Object, e As EventArgs) Handles Button_Submit.Click
        Dim ExtValue As String = TextBox_ConfigValueExt1.Text
        If TextBox_ConfigValueExt1.Text = "" Then
            executeNonQuery("Insert into (ConfigItem,ConfigValue,ConfigType,Site) values ('" & DropDownList_Item.SelectedItem.Text & "','" & TextBox_ConfigValue.Text & "','" & DropDownList_Type.SelectedItem.Text & "','" & DropDownList_Site.SelectedItem.Text & "')")
        Else
            executeNonQuery("Insert into (ConfigItem,ConfigValue,ConfigType,Site,ConfigValueExt1) values ('" & DropDownList_Item.SelectedItem.Text & "','" & TextBox_ConfigValue.Text & "','" & DropDownList_Type.SelectedItem.Text & "','" & DropDownList_Site.SelectedItem.Text & "','" & TextBox_ConfigValueExt1.Text & "')")
        End If

    End Sub
    Public Sub executeNonQuery(ByVal strSQL As String)

        Dim SQLCommand As SqlClient.SqlCommand

        Try

            SQLCommand = New SqlClient.SqlCommand(strSQL, New SqlClient.SqlConnection("Initial catalog=[LF-SQL01\UTILITY1];data source=LindenITWeb;user id=lfintegration;password=linden1*;"))
            SQLCommand.CommandTimeout = 140
            SQLCommand.Connection.Open()
            SQLCommand.ExecuteNonQuery()
            SQLCommand.Connection.Close()
            SQLCommand.Dispose()

        Catch objError As System.Data.SqlClient.SqlException
            MsgBox(objError.Message)
            ' EmailRetailSupport(objError.Message & vbCrLf & vbCrLf & strSQL)

        Catch
            MsgBox(Err.Description)
            ' EmailRetailSupport(Err.Description & vbCrLf & vbCrLf & strSQL)

        End Try

    End Sub
    Protected Sub Button_RetailUpdate_Click(sender As Object, e As EventArgs) Handles Button_RetailUpdate.Click
        executeNonQuery("exec sp_BordBiaDespatchAutomation_Retail")
    End Sub
    Protected Sub Button_PrimalUpdate_Click(sender As Object, e As EventArgs) Handles Button_PrimalUpdate.Click
        executeNonQuery("exec sp_BordBiaDespatchAutomation_Primal")
    End Sub
End Class
