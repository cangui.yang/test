﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" CodeFile="Integration_Primal.aspx.vb" Inherits="Integration_Primal" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">

    <div class="container">
        <h2>Primal Integration</h2>
        <p><strong>Note:Source Location \\LF-VM-TEST01\Visual Studio 2015</strong></p>
        <div class="panel-group" id="accordion">


            <div class="panel panel-info">
                <div class="panel-heading">
                    <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse11">SSIS Integration - ICM Export Integration</a></h4>
                </div>
                <div id="collapse11" class="panel-collapse collapse">
                    <div class="panel-body">
                        <table class="table">
                            <tr>
                                <td style="width: 25%"><strong>Hosting Server:</strong></td>
                                <td style="width: 25%" class="warning">LF-SQL01\INNOVA</td>
                                <td style="width: 25%" class="danger"><strong>Advance Info:</strong></td>
                                <td style="width: 25%" class="success"><strong>How to Resend the ICM File?</strong></td>
                            </tr>
                            <tr>
                                <td><strong>Integration Services Catalogs:</strong></td>
                                <td class="warning">ICM Export</td>

                                <td style="width: 25%" rowspan="5" class="danger">Please note,Records shown below are on the last 10 days, if you want to resend file that older than 10 days, please contact sec line support.</td>
                                <td style="width: 25%" rowspan="5" class="success">Click the &quot;Resend&quot; button next to the date you want to resend.</td>

                            </tr>
                            <tr>
                                <td><strong>Integration Services Project:</strong></td>
                                <td class="warning">Integration_ICM_LindenPrimal</td>
                            </tr>
                            <tr>
                                <td style="height: 37px"><strong>Integration Services Packages:</strong></td>
                                <td class="warning" style="height: 37px">Package.dtsx</td>
                            </tr>
                            <tr>
                                <td><strong>SQL Agent Job:</strong></td>
                                <td class="warning">Export PrimalLamb to ICM</td>
                            </tr>
                            <%--<asp:Parameter Name="Site" Type="String" />--%>
                            <tr>
                                <td><strong>Flow Chart:</strong></td>
                                <td class="warning">
                                    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="\\lf-public\it\Application Integration Design & Doc\Burradon-ICM.vsd" Target="_blank">View Flow Chart</asp:HyperLink>
                                </td>
                            </tr>
                        </table>
                        <asp:UpdatePanel ID="UpdatePanel_ICM" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:SqlDataSource ID="SqlDataSource_ICM" runat="server" ConnectionString="<%$ ConnectionStrings:PrimalInnovaConnectionString %>" SelectCommand="SELECT DISTINCT dispatchtime, PlantNumber FROM vw_ITReport_ICM_New WHERE (dispatchtime &gt;= GETDATE()-10)"></asp:SqlDataSource>
                                <asp:GridView ID="GridView_ICM" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource_ICM" AllowSorting="True" DataKeyNames="dispatchtime" class="table">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="LinkButton_ResendICM" runat="server" OnClick="LinkButton_ResendICM_Click">ReSend</asp:LinkButton>
                                                <ajaxToolkit:ConfirmButtonExtender ID="LinkButton_ResendICM_ConfirmButtonExtender" runat="server" BehaviorID="LinkButton_ResendICM_ConfirmButtonExtender" ConfirmText="Are you sure you want to resend the ICM File?" TargetControlID="LinkButton_ResendICM" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="dispatchtime" HeaderText="dispatchtime" SortExpression="dispatchtime" />
                                        <asp:BoundField DataField="PlantNumber" HeaderText="PlantNumber" SortExpression="PlantNumber" />
                                    </Columns>
                                    <EmptyDataRowStyle BackColor="#33CC33" />
                                    <EmptyDataTemplate>
                                        There is no PO closed yet today.
                                    </EmptyDataTemplate>
                                </asp:GridView>
                                <asp:UpdateProgress ID="UpdateProgress2" runat="server">
                                    <ProgressTemplate>
                                        Please wait... this will take few minutes!
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                                <br />
                                <%--                     <asp:Button ID="Button1" runat="server" Text="Process" class="btn btn-primary" />--%>
                                <asp:Button ID="Button_ICM_Refresh" runat="server" Text="Refresh" class="btn btn-success" />

                            </ContentTemplate>
                        </asp:UpdatePanel>

                    </div>
                </div>
            </div>
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse12">SSIS Integration - Producer Payment Systemem</a></h4>
                </div>
                <div id="collapse12" class="panel-collapse collapse">
                    <div class="panel-body">
                        <table class="table">
                            <tr>
                                <td style="width: 25%"><strong>Hosting Server:</strong></td>
                                <td style="width: 25%" class="warning">LF-SQL01\INNOVA</td>
                                <td style="width: 25%" class="danger"><strong>Advance Info</strong></td>
                                <td style="width: 25%" class="success"><strong>How to?</strong></td>
                            </tr>
                            <tr>
                                <td style="height: 37px"><strong>Integration Services Catalogs:</strong></td>
                                <td class="warning" style="height: 37px">Producer Payments</td>
                                <td rowspan="5" class="danger">Please not, only the last 10 days records are shown on below list</td>
                                <td rowspan="5" class="success">1. Click &#39;Delete&#39; to delete the record and wait for next process by server<br />
                                    2. Click &#39;Reprocess&#39; to delete and trigger the process right away.</td>
                            </tr>
                            <tr>
                                <td><strong>Integration Services Project:</strong></td>
                                <td class="warning">Integration_PP_Primal</td>
                            </tr>
                            <tr>
                                <td><strong>Integration Services Packages:</strong></td>
                                <td class="warning" style="height: 37px">PPLamb.dtsx</td>
                            </tr>
                            <tr>
                                <td><strong>SQL Agent Job:</strong></td>
                                <td class="warning">PPLambImport</td>
                            </tr>
                            <%--<asp:Parameter Name="Site" Type="String" />--%>
                            <tr>
                                <td><strong>Flow chart</strong></td>
                                <td class="warning">
                                    <asp:HyperLink ID="HyperLink2" runat="server" Target="_blank" NavigateUrl="\\lf-public\IT\Application Integration Design & Doc\Burradon-ProducerPaymentSystem.vsd">View Flow Chart</asp:HyperLink>
                                </td>
                            </tr>
                        </table>
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:Panel ID="Panel1" runat="server" Height="300px" ScrollBars="Auto">
                                    <asp:GridView Class="table" ID="GridView_PP" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource_BurradonPP" AllowSorting="True" DataKeyNames="Id, OrderId">
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="LinkButton_Reprocess_pp" runat="server" OnClick="LinkButton_Reprocess_pp_Click">Reprocess</asp:LinkButton>
                                                    <ajaxToolkit:ConfirmButtonExtender ID="LinkButton_Reprocess_ConfirmButtonExtender" runat="server" BehaviorID="LinkButton_Reprocess_ConfirmButtonExtender" ConfirmText="Are you sure you want to Reprocess this PP record?" TargetControlID="LinkButton_Reprocess_pp" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField ShowHeader="False">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="LinkButton_Delete_pp" runat="server" CausesValidation="False" CommandName="Delete" Text="Delete"></asp:LinkButton>
                                                    <ajaxToolkit:ConfirmButtonExtender ID="LinkButton_Delete_pp_ConfirmButtonExtender" runat="server" BehaviorID="LinkButton_Delete_pp_ConfirmButtonExtender" ConfirmText="Are you sure you want to Delete this PP record?" TargetControlID="LinkButton_Delete_pp" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="Id" HeaderText="Id" SortExpression="Id" InsertVisible="False" ReadOnly="True" />
                                            <asp:BoundField DataField="Regtime" HeaderText="Regtime" SortExpression="Regtime" />
                                            <asp:BoundField DataField="AphisImport" HeaderText="AphisImport" SortExpression="AphisImport" />
                                            <asp:BoundField DataField="MesImport" HeaderText="MesImport" SortExpression="MesImport" />
                                            <asp:BoundField DataField="ErrorMessage" HeaderText="ErrorMessage" SortExpression="ErrorMessage" />
                                            <asp:BoundField DataField="OrderId" HeaderText="OrderId" SortExpression="OrderId" />
                                            <asp:BoundField DataField="AnimalType" HeaderText="AnimalType" SortExpression="AnimalType" />
                                        </Columns>
                                        <EmptyDataRowStyle BackColor="#33CC33" />
                                        <EmptyDataTemplate>
                                            There is no record.
                                        </EmptyDataTemplate>
                                    </asp:GridView>

                                </asp:Panel>
                                <asp:SqlDataSource ID="SqlDataSource_BurradonPP" runat="server" ConnectionString="<%$ ConnectionStrings:PrimalInnovaIntegrationConnectionString %>" DeleteCommand="DELETE FROM [tbl_PPImportLogs] WHERE [Id] = @Id" InsertCommand="INSERT INTO [tbl_PPImportLogs] ([Regtime], [AphisImport], [MesImport], [ErrorMessage], [OrderId], [AnimalType]) VALUES (@Regtime, @AphisImport, @MesImport, @ErrorMessage, @OrderId, @AnimalType)" SelectCommand="SELECT Id, Regtime, AphisImport, MesImport, ErrorMessage, OrderId, AnimalType FROM tbl_PPImportLogs WHERE (Regtime &gt;= GETDATE()-10) ORDER BY OrderId DESC" UpdateCommand="UPDATE [tbl_PPImportLogs] SET [Regtime] = @Regtime, [AphisImport] = @AphisImport, [MesImport] = @MesImport, [ErrorMessage] = @ErrorMessage, [OrderId] = @OrderId, [AnimalType] = @AnimalType WHERE [Id] = @Id">
                                    <DeleteParameters>
                                        <asp:Parameter Name="Id" Type="Int32" />
                                    </DeleteParameters>
                                    <InsertParameters>
                                        <asp:Parameter Name="Regtime" Type="DateTime" />
                                        <asp:Parameter Name="AphisImport" Type="Int32" />
                                        <asp:Parameter Name="MesImport" Type="Int32" />
                                        <asp:Parameter Name="ErrorMessage" Type="String" />
                                        <asp:Parameter Name="OrderId" Type="String" />
                                        <asp:Parameter Name="AnimalType" Type="String" />
                                    </InsertParameters>
                                    <UpdateParameters>
                                        <asp:Parameter Name="Regtime" Type="DateTime" />
                                        <asp:Parameter Name="AphisImport" Type="Int32" />
                                        <asp:Parameter Name="MesImport" Type="Int32" />
                                        <asp:Parameter Name="ErrorMessage" Type="String" />
                                        <asp:Parameter Name="OrderId" Type="String" />
                                        <asp:Parameter Name="AnimalType" Type="String" />
                                        <asp:Parameter Name="Id" Type="Int32" />
                                    </UpdateParameters>
                                </asp:SqlDataSource>
                                <br />
                                <%--                     <asp:Button ID="Button1" runat="server" Text="Process" class="btn btn-primary" />--%>
                                <asp:Button ID="Button1" runat="server" Text="Refresh" class="btn btn-success" />
                            </ContentTemplate>
                        </asp:UpdatePanel>

                    </div>
                </div>
            </div>
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse3">SSIS Integration - Carcases From Slaney</a></h4>
                </div>
                <div id="collapse3" class="panel-collapse collapse">
                    <div class="panel-body">
                        <table class="table">
                            <tr>
                                <td style="width: 249px"><strong>Hosting Server:</strong></td>
                                <td class="warning">LF9, LF-SQL01\Innova</td>
                                <td class="danger"><strong>Advance Info</strong></td>
                                <td class="success"><strong>How to</strong></td>
                            </tr>
                            <tr>
                                <td style="width: 25%;"><strong>SQL Agent Job:</strong></td>
                                <td style="width: 25%;" class="warning">&nbsp;1.LF9.Slaney and Newcastle carcase import Job<br />
                                    2.LF-SQL01\Innova.Import_SlaneyCarcase</td>
                                <td style="width: 25%;" rowspan="5" class="danger">This process is going to take over 10 minutes, so please wait if you hit 'Reprocess'<br />
                                    <strong>It only process the Record that with &#39;N&#39; on the Integration Sync</strong></td>
                                <td style="width: 25%;" rowspan="5" class="success">1. Click
                        <asp:LinkButton ID="LinkButton_DownloadSlaneyFile" runat="server">Download FTP File</asp:LinkButton>
                                    &nbsp;to download Slaney CSV file<br />
                                    2. Click Reprocess to Trigger the Import again</td>
                            </tr>
                            <tr>
                                <td><strong>Package:</strong></td>
                                <td class="warning">\\lf9\c$\TEST\FU\Package.dtsx</td>
                            </tr>
                            <tr>
                                <td><strong>SP:</strong></td>
                                <td class="warning">LF-SQL01\Innova.InnovaIntegration.<br />
                                    LindenKillInfo_ThridPartyImport</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td class="warning">&nbsp;</td>
                            </tr>
                            <%--<asp:Parameter Name="Site" Type="String" />--%>
                            <tr>
                                <td>&nbsp;</td>
                                <td class="warning">&nbsp;</td>
                            </tr>
                        </table>
                        <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:SqlDataSource ID="SqlDataSource_CarcaseTransfer_Slaney" runat="server" ConnectionString="<%$ ConnectionStrings:AMPSLindenCPACConnectionString %>" SelectCommand="SELECT DISTINCT ECNumber, CONVERT (varchar(10), ImportedDateTime, 120) AS ImportedDate, IntegrationSync FROM ExternalCarcases WHERE (ECNumber = 296) AND (ImportedDateTime IS NOT NULL) AND (CONVERT (varchar(10), ImportedDateTime, 120) &gt;= GETDATE()-10) ORDER BY ImportedDate DESC, IntegrationSync"></asp:SqlDataSource>
                                <asp:GridView Class="table" ID="GridView_CarcaseTransfer" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource_CarcaseTransfer_Slaney" DataKeyNames="ImportedDate">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="LinkButton_CarcaseTransfer_Reprocess_Slaney" runat="server" OnClick="LinkButton_CarcaseTransfer_Reprocess_Slaney_Click">ReProcess</asp:LinkButton>
                                                <ajaxToolkit:ConfirmButtonExtender ID="LinkButton_CarcaseTransfer_Reprocess_Slaney_ConfirmButtonExtender" runat="server" BehaviorID="LinkButton_CarcaseTransfer_Reprocess_ConfirmButtonExtender" ConfirmText="Are you sure you want to transfer Carcases on selected dispatch date? \n this Process might be taken few mintues to be completed!" TargetControlID="LinkButton_CarcaseTransfer_Reprocess_Slaney" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="ECNumber" HeaderText="ECNumber" SortExpression="ECNumber" />
                                        <asp:BoundField DataField="ImportedDate" HeaderText="ImportedDate" SortExpression="ImportedDate" />
                                        <asp:BoundField DataField="IntegrationSync" HeaderText="IntegrationSync" SortExpression="IntegrationSync" />

                                    </Columns>
                                </asp:GridView>

                                <br />
                                <asp:Button ID="Button4" runat="server" Text="Refresh" class="btn btn-success" />
                                <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                                    <ProgressTemplate>
                                        Please wait!....
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                            </ContentTemplate>
                        </asp:UpdatePanel>

                    </div>
                </div>
            </div>
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse4">SSIS Integration - Carcases From EuroFarm</a></h4>
                </div>
                <div id="collapse4" class="panel-collapse collapse">
                    <div class="panel-body">
                        <table class="table">
                            <tr>
                                <td style="width: 249px"><strong>Hosting Server:</strong></td>
                                <td class="warning">LF-SQL01\Innova</td>
                                <td class="danger"><strong>Advance Info</strong></td>
                                <td class="success"><strong>How to</strong></td>
                            </tr>
                            <tr>
                                <td style="width: 25%;"><strong>ntegration Services Catalogs:</strong></td>
                                <td style="width: 25%;" class="warning">EuroFarmImport</td>
                                <td style="width: 25%;" rowspan="5" class="danger">1.This process is going to take over 10 minutes, so please wait if you hit 'Reprocess'<br />
                                    <br />
                                    2.if qtr in (3,4), then system will fail to generate a animal record into CPAC.EXTKilltran
                                </td>
                                <td style="width: 25%;" rowspan="5" class="success">1. remove unwanted lines<br />
                                    2. saved CMR sheet as \\lfkill\amps\EuroFarm\Carcases.csv<br />
                                    3. saved Trace sheet as \\lfkill\amps\EuroFarm\Animal.csv<br />
                                    4. Click
                                    <asp:LinkButton ID="LinkButton_TriggerEuroFarmImport" runat="server">HERE</asp:LinkButton>
                                    &nbsp;to trigger the import process or run the Job &#39;Import_EuroFarmCarcase&#39;</td>
                            </tr>
                            <tr>
                                <td><strong>Integration Services Project:</strong></td>
                                <td class="warning">Integration_EuroFarm_Import</td>
                            </tr>
                            <tr>
                                <td><strong>Package:</strong></td>
                                <td class="warning">Package.dtsx</td>
                            </tr>
                            <tr>
                                <td><strong>SQL Agent Job:</strong></td>
                                <td class="warning">Import_EuroFarmCarcase</td>
                            </tr>
                            <%--                     <asp:Button ID="Button1" runat="server" Text="Process" class="btn btn-primary" />--%>
                            <tr>
                                <td>&nbsp;</td>
                                <td class="warning">&nbsp;</td>
                            </tr>
                        </table>
                        <asp:UpdatePanel ID="UpdatePanel5" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <%--             <asp:SqlDataSource ID="SqlDataSource_CarcaseImport_EuroFarm" runat="server" ConnectionString="<%$ ConnectionStrings:AMPSLindenCPACConnectionString %>" SelectCommand="SELECT DISTINCT ECNumber, CONVERT (varchar(10), ImportedDateTime, 120) AS ImportedDate, IntegrationSync FROM ExternalCarcases WHERE (ECNumber = 297) AND (ImportedDateTime IS NOT NULL) AND (CONVERT (varchar(10), ImportedDateTime, 120) &gt;= GETDATE()-10) ORDER BY ImportedDate DESC, IntegrationSync">
             </asp:SqlDataSource>--%>
                                <%--                       <asp:GridView Class="table" ID="GridView2" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource_CarcaseImport_EuroFarm" DataKeyNames="ImportedDate">
                           <Columns>
                               <asp:TemplateField>
                                   <ItemTemplate>
                                       <asp:LinkButton ID="LinkButton_CarcaseTransfer_Reprocess_EuroFarm" runat="server" OnClick="LinkButton_CarcaseTransfer_Reprocess_EuroFarm_Click">ReProcess</asp:LinkButton>
                                       <ajaxToolkit:ConfirmButtonExtender ID="LinkButton_CarcaseTransfer_Reprocess_EF_ConfirmButtonExtender" runat="server" BehaviorID="LinkButton_CarcaseTransfer_Reprocess_EF_ConfirmButtonExtender" ConfirmText="Are you sure you want to transfer Carcases on selected dispatch date? \n this Process might be taken few mintues to be completed!" TargetControlID="LinkButton_CarcaseTransfer_Reprocess_EuroFarm" />
                                   </ItemTemplate>
                               </asp:TemplateField>
                               <asp:BoundField DataField="ECNumber" HeaderText="ECNumber" SortExpression="ECNumber" />
                               <asp:BoundField DataField="ImportedDate" HeaderText="ImportedDate" SortExpression="ImportedDate" />
                               <asp:BoundField DataField="IntegrationSync" HeaderText="IntegrationSync" SortExpression="IntegrationSync" />
                               
                           </Columns>
                       </asp:GridView>--%>

                                <br />
                                <asp:Button ID="Button2" runat="server" Text="Refresh" class="btn btn-success" />
                                <asp:UpdateProgress ID="UpdateProgress3" runat="server">
                                    <ProgressTemplate>
                                        Please wait!....
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                            </ContentTemplate>
                        </asp:UpdatePanel>

                    </div>
                </div>
            </div>
            <div class="panel  panel-info">
                <div class="panel-heading">
                    <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse10">SSIS Integration - Purchase Order From Innova to Nav</a></h4>
                </div>

                <div id="collapse10" class="panel-collapse collapse">
                    <div class="panel-body">
                        <table class="table">
                            <tr>
                                <td style="width: 25%; height: 37px;"><strong>Hosting Server:</strong></td>
                                <td style="width: 25%; height: 37px;" class="warning">LF-SQL01\INNOVA</td>
                                <td style="width: 25%; height: 37px;" class="danger"><strong>Advance Info</strong></td>
                                <td style="width: 25%; height: 37px;" class="success"><strong>How to</strong></td>
                            </tr>
                            <tr>
                                <td><strong>Database:</strong></td>
                                <td class="warning">LF-SQL01\UTILITY1.LindenBridge</td>
                                <td class="danger" rowspan="6">this Integration is trigger by the script <strong>59 </strong>in Innova,</td>
                                <td class="success" rowspan="6">reset the recoard and click process to force system update the record in Nav again.</td>
                            </tr>
                            <tr>
                                <td><strong>Table:</strong></td>
                                <td class="warning">tblIntegration_PO_Innova_Primal2Nav_Primal</td>
                            </tr>
                            <tr>
                                <td><strong>Stored Procedures:</strong></td>
                                <td class="warning">sp_Integration_PO_Innova_Primal2LB</td>
                            </tr>
                            <tr>
                                <td><strong>SSIS Catalog:</strong></td>
                                <td class="warning">Primal Innova and Navision</td>
                            </tr>

                            <tr>
                                <td><strong>SSIS Project:</strong></td>
                                <td class="warning">Integration_Innvova_PrimalAndNavision</td>
                            </tr>

                            <tr>
                                <td><strong>Package:</strong></td>
                                <td class="warning">Primal PurchaseOrders Innova2Nav.dtsx</td>
                            </tr>

                        </table>
                        <asp:GridView ID="GridView_POInnova2Nav" CssClass="table" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" DataSourceID="SqlDataSource_PO_Innova2Nav" DataKeyNames="id">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="LinkButtonPOReset_Innova2Nav" runat="server" OnClick="LinkButtonPOReset_Innova2Nav_Click">Reset</asp:LinkButton>
                                        <ajaxToolkit:ConfirmButtonExtender ID="LinkButtonPOReset_Innova2Nav_ConfirmButtonExtender" runat="server" BehaviorID="LinkButtonPOReset_Innova2Nav_ConfirmButtonExtender" ConfirmText="Are you sure you want to reset this record?" TargetControlID="LinkButtonPOReset_Innova2Nav" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="InnovaOrder" HeaderText="InnovaOrder" SortExpression="InnovaOrder" />
                                <asp:BoundField DataField="InnovaOrderLineWeight" HeaderText="Weight" SortExpression="InnovaOrderLineWeight" />
                                <asp:BoundField DataField="InnovaOrderLineQuantity" HeaderText="Qty" SortExpression="InnovaOrderLineQuantity" />
                                <asp:BoundField DataField="InnovaOrderLineUOM" HeaderText="InnovaUOM" SortExpression="InnovaOrderLineUOM" />
                                <asp:BoundField DataField="SyncToNav" HeaderText="SyncToNav" SortExpression="SyncToNav" />
                                <asp:BoundField DataField="ErrorMessage" HeaderText="ErrorMessage" SortExpression="ErrorMessage" />
                                <asp:BoundField DataField="NavPurchasePriceUOM" HeaderText="NavPriceUOM" SortExpression="NavPurchasePriceUOM" />
                                <asp:BoundField DataField="CreatedDateTimeStamp" HeaderText="CreatedDateTimeStamp" SortExpression="CreatedDateTimeStamp" />
                            </Columns>
                        </asp:GridView>
                        <asp:SqlDataSource ID="SqlDataSource_PO_Innova2Nav" runat="server" ConnectionString="<%$ ConnectionStrings:LindenBridgeConnectionString %>" SelectCommand="SELECT id,InnovaOrder, InnovaOrderLineWeight, InnovaOrderLineQuantity, InnovaOrderLineUOM, SyncToNav, ErrorMessage, NavPurchasePriceUOM, CreatedDateTimeStamp FROM tblIntegration_PO_Innova_Primal2Nav_Primal where CreatedDateTimeStamp>=getdate()-10 order by CreatedDateTimeStamp desc"></asp:SqlDataSource>
                        <asp:Button ID="Button_PO_Innova2Nav" runat="server" class="btn btn-primary" Text="Process" />
                    </div>
                </div>
            </div>
            <div class="panel  panel-info">
                <div class="panel-heading">
                    <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse13">SSIS Integration - Sale Order From Innova to Nav</a></h4>
                </div>

                <div id="collapse13" class="panel-collapse collapse">
                    <div class="panel-body">
                        <table class="table">
                            <tr>
                                <td style="width: 25%; height: 37px;"><strong>Hosting Server:</strong></td>
                                <td style="width: 25%; height: 37px;" class="warning">LF-SQL01\INNOVA</td>
                                <td style="width: 25%; height: 37px;" class="danger"><strong>Advance Info</strong></td>
                                <td style="width: 25%; height: 37px;" class="success"><strong>How to</strong></td>
                            </tr>
                            <tr>
                                <td><strong>Database:</strong></td>
                                <td class="warning">LF-SQL01\UTILITY1.LindenBridge</td>
                                <td class="danger" rowspan="4">this Integration is trigger by the script <strong>24 </strong>in Innova,</td>
                                <td class="success" rowspan="4">reset the recoard and click process to force system update the record in Nav again.</td>
                            </tr>
                            <tr>
                                <td><strong>Table:</strong></td>
                                <td class="warning">tblIntegration_SO_Innova_Primal2Nav_Primal</td>
                            </tr>
                            <tr>
                                <td><strong>Stored Procedures:</strong></td>
                                <td class="warning">sp_Integration_SO_Innova_Primal2LB</td>
                            </tr>


                        </table>
                        <asp:GridView ID="GridView_SO_Innova2Nav" CssClass="table" runat="server" AllowPaging="True" AllowSorting="True" DataKeyNames="Id,InnovaOrder" AutoGenerateColumns="False" DataSourceID="SqlDataSource_SO_Innva2Nav">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="LinkButton_SO_Innova2Nav" runat="server" OnClick="LinkButton_SO_Innova2Nav_Click">Reset</asp:LinkButton>
                                        <ajaxToolkit:ConfirmButtonExtender ID="LinkButton_SO_Innova2Nav_ConfirmButtonExtender" runat="server" BehaviorID="LinkButton_SO_Innova2Nav_ConfirmButtonExtender" ConfirmText="Are you sure you want to seset this record?" TargetControlID="LinkButton_SO_Innova2Nav" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="InnovaOrder" HeaderText="InnovaID" SortExpression="InnovaOrder" />
                                <asp:BoundField DataField="InnovaSaleOrderNo" HeaderText="SaleOrder" SortExpression="InnovaSaleOrderNo" />
                                <asp:BoundField DataField="InnovaOrderLineProductCode" HeaderText="ProductCode" SortExpression="InnovaOrderLineProductCode" />
                                <asp:BoundField DataField="InnovaOrderLineWeight" HeaderText="Weight" SortExpression="InnovaOrderLineWeight" />
                                <asp:BoundField DataField="SyncToNav" HeaderText="SyncToNav" SortExpression="SyncToNav" />
                                <asp:BoundField DataField="ErrorMessage" HeaderText="ErrorMessage" SortExpression="ErrorMessage" />
                                <asp:BoundField DataField="CreatedDateTimeStamp" HeaderText="CreatedDateTimeStamp" SortExpression="CreatedDateTimeStamp" />
                                <asp:BoundField DataField="NavSalesPriceUOM" HeaderText="SalesPriceUOM" SortExpression="NavSalesPriceUOM" />
                            </Columns>
                        </asp:GridView>
                        <asp:SqlDataSource ID="SqlDataSource_SO_Innva2Nav" runat="server" ConnectionString="<%$ ConnectionStrings:LindenBridgeConnectionString %>" SelectCommand="SELECT [Id], [InnovaOrder], InnovaSaleOrderNo,[InnovaOrderLineProductCode], [InnovaOrderLineWeight], [SyncToNav], [ErrorMessage], [CreatedDateTimeStamp], [NavSalesPriceUOM] FROM [tblIntegration_SO_Innova_Primal2Nav_Primal] WHERE ([CreatedDateTimeStamp] >=getdate()-15) ORDER BY [InnovaOrder] DESC"></asp:SqlDataSource>
                        <asp:Button ID="Button_SO_Innova2Nav" runat="server" class="btn btn-primary" Text="Process" />
                    </div>
                </div>
            </div>
            <div class="panel  panel-info">
                <div class="panel-heading">
                    <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse15">SP Integration - Dorherty And Gray Export</a></h4>
                </div>

                <div id="collapse15" class="panel-collapse collapse in">
                    <div class="panel-body">
                        <table class="table">
                            <tr>
                                <td style="width: 25%; height: 37px;"><strong>Hosting Server:</strong></td>
                                <td style="width: 25%; height: 37px;" class="warning">LF-SQL01\INNOVA;</td>
                                <td style="width: 25%; height: 37px;" class="danger"><strong>Advance Info</strong></td>
                                <td style="width: 25%; height: 37px;" class="success"><strong>How to</strong></td>
                            </tr>
                            <tr>
                                <td><strong>Database:</strong></td>
                                <td class="warning">Innova</td>
                                <td class="danger" rowspan="4">&nbsp;</td>
                                <td class="success" rowspan="4">Click Resend to send the csv file again.</td>
                            </tr>
                            <tr>
                                <td><strong>Stored Procedure:</strong></td>
                                <td class="warning">sp_ThirdParty_Email_DohertyAndGrayExportRountinLambCSV<br />
                                    sp_ThirdParty_Email_DohertyAndGrayExportRountinBeefCSV</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td class="warning"></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td class="warning">&nbsp;</td>
                            </tr>


                        </table>
                        <asp:UpdatePanel ID="UpdatePane_DandG" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:GridView ID="GridView_DG_Lamb" CssClass="table" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource_DG_lamb" DataKeyNames="TransDate" AllowSorting="True">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Lamb">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="LinkButton_DG_Lamb_Resend" runat="server" OnClick="LinkButton_DG_Lamb_Resend_Click">Resend</asp:LinkButton>
                                                <ajaxToolkit:ConfirmButtonExtender ID="LinkButton_DG_Lamb_Resend_ConfirmButtonExtender" runat="server" BehaviorID="LinkButton_DG_Lamb_Resend_ConfirmButtonExtender" ConfirmText="Are you sure you want to re-send this file?" TargetControlID="LinkButton_DG_Lamb_Resend" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="TransDate" HeaderText="TransDate" ReadOnly="True" SortExpression="TransDate" />
                                        <asp:BoundField DataField="CountNo" HeaderText="AnimalNo" ReadOnly="True" SortExpression="CountNo" />
                                    </Columns>
                                </asp:GridView>
                                <asp:SqlDataSource ID="SqlDataSource_DG_lamb" runat="server" ConnectionString="<%$ ConnectionStrings:PrimalInnovaConnectionString %>" SelectCommand="select distinct convert(varchar(10),transtime,120) as TransDate, count(*) as CountNo  FROM Innova.dbo.vw_PPtransfer_lamb_EmailView WHERE DestinationCode='DG' and TransTime &gt;getdate()-10 group by  convert(varchar(10),transtime,120)"></asp:SqlDataSource>
                                <asp:GridView ID="GridView_DG_Beef" runat="server" CssClass="table" AllowSorting="True" DataKeyNames="TransDate" AutoGenerateColumns="False" DataSourceID="SqlDataSource_DG_Beef">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Beef">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="LinkButton_DG_Beef_Resend" runat="server" OnClick="LinkButton_DG_Beef_Resend_Click">Resend</asp:LinkButton>
                                                <ajaxToolkit:ConfirmButtonExtender ID="LinkButton_DG_Beef_Resend_ConfirmButtonExtender" runat="server" BehaviorID="LinkButton_DG_Beef_Resend_ConfirmButtonExtender" ConfirmText="Are you sure you want to resend this file?" TargetControlID="LinkButton_DG_Beef_Resend" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="TransDate" HeaderText="TransDate" ReadOnly="True" SortExpression="TransDate" />
                                        <asp:BoundField DataField="AnimalNo" HeaderText="AnimalNo" ReadOnly="True" SortExpression="AnimalNo" />
                                    </Columns>
                                </asp:GridView>
                                <asp:SqlDataSource ID="SqlDataSource_DG_Beef" runat="server" ConnectionString="<%$ ConnectionStrings:AMPSLindenBeefConnectionString %>" SelectCommand="SELECT        CONVERT(varchar(10), K.TransDate, 120) AS TransDate, COUNT(*) AS AnimalNo
FROM            KILLTRANS AS K INNER JOIN
                         SuppLier AS S ON K.SupplierNo = S.Supplier
WHERE        (K.DestinationCode = 'DG') AND (K.TransDate &gt;= GETDATE() - 10)
GROUP BY CONVERT(varchar(10), K.TransDate, 120)
ORDER BY TransDate DESC"></asp:SqlDataSource>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
            <div class="panel  panel-info">
                <div class="panel-heading">
                    <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse14">SP Integration - Update Linden Carcase Value</a></h4>
                </div>

                <div id="collapse14" class="panel-collapse collapse">
                    <div class="panel-body">
                        <table class="table">
                            <tr>
                                <td style="width: 25%; height: 37px;"><strong>Hosting Server:</strong></td>
                                <td style="width: 25%; height: 37px;" class="warning">LF-SQL01\INNOVA; LFKIll</td>
                                <td style="width: 25%; height: 37px;" class="danger"><strong>Advance Info</strong></td>
                                <td style="width: 25%; height: 37px;" class="success"><strong>How to</strong></td>
                            </tr>
                            <tr>
                                <td><strong>Database:</strong></td>
                                <td class="warning">LF-SQL01\Innova; LFKILL.AMPSLindenBeef</td>
                                <td class="danger" rowspan="4">&nbsp;</td>
                                <td class="success" rowspan="4">&nbsp;</td>
                            </tr>
                            <tr>
                                <td><strong>Stored Procedure:</strong></td>
                                <td class="warning">LF-SQL01\Innova.dbo.sp_AutoUpdateHQvalue</td>
                            </tr>
                            <tr>
                                <td><strong>Trigger:</strong></td>
                                <td class="warning">LFKILL.AMPSLindenBeef.dbo.Killtrans.SyncStockValueToInnova</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td class="warning">&nbsp;</td>
                            </tr>


                        </table>
                        <table class="table">

                            <tr>
                                <td colspan="4">Update Carcase Value</td>
                            </tr>

                            <tr>
                                <td>Date From</td>
                                <td>
                                    <asp:TextBox ID="TextBox_DateFrom" runat="server"></asp:TextBox>
                                    <ajaxToolkit:CalendarExtender ID="TextBox_DateFrom_CalendarExtender" runat="server" BehaviorID="TextBox_DateFrom_CalendarExtender" TargetControlID="TextBox_DateFrom" Format="yyyy-mm-dd" />
                                </td>
                                <td>Date To</td>
                                <td>
                                    <asp:TextBox ID="TextBox_DeateTo" runat="server"></asp:TextBox>
                                    <ajaxToolkit:CalendarExtender ID="TextBox_DeateTo_CalendarExtender" runat="server" BehaviorID="TextBox_DeateTo_CalendarExtender" TargetControlID="TextBox_DeateTo" Format="yyyy-mm-dd" />
                                </td>
                                <td>
                                    <asp:Button ID="Button_Update_CarcaseValue" class="btn btn-primary" runat="server" Text="Update" /></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="panel  panel-info">
                <div class="panel-heading">
                    <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse8">SP Integration - Purchase Order From Nav to Innova</a></h4>
                </div>

                <div id="collapse8" class="panel-collapse collapse">
                    <div class="panel-body">
                        <table class="table">
                            <tr>
                                <td style="width: 25%; height: 37px;"><strong>Hosting Server:</strong></td>
                                <td style="width: 25%; height: 37px;" class="warning">LF-SQL01\INNOVA</td>
                                <td style="width: 25%; height: 37px;" class="danger"><strong>Advance Info</strong></td>
                                <td style="width: 25%; height: 37px;" class="success"><strong>How to</strong></td>
                            </tr>
                            <tr>
                                <td><strong>Database:</strong></td>
                                <td class="warning">LF-SQL01\UTILITY1.LindenBridge</td>
                                <td class="danger" rowspan="4">&nbsp;</td>
                                <td class="success" rowspan="4">Open and release the Purchase Order to trigger the integration again. </td>
                            </tr>
                            <tr>
                                <td><strong>Table:</strong></td>
                                <td class="warning">LindenBridge.dbo.<br />
                                    tblIntegration_PO_Nav_Primal2Innova_Primal_Orders<br />
                                    tblIntegration_PO_Nav_Primal2Innova_Primal_Lines</td>
                            </tr>
                            <tr>
                                <td><strong>Stored Procedures:</strong></td>
                                <td class="warning">Innova.dbo.sp_Integration_PO_Innova_Primal2LB</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td class="warning">&nbsp;</td>
                            </tr>


                        </table>
                        <asp:GridView ID="GridView_SP_SO_Header" CssClass="table" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" DataSourceID="SqlDataSource_SP_POHeader">
                            <Columns>
                                <asp:BoundField DataField="PONo" HeaderText="PONo" SortExpression="PONo" />
                                <asp:BoundField DataField="LastDateTimeModified" HeaderText="LastDateTimeModified" SortExpression="LastDateTimeModified" />
                                <asp:BoundField DataField="ImportType" HeaderText="ImportType" SortExpression="ImportType" />
                                <asp:BoundField DataField="Status" HeaderText="Status" SortExpression="Status" />
                                <asp:BoundField DataField="ErrorMessage" HeaderText="ErrorMessage" SortExpression="ErrorMessage" />
                                <asp:BoundField DataField="DateTimeProcess" HeaderText="DateTimeProcess" SortExpression="DateTimeProcess" />
                                <asp:BoundField DataField="DateTimeImport" HeaderText="DateTimeImport" SortExpression="DateTimeImport" />
                            </Columns>
                        </asp:GridView>
                        <asp:SqlDataSource ID="SqlDataSource_SP_POHeader" runat="server" ConnectionString="<%$ ConnectionStrings:LindenBridgeConnectionString %>" SelectCommand="SELECT Id, PONo, BuyFromCustomerNo, OrderDesc, PRUnit, LocationCode, DeliveryDate, DeliveryDatePlus1, SupplyCode, LastDateTimeModified, ImportType, Status, ErrorMessage, DateTimeProcess, DateTimeImport, Inventory, prpr, Reference FROM tblIntegration_PO_Nav_Primal2Innova_Primal_Orders WHERE (DateTimeImport &gt;= GETDATE()-10) ORDER BY DateTimeImport DESC, PONo DESC"></asp:SqlDataSource>
                    </div>
                </div>
            </div>
            <div class="panel  panel-info">
                <div class="panel-heading">
                    <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse9">SP Integration - Sales Order From Nav to Innova</a></h4>
                </div>

                <div id="collapse9" class="panel-collapse collapse">
                    <div class="panel-body">
                        <table class="table">
                            <tr>
                                <td style="width: 25%; height: 37px;"><strong>Hosting Server:</strong></td>
                                <td style="width: 25%; height: 37px;" class="warning">LF-SQL01\INNOVA</td>
                                <td style="width: 25%; height: 37px;" class="danger"><strong>Advance Info</strong></td>
                                <td style="width: 25%; height: 37px;" class="success"><strong>How to</strong></td>
                            </tr>
                            <tr>
                                <td><strong>Database:</strong></td>
                                <td class="warning">LF-SQL01\UTILITY1.LindenBridge</td>
                                <td class="danger" rowspan="4">&nbsp;</td>
                                <td class="success" rowspan="4">Open and release the Sale Order to trigger the integration again. </td>
                            </tr>
                            <tr>
                                <td><strong>Table:</strong></td>
                                <td class="warning">LindenBridge.dbo.<br />
                                    tblIntegration_SO_Nav_Primal2Innova_Primal_Orders<br />
                                    tblIntegration_SO_Nav_Primal2Innova_Primal_Lines</td>
                            </tr>
                            <tr>
                                <td><strong>Stored Procedures:</strong></td>
                                <td class="warning">Innova.dbo.sp_Integration_SO_Innova_Primal2LB</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td class="warning">&nbsp;</td>
                            </tr>


                        </table>
                        <asp:GridView ID="GridView_SO2Innova" CssClass="table" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="Id" DataSourceID="SqlDataSource_SONav2Innova">
                            <Columns>
                                <asp:BoundField DataField="SalesOrderNo" HeaderText="SalesOrderNo" SortExpression="SalesOrderNo" />
                                <asp:TemplateField HeaderText="ImportType" SortExpression="ImportType">
                                    <ItemTemplate>
                                        <asp:Label ID="Label1" runat="server" Text='<%# Eval("ImportType").ToString.Replace(1, "Insert").Replace(2, "Update") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="ItemLastDateModified" DataFormatString="{0:dd/MM/yyyy}" HeaderText="DateModified" SortExpression="ItemLastDateModified" />
                                <asp:BoundField DataField="ItemLastTimeModified" DataFormatString="{0:HH:mm:ss}" HeaderText="TimeModified" SortExpression="ItemLastTimeModified" />
                                <asp:BoundField DataField="DateTimeProcess" HeaderText="DateTimeProcess" SortExpression="DateTimeProcess" />
                                <asp:BoundField DataField="DateTimeImport" HeaderText="DateTimeImport" SortExpression="DateTimeImport" />
                                <asp:BoundField DataField="ErrorMessage" HeaderText="ErrorMessage" SortExpression="ErrorMessage" />
                            </Columns>
                        </asp:GridView>
                        <asp:SqlDataSource ID="SqlDataSource_SONav2Innova" runat="server" ConnectionString="<%$ ConnectionStrings:LindenBridgeConnectionString %>" SelectCommand="SELECT * FROM [tblIntegration_SO_Nav_Primal2Innova_Primal_Orders] where DateTimeImport&gt;=getdate()-10 ORDER BY [DateTimeImport] DESC, [SalesOrderNo] DESC"></asp:SqlDataSource>
                    </div>
                </div>
            </div>
            <div class="panel  panel-info">
                <div class="panel-heading">
                    <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse6">SP Integration - Auto Fix</a></h4>
                </div>

                <div id="collapse6" class="panel-collapse collapse in ">
                    <div class="panel-body">
                        <table class="table">
                            <tr>
                                <td style="width: 25%"><strong>Hosting Server:</strong></td>
                                <td style="width: 25%" class="warning">LF-SQL01\INNOVA</td>
                                <td style="width: 25%" class="danger"><strong>Advance Info</strong></td>
                                <td style="width: 25%" class="success"><strong>How to</strong></td>
                            </tr>
                            <tr>
                                <td><strong>Database:</strong></td>
                                <td class="warning">LF-SQL01\UTILITY1.LindenBridge</td>
                                <td class="danger" rowspan="4">auto fix is trigger by SQL Agent every 10 mintutes from 5AM to 11PM</td>
                                <td class="success" rowspan="4">1. Tick the check box to enable the auto fix<br />
                                    2. click &#39;Reset&#39; to reset a import record 
                        <br />
                                </td>
                            </tr>
                            <tr>
                                <td><strong>Table:</strong></td>
                                <td class="warning">LindenBridge.dbo.tbl_AutoFix_Setting</td>
                            </tr>
                            <tr>
                                <td><strong>Stored Procedures:</strong></td>
                                <td class="warning">Innova.dbo.sp_Integration_Autofix</td>
                            </tr>
                            <tr>
                                <td><strong>SQL Server Agent:</strong></td>
                                <td class="warning">Innova_ITGR_AutoFix</td>
                            </tr>


                        </table>
                        <table>
                            <tr>
                                <td style="vertical-align: top">
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:GridView ID="GridView_AutoFix" class="table" runat="server" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="Site,HandlerCode" DataSourceID="SqlDataSource_AutoFix">
                                                <Columns>
                                                    <asp:CommandField ShowEditButton="True" />
                                                    <asp:BoundField DataField="HandlerCode" HeaderText="HandlerCode" ReadOnly="True" SortExpression="HandlerCode" />
                                                    <asp:BoundField DataField="Description" HeaderText="Description" ReadOnly="True" SortExpression="Description" />
                                                    <asp:CheckBoxField DataField="Status" HeaderText="Status" SortExpression="Status" />
                                                </Columns>
                                            </asp:GridView>
                                            <asp:SqlDataSource ID="SqlDataSource_AutoFix" runat="server" ConnectionString="<%$ ConnectionStrings:LindenBridgeConnectionString %>" DeleteCommand="DELETE FROM [tbl_AutoFix_Setting] WHERE [Site] = @Site AND [HandlerCode] = @HandlerCode" InsertCommand="INSERT INTO [tbl_AutoFix_Setting] ([Site], [HandlerCode], [Status]) VALUES ('Kettyle', @HandlerCode, @Status)" SelectCommand="SELECT Id, Site, HandlerCode, Status, Description FROM tbl_AutoFix_Setting WHERE (Site = @Site) ORDER BY HandlerCode" UpdateCommand="UPDATE [tbl_AutoFix_Setting] SET [Status] = @Status WHERE [Site] = @Site AND [HandlerCode] = @HandlerCode">
                                                <DeleteParameters>
                                                    <asp:Parameter Name="Site" Type="String" />
                                                    <asp:Parameter Name="HandlerCode" Type="String" />
                                                </DeleteParameters>
                                                <InsertParameters>
                                                    <%--<asp:Parameter Name="Site" Type="String" />--%>
                                                    <asp:Parameter Name="HandlerCode" Type="String" />
                                                    <asp:Parameter Name="Status" Type="Boolean" />
                                                </InsertParameters>
                                                <SelectParameters>
                                                    <asp:Parameter DefaultValue="Primal" Name="Site" Type="String" />
                                                </SelectParameters>
                                                <UpdateParameters>
                                                    <asp:Parameter Name="Id" Type="Int32" />
                                                    <asp:Parameter Name="Status" Type="Boolean" />
                                                    <asp:Parameter Name="Site" Type="String" />
                                                    <asp:Parameter Name="HandlerCode" Type="String" />
                                                </UpdateParameters>
                                            </asp:SqlDataSource>
                                            <asp:Button ID="Button_AutoFix" runat="server" class="btn btn-primary" Text="Process" />
                                            <ajaxToolkit:ConfirmButtonExtender ID="Button_AutoFix_ConfirmButtonExtender" runat="server" BehaviorID="Button_AutoFix_ConfirmButtonExtender" ConfirmText="Are you sure you want to run the auto fix now?" TargetControlID="Button_AutoFix" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                                <td style="vertical-align: top">
                                    <asp:UpdatePanel ID="UpdatePanel_Itgr_Import" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:Panel ID="Panel_AutoFix" runat="server" Height="400px" ScrollBars="Auto">
                                                <asp:GridView ID="GridView_ITGr_Import" class="table" runat="server" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="id" DataSourceID="SqlDataSource_ItgrImport_log" Width="635px">
                                                    <Columns>
                                                        <asp:CommandField ShowSelectButton="True" />
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="LinkButton_ItgrImport_Reset" runat="server" OnClick="LinkButton_ItgrImport_Reset_Click">Reset</asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="id" HeaderText="id" InsertVisible="False" ReadOnly="True" SortExpression="id" />
                                                        <asp:BoundField DataField="regtime" HeaderText="regtime" SortExpression="regtime" />
                                                        <asp:BoundField DataField="importstatus" HeaderText="importstatus" SortExpression="importstatus" />
                                                        <asp:BoundField DataField="importhandlercode" HeaderText="handlercode" SortExpression="importhandlercode" />
                                                    </Columns>
                                                    <EmptyDataRowStyle BackColor="#009900" />
                                                    <EmptyDataTemplate>
                                                        There is no Errors in ITGR_Import
                                                    </EmptyDataTemplate>
                                                </asp:GridView>

                                            </asp:Panel>
                                            <asp:Label ID="Label_ITGR_Data" runat="server" Text="" /><br />
                                            <asp:Label ID="Label_ITGR_Errors" runat="server" Text="" /><br />
                                            <asp:SqlDataSource ID="SqlDataSource_ItgrImport_log" runat="server" ConnectionString="<%$ ConnectionStrings:PrimalInnovaConnectionString %>" SelectCommand="SELECT id, regtime, importstatus, importhandlercode, data FROM itgr_imports WHERE importstatus=3 and (CONVERT (varchar(10), regtime, 120) = CONVERT (varchar(10), GETDATE(), 120))"></asp:SqlDataSource>

                                            <asp:Button ID="Button_Autofix_Refresh" runat="server" class="btn btn-success" Text="Refresh" />

                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="panel  panel-info">
                <div class="panel-heading">
                    <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse7">Triggers - Stock Value</a></h4>
                </div>
                <div id="collapse7" class="panel-collapse collapse in ">
                    <div class="panel-body">
                        <table class="table">
                            <tr>
                                <td style="width: 25%; height: 22px;"><strong>Hosting Server:</strong></td>
                                <td style="width: 25%; height: 22px;" class="warning">LBSQL\Utility1</td>
                                <td style="width: 25%; height: 22px;" class="danger"><strong>Advance Info</strong></td>
                                <td style="width: 25%; height: 22px;" class="success"><strong>How to</strong></td>
                                <tr>
                                    <td><strong>Database:</strong></td>
                                    <td class="warning">AMPSWBBeef</td>
                                    <td class="danger" rowspan="4">Update Stockvalue to LB Innova<br />
                                        [lbsql\INNOVA].[Innova].[dbo].[proc_individuals]</td>
                                    <td class="success">&nbsp;</td>
                                </tr>
                            <tr>
                                <td style="width: 249px; height: 22px;"><strong>Table:</strong></td>
                                <td style="height: 22px; width: 6px;" class="warning">KILLTRANS</td>
                                <td style="height: 22px; width: 6px;" class="success">&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="width: 249px"><strong>Trigger:</strong></td>
                                <td style="width: 6px" class="warning">Trigger_SyncStockValue</td>
                                <td style="width: 6px" class="success">&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="width: 249px; height: 23px;">&nbsp;</td>
                                <td style="height: 23px; width: 6px;" class="warning">&nbsp;</td>
                                <td style="height: 23px; width: 6px;" class="success">&nbsp;</td>
                            </tr>
                            <%--                     <asp:Button ID="Button1" runat="server" Text="Process" class="btn btn-primary" />--%>
                        </table>
                        <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <br />
                                <%--                     <asp:Button ID="Button1" runat="server" Text="Process" class="btn btn-primary" />--%>
                    &nbsp; 
                            </ContentTemplate>
                        </asp:UpdatePanel>

                    </div>
                </div>
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                            &times; 
                            <div class="modal-header">
                            </div>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Alert Message:</h4>
                    </div>
                    <div class="modal-body">
                        <asp:UpdatePanel ID="UpdatePanel_AlertMessage" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:Label ID="Label_AlertMessage" runat="server" Text=""></asp:Label>
                            </ContentTemplate>
                        </asp:UpdatePanel>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
    </div>
</asp:Content>

