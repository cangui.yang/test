﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" CodeFile="Integration_Primal_SI.aspx.vb" Inherits="Integration_Primal_SI" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">


<div class="container">
  <h2>Primal SI Integration</h2>
  <p><strong>Note:Source Location </strong>\\lf-vm-test01\Users\cangui.yang\Documents\Visual Studio 2015\Projects\LindenFoods_Integration\Integration_Innova_KettyleAndNavision</p>
          <div class="panel-group" id="accordion">
              <div class="panel panel-info">
                  <div class="panel-heading">
                      <h4 class="panel-title">  <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
                         SI Despatch Processor - Remove Packs in Innova & Scan</a>Vaegt</h4>
      </div>
      <div id="collapse1" class="panel-collapse collapse in">
        <div class="panel-body">
            
            <table class="table">
                <tr >
                    <td style="width:25%"><strong>Hosting Server:</strong></td>
                    <td style="width:25%" class="warning">LF7 &amp; [LF-SQL\innova]</td>
                    <td style="width:25%" class="danger"><strong>Advance Info</strong></td>
                    <td style="width:25%" class="success"><strong>How to</strong></td>
                </tr>
                <tr >
                    <td style="width: 249px"><strong>SQL Database :</strong></td>
                    <td class="warning" style="width: 25%">SI &amp; Innova</td>
                    <td class="danger" rowspan="5">Packs that cancel in SI will generate a file to scanvet and xml to innova to calcel packs</td>
                    <td class="success" rowspan="5"><strong>How to generate a xml file to cancel a pack in Innova?</strong><br />
                        run [LF-SQL01\INNOVA].INNOVA.DBO.<br />
                        sp_Integration_RemovePacks_FromSI<br />
                        parameter SSCC/extcode and its value SSCC/extcode&nbsp; </td>
                </tr>
                <tr>
                    <td style="width: 249px; height: 37px;"><span style="font-weight: bold">SQL view:</span></td>
                    <td class="warning" style="width: 25%; height: 37px;">SI.dbo.vw_BoxesToCancel</td>
                </tr>
                <tr>
                    <td style="width: 249px">&nbsp;</td>
                    <td class="warning" style="width: 25%">&nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 249px; height: 22px;">&nbsp;</td>
                    <td style="height: 22px; width: 25%;" class="warning">&nbsp;</td>
                </tr>
              
                <tr>
                    <td style="width: 249px; height: 22px;">&nbsp;</td>
                    <td style="height: 22px; width: 25%;" class="warning">
                        &nbsp;</td>
                </tr>
              
            </table>
            <br />
             <asp:UpdatePanel ID="UpdatePanel_SSIS_SO" runat="server" UpdateMode="Conditional">
                  <ContentTemplate>
                   <table >
                       <tr>
                           <td style="height:25px;">DOTNo:</td>
                           <td style="height:25px;"><asp:TextBox ID="TextBox_PackDotNo" Width="500px" runat="server"></asp:TextBox></td>
                           <td style="height:25px;"><asp:Button ID="Button_RemovePacks" runat="server" Text="Remove" /></td>
                       </tr>
                   </table>
                      <asp:Label ID="Label_Result" runat="server" Text=""></asp:Label>
                        <br />
                 <asp:Button ID="Button_SSIS_SO_Process" runat="server" Text="Process" class="btn btn-primary" /> 
                 <asp:Button ID="Button_SSIS_SO_Refresh" runat="server" Text="Refresh" class="btn btn-success" /> 
                  </ContentTemplate>
             </asp:UpdatePanel>
        </div>
      </div>
    </div>
   
  
   
  </div>
    <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Alert Message:</h4>
        </div>
          <div class="modal-body">
              <asp:UpdatePanel ID="UpdatePanel_AlertMessage" runat="server" UpdateMode="Conditional">
                  <ContentTemplate>
                      <asp:Label ID="Label_AlertMessage" runat="server" Text=""></asp:Label>
                  </ContentTemplate>
              </asp:UpdatePanel>
              
          </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
</div>
</asp:Content>

