﻿Imports System.Data.SqlClient
Partial Class Integration_Burradon
    Inherits System.Web.UI.Page

    Sub PopUpMessage(ByVal messagestr As String)
        Label_AlertMessage.Text = messagestr
        UpdatePanel_AlertMessage.Update()
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "$('#myModal').modal();", True)
    End Sub


    Protected Sub Button_AutoFix_Click(sender As Object, e As EventArgs) Handles Button_AutoFix.Click
        Try
            Dim SQLStr As String = "EXEC msdb.dbo.sp_start_job N'Integration_AutoFix'"
            Dim tem As String = executeNonQuery(SQLStr, ConfigurationManager.ConnectionStrings("BurradonInnovaConnectionString").ConnectionString)
            If tem = "0" Then
                PopUpMessage("Process Completed")
            Else
                PopUpMessage(tem)
            End If
        Catch ex As Exception
            PopUpMessage(ex.Message.ToString)
        End Try
    End Sub

    Protected Sub LinkButton_ItgrImport_Reset_Click(sender As Object, e As EventArgs)
        Dim btn As LinkButton = sender
        Dim row As GridViewRow = btn.NamingContainer
        Dim pk As String = GridView_ITGr_Import.DataKeys(row.RowIndex).Value.ToString
        Dim SQLstr = "Update itgr_imports set importstatus=0 where id=" & pk
        executeNonQuery(SQLstr, ConfigurationManager.ConnectionStrings("BurradonInnovaConnectionString").ConnectionString)

        GridView_ITGr_Import.DataBind()
        UpdatePanel_Itgr_Import.Update()
    End Sub
    Protected Sub Button_Autofix_Refresh_Click(sender As Object, e As EventArgs) Handles Button_Autofix_Refresh.Click
        GridView_ITGr_Import.DataBind()
        UpdatePanel_Itgr_Import.Update()
    End Sub
    'Protected Sub LinkButton_ResendEmail_Click(sender As Object, e As EventArgs)
    '    Try
    '        Dim btn As LinkButton = sender
    '        Dim row As GridViewRow = btn.NamingContainer
    '        Dim pk As String = CDate(GridView_ICM.DataKeys(row.RowIndex).Value).ToString("yyyy-MM-dd")
    '        '  MsgBox("Exec [sp_ICM_Export_BySSIS] " & pk)
    '        ' Dim
    '        executeNonQuery("Exec [sp_ICM_Export_BySSIS] '" & pk & "'", ConfigurationManager.ConnectionStrings("BurradonInnovaConnectionString_Trust").ConnectionString)
    '        PopUpMessage("Reprocess ICM Process Complete")
    '    Catch ex As Exception
    '        PopUpMessage(ex.Message)
    '    End Try
    'End Sub
    Protected Sub LinkButton_Reprocess_pp_Click(sender As Object, e As EventArgs)
        Try
            Dim btn As LinkButton = sender
            Dim row As GridViewRow = btn.NamingContainer
            Dim orderId As String = GridView_PP.DataKeys(row.RowIndex).Values(1)
            executeNonQuery("DELETE FROM [tbl_PPImportLogs] WHERE [Id] = " & orderId, ConfigurationManager.ConnectionStrings("BurradonInnovaIntegrationConnectionString").ConnectionString)
            executeNonQuery("Exec [PP_Transfer_Lamb_BySSIS] '" & orderId & "'", ConfigurationManager.ConnectionStrings("BurradonInnovaConnectionString_Trust").ConnectionString)
        Catch ex As Exception
            PopUpMessage(ex.Message)
        End Try
    End Sub
    Protected Sub LinkButton_CarcaseTransfer_Reprocess_Click(sender As Object, e As EventArgs)
        Try
            Dim btn As LinkButton = sender
            Dim row As GridViewRow = btn.NamingContainer
            Dim DispatchDate As String = CDate(GridView_CarcaseTransfer.DataKeys(row.RowIndex).Value).ToString("yyyy-MM-dd")
            Dim tem As String = executeNonQuery("Exec Integration_TransferBurradonCarcase_SSIS '" & DispatchDate & "'", ConfigurationManager.ConnectionStrings("PrimalInnovaIntegrationConnectionString_Trust").ConnectionString)

            If tem = "0" Then
                PopUpMessage("Process completed!")
            Else
                PopUpMessage(tem)
            End If
            PopUpMessage("Process Done!")
        Catch ex As Exception
            PopUpMessage(ex.Message)
        End Try
    End Sub

    Protected Sub LinkButton_ResendICM_Click(sender As Object, e As EventArgs)
        Try
            Dim btn As LinkButton = sender
            Dim row As GridViewRow = btn.NamingContainer
            Dim DispatchDate As String = CDate(GridView_ICM.DataKeys(row.RowIndex).Value).ToString("yyyy-MM-dd")
            Dim tem As String = executeNonQuery("Exec sp_ICM_Export_BySSIS '" & DispatchDate & "'", ConfigurationManager.ConnectionStrings("BurradonInnovaConnectionString_Trust").ConnectionString)
            If tem = "0" Then
                PopUpMessage("Process completed!")
            Else
                PopUpMessage(tem)
            End If
            ' PopUpMessage(tem)
        Catch ex As Exception
            PopUpMessage(ex.Message)
        End Try
    End Sub
    Sub GetTriggerText()
        Try
            Dim sqlstr As String = "SELECT  DB_NAME() AS DataBaseName,  dbo.SysObjects.Name AS TriggerName, dbo.sysComments.Text AS SqlContent FROM dbo.SysObjects INNER JOIN  dbo.sysComments ON  dbo.SysObjects.ID = dbo.sysComments.ID WHERE (dbo.SysObjects.xType = 'TR')  AND dbo.SysObjects.Name = 'Trigger_SyncStockValue'"
            '   getScalarStr(sqlstr,)
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub GridView_ITGr_Import_SelectedIndexChanged(sender As Object, e As EventArgs) Handles GridView_ITGr_Import.SelectedIndexChanged
        Try
            Label_ITGR_Data.Text = ""
            Label_ITGR_Errors.Text = ""
            Dim id As String = GridView_ITGr_Import.SelectedDataKey.Value.ToString
            Dim SQLstr As String = "SELECT [errorinfo] FROM [Innova].[dbo].[itgr_importlogs] where errortext like '%" & id & "%' "
            Label_ITGR_Errors.Text = getScalarStr(SQLstr, ConfigurationManager.ConnectionStrings("BurradonInnovaConnectionString").ConnectionString) '.Replace(Char(13), vbCrLf)
            SQLstr = "SELECT convert(varchar(max),data) FROM itgr_imports where id=" & id
            Label_ITGR_Data.Text = getScalarStr(SQLstr, ConfigurationManager.ConnectionStrings("BurradonInnovaConnectionString").ConnectionString)
        Catch ex As Exception
            PopUpMessage(ex.Message)
        End Try

    End Sub
End Class
