﻿Imports System.Data
Imports System.Data.SqlClient
Partial Class Integration_Primal
    Inherits System.Web.UI.Page

    Sub PopUpMessage(ByVal messagestr As String)
        Label_AlertMessage.Text = messagestr
        UpdatePanel_AlertMessage.Update()
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "$('#myModal').modal();", True)
    End Sub


    Protected Sub Button_AutoFix_Click(sender As Object, e As EventArgs) Handles Button_AutoFix.Click
        Try
            Dim SQLStr As String = "EXEC msdb.dbo.sp_start_job N'Innova_ITGR_AutoFix'"
            Dim tem As String = executeNonQuery(SQLStr, ConfigurationManager.ConnectionStrings("PrimalInnovaConnectionString").ConnectionString)
            If tem = "0" Then
                PopUpMessage("Process Completed")
            Else
                PopUpMessage(tem)
            End If
        Catch ex As Exception
            PopUpMessage(ex.Message.ToString)
        End Try
    End Sub

    Protected Sub LinkButton_ItgrImport_Reset_Click(sender As Object, e As EventArgs)
        Dim btn As LinkButton = sender
        Dim row As GridViewRow = btn.NamingContainer
        Dim pk As String = GridView_ITGr_Import.DataKeys(row.RowIndex).Value.ToString
        Dim SQLstr = "Update itgr_imports set importstatus=0 where id=" & pk
        executeNonQuery(SQLstr, ConfigurationManager.ConnectionStrings("BurradonInnovaConnectionString").ConnectionString)

        GridView_ITGr_Import.DataBind()
        UpdatePanel_Itgr_Import.Update()
    End Sub
    Protected Sub Button_Autofix_Refresh_Click(sender As Object, e As EventArgs) Handles Button_Autofix_Refresh.Click
        GridView_ITGr_Import.DataBind()
        UpdatePanel_Itgr_Import.Update()
    End Sub
    'Protected Sub LinkButton_ResendEmail_Click(sender As Object, e As EventArgs)
    '    Try
    '        Dim btn As LinkButton = sender
    '        Dim row As GridViewRow = btn.NamingContainer
    '        Dim pk As String = CDate(GridView_ICM.DataKeys(row.RowIndex).Value).ToString("yyyy-MM-dd")
    '        '  MsgBox("Exec [sp_ICM_Export_BySSIS] " & pk)
    '        ' Dim
    '        executeNonQuery("Exec [sp_ICM_Export_BySSIS] '" & pk & "'", ConfigurationManager.ConnectionStrings("BurradonInnovaConnectionString_Trust").ConnectionString)
    '        PopUpMessage("Reprocess ICM Process Complete")
    '    Catch ex As Exception
    '        PopUpMessage(ex.Message)
    '    End Try
    'End Sub
    Protected Sub LinkButton_Reprocess_pp_Click(sender As Object, e As EventArgs)
        Try
            Dim btn As LinkButton = sender
            Dim row As GridViewRow = btn.NamingContainer
            Dim orderId As String = GridView_PP.DataKeys(row.RowIndex).Values(1)
            executeNonQuery("DELETE FROM [tbl_PPImportLogs] WHERE [Id] = " & orderId, ConfigurationManager.ConnectionStrings("PrimalInnovaIntegrationConnectionString").ConnectionString)
            executeNonQuery("Exec [PP_Transfer_Lamb_BySSIS] '" & orderId & "'", ConfigurationManager.ConnectionStrings("PrimalInnovaConnectionString_Trust").ConnectionString)
        Catch ex As Exception
            PopUpMessage(ex.Message)
        End Try
    End Sub
    Protected Sub LinkButton_CarcaseTransfer_Reprocess_Slaney_Click(sender As Object, e As EventArgs)
        Try
            Dim btn As LinkButton = sender
            Dim row As GridViewRow = btn.NamingContainer
            '  Dim DispatchDate As String = CDate(GridView_CarcaseTransfer.DataKeys(row.RowIndex).Value).ToString("yyyy-MM-dd")
            Dim tem As String = executeNonQuery("EXEC msdb.dbo.sp_start_job N'Import_SlaneyCarcase'", ConfigurationManager.ConnectionStrings("AMPSLindenCPACConnectionString").ConnectionString)

            If tem = "0" Then
                PopUpMessage("Process completed!")
            Else
                PopUpMessage(tem)
            End If
            PopUpMessage("Process Done!")
        Catch ex As Exception
            PopUpMessage(ex.Message)
        End Try
    End Sub

    Protected Sub LinkButton_ResendICM_Click(sender As Object, e As EventArgs)
        Try
            Dim btn As LinkButton = sender
            Dim row As GridViewRow = btn.NamingContainer
            Dim DispatchDate As String = CDate(GridView_ICM.DataKeys(row.RowIndex).Value).ToString("yyyy-MM-dd")
            Dim tem As String = executeNonQuery("Exec sp_ICM_Export_BySSIS '" & DispatchDate & "'", ConfigurationManager.ConnectionStrings("PrimalInnovaConnectionString_Trust").ConnectionString)
            If tem = "0" Then
                PopUpMessage("Process completed!")
            Else
                PopUpMessage(tem)
            End If
            ' PopUpMessage(tem)
        Catch ex As Exception
            PopUpMessage(ex.Message)
        End Try
    End Sub
    Sub GetTriggerText()
        Try
            Dim sqlstr As String = "SELECT  DB_NAME() AS DataBaseName,  dbo.SysObjects.Name AS TriggerName, dbo.sysComments.Text AS SqlContent FROM dbo.SysObjects INNER JOIN  dbo.sysComments ON  dbo.SysObjects.ID = dbo.sysComments.ID WHERE (dbo.SysObjects.xType = 'TR')  AND dbo.SysObjects.Name = 'Trigger_SyncStockValue'"
            '   getScalarStr(sqlstr,)
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub GridView_ITGr_Import_SelectedIndexChanged(sender As Object, e As EventArgs) Handles GridView_ITGr_Import.SelectedIndexChanged
        Try
            Label_ITGR_Data.Text = ""
            Label_ITGR_Errors.Text = ""
            Dim id As String = GridView_ITGr_Import.SelectedDataKey.Value.ToString
            Dim SQLstr As String = "SELECT [errorinfo] FROM [Innova].[dbo].[itgr_importlogs] where errortext like '%" & id & "%' "
            Label_ITGR_Errors.Text = getScalarStr(SQLstr, ConfigurationManager.ConnectionStrings("PrimalInnovaConnectionString").ConnectionString) '.Replace(Char(13), vbCrLf)
            SQLstr = "SELECT convert(varchar(max),data) FROM itgr_imports where id=" & id
            Label_ITGR_Data.Text = getScalarStr(SQLstr, ConfigurationManager.ConnectionStrings("PrimalInnovaConnectionString").ConnectionString)
        Catch ex As Exception
            PopUpMessage(ex.Message)
        End Try

    End Sub

    Protected Sub Button_ICM_Refresh_Click(sender As Object, e As EventArgs) Handles Button_ICM_Refresh.Click
        GridView_ICM.DataBind()
    End Sub
    Protected Sub LinkButton_DownloadSlaneyFile_Click(sender As Object, e As EventArgs) Handles LinkButton_DownloadSlaneyFile.Click
        Try
            Dim btn As LinkButton = sender
            Dim row As GridViewRow = btn.NamingContainer
            '  Dim DispatchDate As String = CDate(GridView_CarcaseTransfer.DataKeys(row.RowIndex).Value).ToString("yyyy-MM-dd")

            Dim tem As String = executeNonQuery("EXEC InsertIntoExtKilltrans", ConfigurationManager.ConnectionStrings("AMPSLindenCPACConnectionString").ConnectionString)
            If tem = "0" Then

                tem = executeNonQuery("EXEC lf9.msdb.dbo.sp_start_job N'Slaney and Newcastle carcase import Job'", ConfigurationManager.ConnectionStrings("AMPSLindenCPACConnectionString").ConnectionString)
                If tem = "0" Then
                    PopUpMessage("Process completed!")
                Else
                    PopUpMessage(tem)
                End If

            Else
                PopUpMessage(tem)
            End If
            PopUpMessage("Process Done!")
        Catch ex As Exception
            PopUpMessage(ex.Message)
        End Try
    End Sub
    Protected Sub LinkButton_CarcaseTransfer_Reprocess_EuroFarm_Click(sender As Object, e As EventArgs)
        Try
            Dim tem As String = executeNonQuery("EXEC  [dbo].[LindenKillInfo_ThridPartyImport]
		                                                @SupplierCode = N'2650',
		                                                @LotPreFix = N'EF',
		                                                @CompanyID = 297,
		                                                @RearedIn = N'prcn0022',
		                                                @BornIn = N'prcn0022',
		                                                @FatConfirmationClassIncl = 'Y',
		                                                @RebuildBarcode='Y',
		                                                @QAMode='Bordbia',
		                                                @QtrMode='13'
", ConfigurationManager.ConnectionStrings("PrimalInnovaIntegrationConnectionString").ConnectionString)

            PopUpMessage("Process Done!")
        Catch ex As Exception
            PopUpMessage(ex.Message)
        End Try
    End Sub
    Protected Sub LinkButton_TriggerEuroFarmImport_Click(sender As Object, e As EventArgs) Handles LinkButton_TriggerEuroFarmImport.Click
        Try
            ' Dim btn As LinkButton = sender
            ' Dim row As GridViewRow = btn.NamingContainer
            '  Dim DispatchDate As String = CDate(GridView_ICM.DataKeys(row.RowIndex).Value).ToString("yyyy-MM-dd")
            Dim tem As String = executeNonQuery("EXEC msdb.dbo.sp_start_job N'Import_EuroFarmCarcase'", ConfigurationManager.ConnectionStrings("PrimalInnovaConnectionString_Trust").ConnectionString)
            If tem = "0" Then
                PopUpMessage("Process completed!")
            Else
                PopUpMessage(tem)
            End If
            ' PopUpMessage(tem)
        Catch ex As Exception
            PopUpMessage(ex.Message)
        End Try
    End Sub



    Protected Sub LinkButtonPOReset_Innova2Nav_Click(sender As Object, e As EventArgs)
        Try
            Dim btn As LinkButton = sender
            Dim row As GridViewRow = btn.NamingContainer
            Dim POId As String = GridView_POInnova2Nav.DataKeys(row.RowIndex).Value
            Dim tem As String = executeNonQuery("Update [tblIntegration_PO_Innova_Primal2Nav_Primal] set SyncToNav='N', ErrorMessage =null where id=" & POId, ConfigurationManager.ConnectionStrings("LindenBridgeConnectionString").ConnectionString)
        Catch ex As Exception
            PopUpMessage(ex.Message)
        End Try
    End Sub
    Protected Sub Button_PO_Innova2Nav_Click(sender As Object, e As EventArgs) Handles Button_PO_Innova2Nav.Click
        Try
            Dim tem As String = executeNonQuery("EXEC msdb.dbo.sp_start_job N'Integration Primal Innova Transfer Wgt for PO'", ConfigurationManager.ConnectionStrings("LindenBridgeConnectionString").ConnectionString)
            If tem = "0" Then
                PopUpMessage("Process completed")
            Else
                PopUpMessage(tem)
            End If
        Catch ex As Exception
            PopUpMessage(ex.Message)
        End Try
    End Sub

    Protected Sub Button_SO_Innova2Nav_Click(sender As Object, e As EventArgs) Handles Button_SO_Innova2Nav.Click
        Try


            Dim ds As New Data.DataSet
            Dim da As New SqlDataAdapter
            da = New SqlClient.SqlDataAdapter("Select InnovaOrder from tblIntegration_SO_Innova_Primal2Nav_Primal where TranOrder.SyncToNav='N' and TranOrder.ErrorMessage is null", ConfigurationManager.ConnectionStrings("LindenBridgeConnectionString").ConnectionString)
            da.Fill(ds, "SO")
            da.Dispose()
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                Dim InnovaID As String = ds.Tables(0).Rows(i).Item("InnovaOrder").ToString()
                Dim tem As String = executeNonQuery("EXEC sp_Integration_SO_Innova_Primal_LB2Nav " & InnovaID, ConfigurationManager.ConnectionStrings("LindenBridgeConnectionString").ConnectionString)
                If tem = "0" Then
                    PopUpMessage("Process completed")
                Else
                    PopUpMessage(tem)
                End If
            Next
        Catch ex As Exception
            PopUpMessage(ex.Message)

        End Try
    End Sub
    Protected Sub LinkButton_SO_Innova2Nav_Click(sender As Object, e As EventArgs)
        Try

            Dim btn As LinkButton = sender
            Dim row As GridViewRow = btn.NamingContainer
            ' Dim InnovaID As String = GridView_SO_Innova2Nav.DataKeys(row.RowIndex).Value(1)
            Dim RecordID As String = GridView_SO_Innova2Nav.DataKeys(row.RowIndex).Value(0)
            Dim tem As String = executeNonQuery("Update tblIntegration_SO_Innova_Primal2Nav_Primal Set SyncToNav='N' , ErrorMessage=null where InnovaOrder=" & RecordID, ConfigurationManager.ConnectionStrings("LindenBridgeConnectionString").ConnectionString)
            If tem = "0" Then
                PopUpMessage("Process completed")
            Else
                PopUpMessage(tem)
            End If

        Catch ex As Exception
            PopUpMessage(ex.Message)
        End Try
    End Sub
    Protected Sub Button_Update_CarcaseValue_Click(sender As Object, e As EventArgs) Handles Button_Update_CarcaseValue.Click
        Try
            Dim tem As String = executeNonQuery("Exec sp_AutoUpdateHQvalue_ByDateRange '" & TextBox_DateFrom.Text & "','" & TextBox_DeateTo.Text & "'", ConfigurationManager.ConnectionStrings("PrimalInnovaConnectionString").ConnectionString)
            If tem = "0" Then
                PopUpMessage("Process completed")
            Else
                PopUpMessage(tem)
            End If
        Catch ex As Exception
            PopUpMessage(ex.Message)
        End Try

    End Sub
    Protected Sub LinkButton_DG_Lamb_Resend_Click(sender As Object, e As EventArgs)
        Try

            Dim btn As LinkButton = sender
            Dim row As GridViewRow = btn.NamingContainer

            Dim RecordID As String = GridView_DG_Lamb.DataKeys(row.RowIndex).Value
            Dim tem As String = executeNonQuery("Exec sp_ThirdParty_Email_DohertyAndGrayExportRountinLambCSV '" & RecordID & "'", ConfigurationManager.ConnectionStrings("PrimalInnovaConnectionString").ConnectionString)
            If tem = "0" Then
                PopUpMessage("Process completed")
            Else
                PopUpMessage(tem)
            End If
        Catch ex As Exception
            PopUpMessage(ex.Message)
        End Try
    End Sub
    Protected Sub LinkButton_DG_Beef_Resend_Click(sender As Object, e As EventArgs)
        Try

            Dim btn As LinkButton = sender
            Dim row As GridViewRow = btn.NamingContainer

            Dim RecordID As String = GridView_DG_Beef.DataKeys(row.RowIndex).Value
            '   MsgBox(RecordID)
            Dim tem As String = executeNonQuery("Exec sp_ThirdParty_Email_DohertyAndGrayExportRountinBeefCSV '" & RecordID & "'", ConfigurationManager.ConnectionStrings("PrimalInnovaConnectionString").ConnectionString)
            If tem = "0" Then
                PopUpMessage("Process completed")
            Else
                PopUpMessage(tem)
            End If
        Catch ex As Exception
            PopUpMessage(ex.Message)
        End Try
    End Sub
End Class
