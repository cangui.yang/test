﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" CodeFile="Integration_Burradon.aspx.vb" Inherits="Integration_Burradon" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">


<div class="container">
  <h2>Burradon Integration</h2>
  <p><strong>Note:Source Location \\LF-VM-TEST01\Visual Studio 2015</strong></p>
          <div class="panel-group" id="accordion">
             
 
      <div class="panel panel-info">
      <div class="panel-heading">
        <h4 class="panel-title">  <a data-toggle="collapse" data-parent="#accordion" href="#collapse11">
            SSIS Integration - ICM Export Integration</a></h4>
      </div>
      <div id="collapse11" class="panel-collapse collapse  in ">
        <div class="panel-body">
             <table class="table" >
                <tr >
                    <td style="width:25%"><strong>Hosting Server:</strong></td>
                    <td style="width:25%" class="warning" >LBSQL\INNOVA</td>
                    <td style="width:25%" class="danger"><strong>Advance Info:</strong></td>
                    <td style="width:25%" class="success"><strong>How to Resend the ICM File?</strong></td>             
                </tr>
                <tr >
                    <td><strong>Integration Services Catalogs:</strong></td>
                    <td class="warning">Integration_ICMReport</td>
                 
                    <td style="width:25%" rowspan="5" class="danger">Please note,Records shown below are on the last 21 days, if you want to resend file that older than 21 days, please contact sec line support.</td>
                    <td style="width:25%" rowspan="5" class="success">Click the &quot;Resend&quot; button next to the date you want to resend.</td>             
                 
                </tr>
                <tr>
                    <td ><strong>Integration Services Project:</strong></td>
                    <td class="warning">Integration_ICM_Burradon</td>
                </tr>
                <tr>
                    <td style="height: 37px" ><strong>Integration Services Packages:</strong></td>
                    <td class="warning" style="height: 37px">Package.dtsx</td>
                </tr>
                <tr>
                    <td><strong>SQL Agent Job:</strong></td>
                    <td class="warning">Export Lambs to ICM  </td>
                </tr>
                 <%--<asp:Parameter Name="Site" Type="String" />--%>
                <tr>
                    <td ><strong>Flow Chart:</strong></td>
                    <td class="warning">
                        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="\\lf-public\it\Application Integration Design & Doc\Burradon-ICM.vsd" Target="_blank">View Flow Chart</asp:HyperLink>
                    </td>
                </tr>
                 </table>
            <asp:UpdatePanel ID="UpdatePanel_ICM" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                       <asp:GridView ID="GridView_ICM" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource_ICM" AllowSorting="True" DataKeyNames="dispatchtime" class="table">
                           <Columns>
                               <asp:TemplateField>
                                   <ItemTemplate>
                                       <asp:LinkButton ID="LinkButton_ResendICM" runat="server" OnClick="LinkButton_ResendICM_Click">ReSend</asp:LinkButton>
                                       <ajaxToolkit:ConfirmButtonExtender ID="LinkButton_ResendICM_ConfirmButtonExtender" runat="server" BehaviorID="LinkButton_ResendICM_ConfirmButtonExtender" ConfirmText="Are you sure you want to resend the ICM File?" TargetControlID="LinkButton_ResendICM" />
                                   </ItemTemplate>
                               </asp:TemplateField>
                               <asp:BoundField DataField="dispatchtime" HeaderText="dispatchtime" SortExpression="dispatchtime" />
                               <asp:BoundField DataField="PlantNumber" HeaderText="PlantNumber" SortExpression="PlantNumber" />
                           </Columns>
                 <EmptyDataRowStyle BackColor="#33CC33" />
                 <EmptyDataTemplate>
                     There is no PO closed yet today.
                 </EmptyDataTemplate>
             </asp:GridView>
             <asp:SqlDataSource ID="SqlDataSource_ICM" runat="server" ConnectionString="<%$ ConnectionStrings:BurradonInnovaConnectionString %>" DeleteCommand="DELETE FROM [tbl_SSISIntegration_Log] WHERE [Id] = @Id" InsertCommand="INSERT INTO [tbl_SSISIntegration_Log] ([Value], [Type], [DateTimeInserted], [Processed], [ErrorMessage], [DateTimeProcessed]) VALUES (@Value, @Type, @DateTimeInserted, @Processed, @ErrorMessage, @DateTimeProcessed)" SelectCommand="Select distinct [dispatchtime],[PlantNumber] FROM [innova].[dbo].[vw_ITReport_ICM] where [dispatchtime]>=getdate()-21" UpdateCommand="UPDATE [tbl_SSISIntegration_Log] SET [Value] = @Value, [Type] = @Type, [DateTimeInserted] = @DateTimeInserted, [Processed] = @Processed, [ErrorMessage] = @ErrorMessage, [DateTimeProcessed] = @DateTimeProcessed WHERE [Id] = @Id" ProviderName="<%$ ConnectionStrings:BurradonInnovaConnectionString.ProviderName %>">
                 <DeleteParameters>
                     <asp:Parameter Name="Id" Type="Int32" />
                 </DeleteParameters>
              
             </asp:SqlDataSource>
                      <asp:UpdateProgress ID="UpdateProgress2" runat="server">
                        <ProgressTemplate>
                            Please wait... this will take few minutes!
                        </ProgressTemplate>
                    </asp:UpdateProgress>
             <br />
<%--                     <asp:Button ID="Button1" runat="server" Text="Process" class="btn btn-primary" />--%>
                    <asp:Button ID="Button2" runat="server" Text="Refresh" class="btn btn-success" /> 
                  
                </ContentTemplate>
            </asp:UpdatePanel>
          
        </div>
      </div>
    </div>
<div class="panel panel-info">
      <div class="panel-heading">
        <h4 class="panel-title">  <a data-toggle="collapse" data-parent="#accordion" href="#collapse12">
            SSIS Integration - Producer Payment Systemem</a></h4>
      </div>
      <div id="collapse12" class="panel-collapse collapse in">
        <div class="panel-body">
             <table class="table">
                <tr >
                    <td style="width:25%"><strong>Hosting Server:</strong></td>
                    <td style="width:25%" class="warning">LBSQL\INNOVA</td>
                    <td style="width:25%" class="danger" ><strong>Advance Info</strong></td>
                    <td style="width:25%" class="success"><strong>How to?</strong></td>
                </tr>
                <tr >
                    <td ><strong>Integration Services Catalogs:</strong></td>
                    <td class="warning">Producer Payments</td>
                    <td rowspan="5" class="danger">Please not, only the last 21 days records are shown on below list</td>
                    <td rowspan="5" class="success" >1. Click &#39;Delete&#39; to delete the record and wait for next process by server<br />
                        2. Click &#39;Reprocess&#39; to delete and trigger the process right away.</td>
                </tr>
                <tr>
                    <td ><strong>Integration Services Project:</strong></td>
                    <td class="warning">Integration_PP_Burradon</td>
                </tr>
                <tr>
                    <td ><strong>Integration Services Packages:</strong></td>
                    <td class="warning">Burradon PPLamb.dtsx</td>
                </tr>
                <tr>
                    <td ><strong>SQL Agent Job:</strong></td>
                    <td class="warning">PPLambImport</td>
                </tr>
                 <%--<asp:Parameter Name="Site" Type="String" />--%>
                <tr>
                    <td ><strong>Flow chart</strong></td>
                    <td class="warning">
                        <asp:HyperLink ID="HyperLink2" runat="server" Target="_blank" NavigateUrl="\\lf-public\IT\Application Integration Design & Doc\Burradon-ProducerPaymentSystem.vsd">View Flow Chart</asp:HyperLink>
                    </td>
                </tr>
                 </table>
            <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                       <asp:GridView Class="table" ID="GridView_PP" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource_BurradonPP" AllowSorting="True" DataKeyNames="Id, OrderId">
                           <Columns>
                               <asp:TemplateField>
                                   <ItemTemplate>
                                       <asp:LinkButton ID="LinkButton_Reprocess_pp" runat="server" OnClick="LinkButton_Reprocess_pp_Click">Reprocess</asp:LinkButton>
                                        <ajaxToolkit:ConfirmButtonExtender ID="LinkButton_Reprocess_ConfirmButtonExtender" runat="server" BehaviorID="LinkButton_Reprocess_ConfirmButtonExtender" ConfirmText="Are you sure you want to Reprocess this PP record?" TargetControlID="LinkButton_Reprocess_pp" />
                                   </ItemTemplate>
                               </asp:TemplateField>
                               <asp:TemplateField ShowHeader="False">
                                   <ItemTemplate>
                                       <asp:LinkButton ID="LinkButton_Delete_pp" runat="server" CausesValidation="False" CommandName="Delete" Text="Delete"></asp:LinkButton>
                                      <ajaxToolkit:ConfirmButtonExtender ID="LinkButton_Delete_pp_ConfirmButtonExtender" runat="server" BehaviorID="LinkButton_Delete_pp_ConfirmButtonExtender" ConfirmText="Are you sure you want to Delete this PP record?" TargetControlID="LinkButton_Delete_pp" />
                                   </ItemTemplate>
                               </asp:TemplateField>
                               <asp:BoundField DataField="Id" HeaderText="Id" SortExpression="Id" InsertVisible="False" ReadOnly="True" />
                               <asp:BoundField DataField="Regtime" HeaderText="Regtime" SortExpression="Regtime" />
                               <asp:BoundField DataField="AphisImport" HeaderText="AphisImport" SortExpression="AphisImport" />
                               <asp:BoundField DataField="MesImport" HeaderText="MesImport" SortExpression="MesImport" />
                               <asp:BoundField DataField="ErrorMessage" HeaderText="ErrorMessage" SortExpression="ErrorMessage" />
                               <asp:BoundField DataField="OrderId" HeaderText="OrderId" SortExpression="OrderId" />
                               <asp:BoundField DataField="AnimalType" HeaderText="AnimalType" SortExpression="AnimalType" />
                           </Columns>
                 <EmptyDataRowStyle BackColor="#33CC33" />
                 <EmptyDataTemplate>
                     There is no record.
                 </EmptyDataTemplate>
             </asp:GridView>
             <asp:SqlDataSource ID="SqlDataSource_BurradonPP" runat="server" ConnectionString="<%$ ConnectionStrings:BurradonInnovaIntegrationConnectionString %>" DeleteCommand="DELETE FROM [tbl_PPImportLogs] WHERE [Id] = @Id" InsertCommand="INSERT INTO [tbl_PPImportLogs] ([Regtime], [AphisImport], [MesImport], [ErrorMessage], [OrderId], [AnimalType]) VALUES (@Regtime, @AphisImport, @MesImport, @ErrorMessage, @OrderId, @AnimalType)" SelectCommand="SELECT Id, Regtime, AphisImport, MesImport, ErrorMessage, OrderId, AnimalType FROM tbl_PPImportLogs WHERE (Regtime &gt;= GETDATE() - 21) ORDER BY OrderId DESC" UpdateCommand="UPDATE [tbl_PPImportLogs] SET [Regtime] = @Regtime, [AphisImport] = @AphisImport, [MesImport] = @MesImport, [ErrorMessage] = @ErrorMessage, [OrderId] = @OrderId, [AnimalType] = @AnimalType WHERE [Id] = @Id">
                 <DeleteParameters>
                     <asp:Parameter Name="Id" Type="Int32" />
                 </DeleteParameters>
                 <InsertParameters>
                     <asp:Parameter Name="Regtime" Type="DateTime" />
                     <asp:Parameter Name="AphisImport" Type="Int32" />
                     <asp:Parameter Name="MesImport" Type="Int32" />
                     <asp:Parameter Name="ErrorMessage" Type="String" />
                     <asp:Parameter Name="OrderId" Type="String" />
                     <asp:Parameter Name="AnimalType" Type="String" />
                 </InsertParameters>
                 <UpdateParameters>
                     <asp:Parameter Name="Regtime" Type="DateTime" />
                     <asp:Parameter Name="AphisImport" Type="Int32" />
                     <asp:Parameter Name="MesImport" Type="Int32" />
                     <asp:Parameter Name="ErrorMessage" Type="String" />
                     <asp:Parameter Name="OrderId" Type="String" />
                     <asp:Parameter Name="AnimalType" Type="String" />
                     <asp:Parameter Name="Id" Type="Int32" />
                 </UpdateParameters>
             </asp:SqlDataSource>
             <br />
<%--                     <asp:Button ID="Button1" runat="server" Text="Process" class="btn btn-primary" />--%>
                    <asp:Button ID="Button1" runat="server" Text="Refresh" class="btn btn-success" /> 
                </ContentTemplate>
            </asp:UpdatePanel>
          
        </div>
      </div>
    </div>
<div class="panel panel-info">
      <div class="panel-heading">
        <h4 class="panel-title">  <a data-toggle="collapse" data-parent="#accordion" href="#collapse13">
            SSIS Integration - Transfer Carcases To Primal CPAC &amp; Innova</a></h4>
      </div>
      <div id="collapse13" class="panel-collapse collapse in">
        <div class="panel-body">
             <table class="table">
                <tr >
                    <td style="width: 249px"><strong>Hosting Server:</strong></td>
                    <td class="warning">LF-SQL01\INNOVA</td>
                    <td class="danger"><strong>Advance Info</strong></td>
                    <td class="success"><strong>How to</strong></td>
                </tr>
                <tr >
                    <td style="width:25%;"><strong>Integration Services Catalogs:</strong></td>
                    <td style="width:25%;" class="warning">Burradon Import</td>
                    <td style="width:25%;" rowspan="5" class="danger">This process is going to take over 10 minutes, so please wait if you hit 'Reprocess'</td>
                    <td style="width:25%;" rowspan="5" class="success">&nbsp;</td>
                </tr>
                <tr>
                    <td ><strong>Integration Services Project:</strong></td>
                    <td class="warning">Integration_BurradonCarcase_Import</td>
                </tr>
                <tr>
                    <td ><strong>Integration Packages:</strong></td>
                    <td class="warning">BurradonCarcaseImport.dtsx</td>
                </tr>
                <tr>
                    <td ><strong>SQL Agent Job:</strong></td>
                    <td class="warning">Import_BurradonCarcase</td>
                </tr>
                 <%--<asp:Parameter Name="Site" Type="String" />--%>
                <tr>
                    <td ><strong>Flow Chart:</strong></td>
                    <td class="warning">
                        <asp:HyperLink ID="HyperLink3" runat="server" NavigateUrl="\\lf-public\IT\Application Integration Design &amp; Doc\Burradon-Carcase Tranfer to CPAC.vsd" Target="_blank">View Flow Chart</asp:HyperLink>
                    </td>
                </tr>
                 </table>
            <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
             <asp:SqlDataSource ID="SqlDataSource_CarcaseTransfer" runat="server" ConnectionString="<%$ ConnectionStrings:BurradonInnovaConnectionString %>" SelectCommand="SELECT DISTINCT ProcOrd.dispatchtime FROM proc_items AS procItem INNER JOIN vw_Proc_Orders_ForDistQueries AS ProcOrd ON ProcOrd.[order] = procItem.[order] INNER JOIN proc_orderl AS procOrdLine ON ProcOrd.[order] = procOrdLine.[order] AND procItem.orderline = procOrdLine.id WHERE (ProcOrd.customer = 90) AND (procOrdLine.material IN (1283, 1284)) AND (procItem.regtime &gt; GETDATE() - 20) AND (ProcOrd.dispatchtime &gt; GETDATE() - 20) ORDER BY ProcOrd.dispatchtime DESC">
             </asp:SqlDataSource>
                       <asp:GridView Class="table" ID="GridView_CarcaseTransfer" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource_CarcaseTransfer" DataKeyNames="dispatchtime">
                           <Columns>
                               <asp:TemplateField>
                                   <ItemTemplate>
                                       <asp:LinkButton ID="LinkButton_CarcaseTransfer_Reprocess" runat="server" OnClick="LinkButton_CarcaseTransfer_Reprocess_Click">ReProcess</asp:LinkButton>
                                       <ajaxToolkit:ConfirmButtonExtender ID="LinkButton_CarcaseTransfer_Reprocess_ConfirmButtonExtender" runat="server" BehaviorID="LinkButton_CarcaseTransfer_Reprocess_ConfirmButtonExtender" ConfirmText="Are you sure you want to transfer Carcases on selected dispatch date? \n this Process might be taken few mintues to be completed!" TargetControlID="LinkButton_CarcaseTransfer_Reprocess" />
                                   </ItemTemplate>
                               </asp:TemplateField>
                               <asp:BoundField DataField="dispatchtime" HeaderText="dispatchtime" SortExpression="dispatchtime" DataFormatString="{0:dd/MM/yyyy}" />
                           </Columns>
                       </asp:GridView>
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:PrimalInnovaIntegrationConnectionString_Trust %>" SelectCommand="SELECT * FROM [base_companies]"></asp:SqlDataSource>
             <br />
<%--                     <asp:Button ID="Button1" runat="server" Text="Process" class="btn btn-primary" />--%>
                    <asp:Button ID="Button4" runat="server" Text="Refresh" class="btn btn-success" /> 
                    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                        <ProgressTemplate>
                            Please wait!....
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </ContentTemplate>
            </asp:UpdatePanel>
          
        </div>
      </div>
    </div>

    <div class="panel  panel-info">
      <div class="panel-heading">
        <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse6">
            SP Integration - Auto Fix</a></h4>
      </div>
      <div id="collapse6" class="panel-collapse collapse in">
        <div class="panel-body">  
            <table class="table">
                <tr >
                    <td style="width:25%"><strong>Hosting Server:</strong></td>
                    <td style="width:25%" class="warning">LBSQL\INNOVA</td>
                    <td style="width:25%" class="danger"><strong>Advance Info</strong></td>
                    <td style="width:25%" class="success"><strong>How to</strong></td>
                </tr>
                <tr >
                    <td ><strong>Database:</strong></td>
                    <td class="warning">LF-SQL01\UTILITY1.LindenBridge</td>
                    <td class="danger" rowspan="4" >&nbsp;</td>
                    <td class="success" rowspan="4" >1. Tick the check box to enable the auto fix<br />
                        2. click &#39;Reset&#39; to reset a import record </td>
                </tr>
                <tr >
                    <td ><strong>Table:</strong></td>
                    <td class="warning" >LindenBridge.dbo.tbl_AutoFix_Setting</td>
                </tr>
                <tr>
                    <td ><strong>Stored Procedures:</strong></td>
                    <td class="warning" >Innova.dbo.sp_Integration_Autofix</td>
                </tr>
                <tr>
                    <td ><strong>SQL Server Agent:</strong></td>
                    <td class="warning" >Integration_AutoFix</td>
                </tr>
             
              
            </table>
            <table >
                <tr>
                    <td style="vertical-align:top">
<asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                        <asp:GridView ID="GridView1" class="table" runat="server" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="Site,HandlerCode" DataSourceID="SqlDataSource_AutoFix">
                            <Columns>
                                <asp:CommandField ShowEditButton="True" />
                                <asp:BoundField DataField="HandlerCode" HeaderText="HandlerCode" ReadOnly="True" SortExpression="HandlerCode" />
                                <asp:BoundField DataField="Description" HeaderText="Description" ReadOnly="True" SortExpression="Description" />
                                <asp:CheckBoxField DataField="Status" HeaderText="Status" SortExpression="Status" />
                            </Columns>
                        </asp:GridView>
                        <asp:SqlDataSource ID="SqlDataSource_AutoFix" runat="server" ConnectionString="<%$ ConnectionStrings:LindenBridgeConnectionString %>" DeleteCommand="DELETE FROM [tbl_AutoFix_Setting] WHERE [Site] = @Site AND [HandlerCode] = @HandlerCode" InsertCommand="INSERT INTO [tbl_AutoFix_Setting] ([Site], [HandlerCode], [Status]) VALUES ('Kettyle', @HandlerCode, @Status)" SelectCommand="SELECT Id, Site, HandlerCode, Status, Description FROM tbl_AutoFix_Setting WHERE (Site = @Site) ORDER BY HandlerCode" UpdateCommand="UPDATE [tbl_AutoFix_Setting] SET [Status] = @Status WHERE [Site] = @Site AND [HandlerCode] = @HandlerCode">
                            <DeleteParameters>
                                <asp:Parameter Name="Site" Type="String" />
                                <asp:Parameter Name="HandlerCode" Type="String" />
                            </DeleteParameters>
                            <InsertParameters>
                                <%--<asp:Parameter Name="Site" Type="String" />--%>
                                <asp:Parameter Name="HandlerCode" Type="String" />
                                <asp:Parameter Name="Status" Type="Boolean" />
                            </InsertParameters>
                            <SelectParameters>
                                <asp:Parameter DefaultValue="Burradon" Name="Site" Type="String" />
                            </SelectParameters>
                            <UpdateParameters>
                                <asp:Parameter Name="Id" Type="Int32" />
                                <asp:Parameter Name="Status" Type="Boolean" />
                                <asp:Parameter Name="Site" Type="String" />
                                <asp:Parameter Name="HandlerCode" Type="String" />
                            </UpdateParameters>
                        </asp:SqlDataSource>
                                <asp:Button ID="Button_AutoFix" runat="server" class="btn btn-primary" Text="Process" />
                                <ajaxToolkit:ConfirmButtonExtender ID="Button_AutoFix_ConfirmButtonExtender" runat="server" BehaviorID="Button_AutoFix_ConfirmButtonExtender" ConfirmText="Are you sure you want to run the auto fix now?" TargetControlID="Button_AutoFix" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                    <td style="vertical-align:top">
<asp:UpdatePanel ID="UpdatePanel_Itgr_Import" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                               
                                    <asp:GridView ID="GridView_ITGr_Import" class="table" runat="server" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="id" DataSourceID="SqlDataSource_ItgrImport_log" Width="635px">
                                        <Columns>
                                            <asp:CommandField ShowSelectButton="True" />
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="LinkButton_ItgrImport_Reset" runat="server" OnClick="LinkButton_ItgrImport_Reset_Click">Reset</asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="id" HeaderText="id" InsertVisible="False" ReadOnly="True" SortExpression="id" />
                                            <asp:BoundField DataField="regtime" HeaderText="regtime" SortExpression="regtime" />
                                            <asp:BoundField DataField="importstatus" HeaderText="importstatus" SortExpression="importstatus" />
                                            <asp:BoundField DataField="importhandlercode" HeaderText="handlercode" SortExpression="importhandlercode" />
                                        </Columns>
                                        <EmptyDataRowStyle BackColor="#009900" />
                                        <EmptyDataTemplate>
                                            There is no Errors in ITGR_Import
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                  
                              
                                <asp:Label ID="Label_ITGR_Data" runat="server" Text="" /><br />     
                                <asp:Label ID="Label_ITGR_Errors" runat="server" Text="" /><br />                       
                                <asp:SqlDataSource ID="SqlDataSource_ItgrImport_log" runat="server" ConnectionString="<%$ ConnectionStrings:BurradonInnovaConnectionString %>" SelectCommand="SELECT id, regtime, importstatus, importhandlercode, data FROM itgr_imports WHERE importstatus=3 and (CONVERT (varchar(10), regtime, 120) = CONVERT (varchar(10), GETDATE(), 120))"></asp:SqlDataSource>
                
                                <asp:Button ID="Button_Autofix_Refresh" runat="server" class="btn btn-success" Text="Refresh" />
                
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
            </table>
        </div>
      </div>
    </div>
 <div class="panel  panel-info">
      <div class="panel-heading">
        <h4 class="panel-title">  <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">
            Triggers - Stock Value</a></h4>
      </div>
      <div id="collapse4" class="panel-collapse collapse in ">
        <div class="panel-body">
             <table class="table" >
                <tr >
                    <td style="width:25%; height: 22px;"><strong>Hosting Server:</strong></td>
                    <td style="width:25%; height: 22px;" class="warning">LBSQL\Utility1</td>
                    <td style="width:25%; height: 22px;" class="danger"><strong>Advance Info</strong></td>
                    <td style="width:25%; height: 22px;" class="success"><strong>How to</strong></td>
                <tr >
                    <td><strong>Database:</strong></td>
                    <td class="warning">AMPSWBBeef</td>
                    <td class="danger" rowspan="4">Update Stockvalue to LB Innova<br />
                        [lbsql\INNOVA].[Innova].[dbo].[proc_individuals]</td>
                    <td class="success">&nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 249px; height: 22px;"><strong>Table:</strong></td>
                    <td style="height: 22px; width: 6px;" class="warning">KILLTRANS</td>
                    <td style="height: 22px; width: 6px;" class="success">&nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 249px"><strong>Trigger:</strong></td>
                    <td style="width: 6px" class="warning">Trigger_SyncStockValue</td>
                    <td style="width: 6px" class="success">&nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 249px; height: 23px;">&nbsp;</td>
                    <td style="height: 23px; width: 6px;" class="warning">&nbsp;</td>
                    <td style="height: 23px; width: 6px;" class="success">&nbsp;</td>
                </tr>
                 <%--<asp:Parameter Name="Site" Type="String" />--%>
            </table>
            <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
             <br />
<%--                     <asp:Button ID="Button1" runat="server" Text="Process" class="btn btn-primary" />--%>
                    &nbsp; 
                </ContentTemplate>
            </asp:UpdatePanel>
          
        </div>
      </div>
    </div>
  </div> 
    <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;  <div class="modal-header">
              </div>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Alert Message:</h4>
        </div>
          <div class="modal-body">
              <asp:UpdatePanel ID="UpdatePanel_AlertMessage" runat="server" UpdateMode="Conditional">
                  <ContentTemplate>
                      <asp:Label ID="Label_AlertMessage" runat="server" Text=""></asp:Label>
                  </ContentTemplate>
              </asp:UpdatePanel>
              
          </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
</div>
</asp:Content>

