﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" CodeFile="Integration_Home.aspx.vb" Inherits="IT_Integration_Home" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <div class="container">
        <p></p>
    <div class="panel panel-primary">
        <div class="panel-heading">
            Sites Integration</div>
        <div class="panel-body">
            <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/IT/Integration_Primal.aspx" Target="_blank">Primal Integration</asp:HyperLink>
            <br />
             <asp:HyperLink ID="HyperLink4" runat="server" NavigateUrl="~/IT/Integration_Primal_SI.aspx" Target="_blank">Primal-SI Integration</asp:HyperLink>
            <br />
            <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/IT/Integration_Kettyle.aspx" Target="_blank">Kettyle Integration</asp:HyperLink>
            <br />
          <asp:HyperLink ID="HyperLink3" runat="server" NavigateUrl="~/IT/Integration_Burradon.aspx" Target="_blank">Burradon Integration</asp:HyperLink></div>
    </div>
<div class="panel panel-success">
      <div class="panel-heading">Other Integration</div>
      <div class="panel-body">
      </div>
    </div>

        </div>
</asp:Content>

