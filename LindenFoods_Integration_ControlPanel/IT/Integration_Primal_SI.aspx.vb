﻿Imports System.Data.SqlClient
Partial Class Integration_Primal_SI
    Inherits System.Web.UI.Page

    Sub PopUpMessage(ByVal messagestr As String)
        Label_AlertMessage.Text = messagestr
        UpdatePanel_AlertMessage.Update()
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "$('#myModal').modal();", True)
    End Sub


    'Protected Sub Button_AutoFix_Click(sender As Object, e As EventArgs) Handles Button_AutoFix.Click
    '    Try
    '        Dim SQLStr As String = "EXEC msdb.dbo.sp_start_job N'Integration_Kettyle_Autofix'"
    '        executeNonQuery(SQLStr, ConfigurationManager.ConnectionStrings("KFInnovaIntegrationConnectionString").ConnectionString)
    '        PopUpMessage("Process Completed")
    '    Catch ex As Exception
    '        PopUpMessage(ex.Message.ToString)
    '    End Try
    'End Sub

    'Protected Sub LinkButton_ItgrImport_Reset_Click(sender As Object, e As EventArgs)
    '    Dim btn As LinkButton = sender
    '    Dim row As GridViewRow = btn.NamingContainer
    '    Dim pk As String = GridView_ITGr_Import.DataKeys(row.RowIndex).Value.ToString
    '    Dim SQLstr = "Update itgr_imports set importstatus=0 where id=" & pk
    '    executeNonQuery(SQLstr, ConfigurationManager.ConnectionStrings("KettyleInnovaConnectionString").ConnectionString)

    '    GridView_ITGr_Import.DataBind()
    '    UpdatePanel_Itgr_Import.Update()
    'End Sub



    Protected Sub Button_RemovePacks_Click(sender As Object, e As EventArgs) Handles Button_RemovePacks.Click
        Try
            Label_Result.Text = String.Empty
            Dim packkeys As String() = TextBox_PackDotNo.Text.Split(",")
            If UBound(packkeys) > 0 Then
                For i As Integer = 0 To UBound(packkeys) - 1
                    Dim SQLStr_PacksInInnova As String = "Select SSCC from [proc_packs] where extcode='" & packkeys(i) & "' or SSCC='" & packkeys(i) & "'"
                    Dim SQLStr_PacksInSI As String = "Select right(batchno,8) as DotNo from inventoryaudit where right(batchno,8)=right(" & packkeys(i) & ",8)"
                    Dim Str_innovaResult As String = getScalarStr(SQLStr_PacksInInnova, ConfigurationManager.ConnectionStrings("PrimalInnovaConnectionString").ConnectionString)
                    Dim Str_SIResult As String = getScalarStr(SQLStr_PacksInSI, ConfigurationManager.ConnectionStrings("SIConnectionString").ConnectionString)
                    If Str_innovaResult = "0" Then
                        Label_Result.Text = Label_Result.Text & "<br /> Pack " & packkeys(i) & " Not Found In Innova "
                    Else
                        Label_Result.Text = Label_Result.Text & "<br /> Pack " & packkeys(i) & " Found In Innova "
                        Dim Str_innovaResult_NoneQ As String = executeNonQuery("EXEC sp_Integration_RemovePacks_FromSI 'SSCC','" & Str_innovaResult.Trim & "'", ConfigurationManager.ConnectionStrings("PrimalInnovaConnectionString").ConnectionString)
                        If Str_innovaResult_NoneQ = "0" Then
                            Label_Result.Text = Label_Result.Text & "<br /> XML File have been generated to LF-SQL\Innova.innova.dbo.itgr_import"
                        End If
                    End If
                    If Str_SIResult = "0" Then
                        Label_Result.Text = Label_Result.Text & "<br /> Pack " & packkeys(i) & " Not Found In SI "
                    Else
                        Label_Result.Text = Label_Result.Text & "<br /> Pack " & packkeys(i) & " Found In SI "
                        Label_Result.Text = Label_Result.Text & "<br /> Please operate the removal manually in SI database"
                    End If


                    '  executeNonQuery(SQLStr_PacksInInnova, ConfigurationManager.ConnectionStrings("KettyleInnovaConnectionString").ConnectionString)

                Next

            End If

        Catch ex As Exception
            PopUpMessage(ex.Message.ToString)
        End Try
    End Sub
End Class
