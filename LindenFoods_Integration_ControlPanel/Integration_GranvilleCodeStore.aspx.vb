﻿
Partial Class Integration_GranvilleCodeStore
    Inherits System.Web.UI.Page

    Protected Sub Button_Search_Click(sender As Object, e As EventArgs) Handles Button_Search.Click

        Dim temStr As String = TextBox_PalletIDs.Text
        If temStr IsNot String.Empty Then
            temStr = TextBox_PalletIDs.Text.Replace("'", "")
            Dim temstrs As String() = temStr.Split(",")

            If UBound(temstrs) = 0 Then
                TextBox_PalletIDs.Text = "'" & temstrs(0) & "'"
            Else
                TextBox_PalletIDs.Text = String.Empty
                For i As Integer = 0 To UBound(temstrs)
                    TextBox_PalletIDs.Text = TextBox_PalletIDs.Text & "'" & temstrs(i) & "',"
                Next
                TextBox_PalletIDs.Text = Left(TextBox_PalletIDs.Text, TextBox_PalletIDs.Text.Length - 1)
            End If
            SqlDataSource.SelectCommand = "SELECT DISTINCT PalletID, SyncToInnova, convert(varchar(10),ImportedDateTime,120) as ImportedDate  FROM [tbl_Granville_ReturnPallets] WHERE ([PalletID] in (" & TextBox_PalletIDs.Text & ")) ORDER BY [PalletID]"
            SqlDataSource.DataBind()
            GridView1.DataBind()

        End If

    End Sub
    Protected Sub LinkButton_Reset(sender As Object, e As EventArgs)
        Dim btn As LinkButton = sender
        Dim row As GridViewRow = btn.NamingContainer
        Dim pk As String = GridView1.DataKeys(row.RowIndex).Value.ToString

        LindenBridge_executeNonQuery("Update [tbl_Granville_ReturnPallets] set [SyncToInnova]='N' where PalletID ='" & pk & "'")
        LindenBridge_executeNonQuery("Exec [LF-SQL01\INNOVA].Innova.dbo.sp_Import_Granville_ReturnPallets")
    End Sub

End Class
