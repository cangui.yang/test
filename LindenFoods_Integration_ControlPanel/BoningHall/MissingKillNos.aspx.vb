﻿
Partial Class BoningHall_MissingKillNos
    Inherits System.Web.UI.Page
    Dim DataKey_number As String
    Dim DataKey_Qtr As String
    Dim DataKey_Regtime As String
    Dim DataKey_Regtime_From As String
    Dim DataKey_Regtime_To As String
    Protected Sub GridView_WithMissingKillNo_SelectedIndexChanged(sender As Object, e As EventArgs) Handles GridView_WithMissingKillNo.SelectedIndexChanged
        Try



            '  Label_ItemNo.Text = GridView_WithMissingKillNo.DataKeys(GridView_WithMissingKillNo.SelectedIndex).Values(0)
            DataKey_Qtr = GridView_WithMissingKillNo.DataKeys(GridView_WithMissingKillNo.SelectedIndex).Values(1)

            ' DataKey_Regtime = GridView_WithMissingKillNo.DataKeys(GridView_WithMissingKillNo.SelectedIndex).Values(2)
            '  DataKey_Regtime_From = GridView_WithMissingKillNo.DataKeys(GridView_WithMissingKillNo.SelectedIndex).Values(3)
            ' DataKey_Regtime_To = GridView_WithMissingKillNo.DataKeys(GridView_WithMissingKillNo.SelectedIndex).Values(4)
            '   UpdatePanel_MissingQtr.Update()
            'SqlDataSource_MissingItem.SelectCommand = "SELECT proc_matxacts.weight, CONVERT (varchar(20), proc_matxacts.regtime, 120) AS regtime, proc_items.number, proc_individuals.idcode, proc_individuals.ricountry, proc_individuals.killnumber, CASE WHEN SUBSTRING(CONVERT (varchar(20) , proc_items.number) , 4 , 1) IN ('1' , '3') THEN 'HQ' ELSE 'FQ' END AS Qtr FROM proc_matxacts INNER JOIN proc_items ON proc_matxacts.item = proc_items.id INNER JOIN proc_individuals ON proc_items.individual = proc_individuals.id INNER JOIN proc_lots ON proc_individuals.lot = proc_lots.lot WHERE (CONVERT (varchar(10), proc_matxacts.prday, 120) >= CONVERT (varchar(10), GETDATE() - 2, 120)) AND (proc_matxacts.xactpath = 5) AND (proc_matxacts.artype = 3) AND (CASE WHEN SUBSTRING(CONVERT (varchar(20) , proc_items.number) , 4 , 1) IN ('1' , '3') THEN 'HQ' ELSE 'FQ' END = '" & DataKey_Qtr & "') AND (CONVERT (varchar(20), proc_matxacts.regtime, 120) BETWEEN '" & DataKey_Regtime_From & "' AND '" & DataKey_Regtime_To & "')"
            SqlDataSource_MissingItem.SelectCommand = "SELECT        proc_matxacts.weight, CONVERT(varchar(20), proc_matxacts.regtime, 120) AS regtime, proc_items.number, proc_individuals.idcode, proc_individuals.ricountry, proc_individuals.killnumber, 
                         CASE WHEN SUBSTRING(CONVERT(varchar(20), proc_items.number), 4, 1) IN ('1', '3') THEN 'HQ' ELSE 'FQ' END AS Qtr,proc_matxacts.item
FROM            proc_matxacts INNER JOIN
                         proc_items ON proc_matxacts.item = proc_items.id INNER JOIN
                         proc_individuals ON proc_items.individual = proc_individuals.id INNER JOIN
                         proc_lots ON proc_individuals.lot = proc_lots.lot
WHERE        (CONVERT(varchar(10), proc_matxacts.prday, 120) = CONVERT(varchar(10), CONVERT(datetime, '" & TextBox_Date.Text & "', 103), 120)) AND (CONVERT(varchar(10), proc_matxacts.regtime, 120) = CONVERT(varchar(10), CONVERT(datetime, '" & TextBox_Date.Text & "', 103), 120)) AND (proc_matxacts.xactpath = 5) AND (proc_matxacts.artype = 3) AND 
                         (CASE WHEN SUBSTRING(CONVERT(varchar(20), proc_items.number), 4, 1) IN ('1', '3') THEN 'HQ' ELSE 'FQ' END = '" & DataKey_Qtr & "') 
						 
						 
						 AND (proc_individuals.idcode IN
                             (SELECT        proc_individuals.idcode
                               FROM            proc_matxacts INNER JOIN
                                                         proc_items ON proc_matxacts.item = proc_items.id INNER JOIN
                                                         proc_individuals ON proc_items.individual = proc_individuals.id INNER JOIN
                                                         proc_lots ON proc_individuals.lot = proc_lots.lot
                               WHERE        (CONVERT(varchar(10), proc_matxacts.prday, 120) = CONVERT(varchar(10), CONVERT(datetime, '" & TextBox_Date.Text & "', 103), 120)) AND (proc_matxacts.xactpath = 5) AND (proc_matxacts.artype = 3) AND (CONVERT(varchar(10), proc_matxacts.regtime, 120) 
                                                         = CONVERT(varchar(10), CONVERT(datetime, '" & TextBox_Date.Text & "', 103), 120))
                               GROUP BY CASE WHEN SUBSTRING(CONVERT(varchar(20), proc_items.number), 4, 1) IN ('1', '3') THEN 'HQ' ELSE 'FQ' END, proc_individuals.killnumber, proc_individuals.idcode
                               HAVING         (COUNT(*) = 1)))
"
            SqlDataSource_MissingItem.DataBind()

            GridView_KillNoSelected.DataBind()
            GridView_KillNoSelected.DataBind()
            '    UpdatePanel_ReplacebyQtr.Update()
            ' MsgBox(Label_ItemNo.Text & "-" & Label_Qtr.Text & "-" & Label_RegTime_To.Text)
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        'TextBox_Date.Text = Now.Date.ToString("dd/MM/yyyy")
        'SqlDataSource_Innova_Items.DataBind()
        'GridView_WithMissingKillNo.DataBind()

    End Sub
    Protected Sub TextBox_Date_TextChanged(sender As Object, e As EventArgs) Handles TextBox_Date.TextChanged
        SqlDataSource_Innova_Items.DataBind()
        GridView_WithMissingKillNo.DataBind()
    End Sub

End Class
