﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" CodeFile="MissingKillNos.aspx.vb" Inherits="BoningHall_MissingKillNos" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <div class="container">
  <h2>Re-Locate Missing Qtr</h2>
  <p><strong>Note:</strong> Only Qtrs with KillNo 9999 are allow to re-located </p>
         <asp:UpdatePanel ID="UpdatePanel_MissingQtr" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
    <table>
        <tr><td>
            <asp:Label ID="Label1" runat="server" Text="Date:"></asp:Label>&nbsp;<asp:TextBox ID="TextBox_Date" runat="server" AutoPostBack="True"></asp:TextBox>
            <ajaxToolkit:CalendarExtender ID="TextBox_Date_CalendarExtender" runat="server" BehaviorID="TextBox_Date_CalendarExtender" TargetControlID="TextBox_Date" Format="dd/MM/yyyy" />
            </td><td></td></tr>
        <tr>
            <td>
               
                        <asp:Panel ID="Panel1" runat="server" Height="300px" BorderColor="Red" BorderStyle="Solid" BorderWidth="2px" ScrollBars="Auto">

                            <asp:GridView ID="GridView_WithMissingKillNo" runat="server" AutoGenerateColumns="False" CellPadding="4" DataSourceID="SqlDataSource_Innova_Items" DataKeyNames="number,Qtr,regtime,regtimeFrom,regtimeTo" ForeColor="#333333" GridLines="None" AllowSorting="True">
                                <%--<AlternatingRowStyle BackColor="White" />--%>
                                <Columns>
                                    <asp:CommandField ShowSelectButton="True" />
                                     <asp:BoundField DataField="killnumber" HeaderText="KillNo" SortExpression="killnumber" />
                                    <asp:BoundField DataField="weight" HeaderText="Wgt" SortExpression="weight" />
                                    <asp:BoundField DataField="regtime" HeaderText="Regtime" SortExpression="regtime" />
                                    <asp:BoundField DataField="Qtr" HeaderText="Qtr" ReadOnly="True" SortExpression="Qtr" />
                      
                                </Columns>
                                <EmptyDataTemplate>
                                    No Records Found
                                </EmptyDataTemplate>
                                <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                                <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                <SortedAscendingCellStyle BackColor="#FDF5AC" />
                                <SortedAscendingHeaderStyle BackColor="#4D0000" />
                                <SortedDescendingCellStyle BackColor="#FCF6C0" />
                                <SortedDescendingHeaderStyle BackColor="#820000" />
                            </asp:GridView>
                            <asp:SqlDataSource ID="SqlDataSource_Innova_Items" runat="server" ConnectionString="<%$ ConnectionStrings:PrimalInnovaConnectionString %>" SelectCommand="SELECT proc_matxacts.weight, CONVERT (varchar(20), proc_matxacts.regtime, 120) AS regtime, CONVERT (varchar(20), DATEADD(mi, - 2, proc_matxacts.regtime), 120) AS regtimeFrom, CONVERT (varchar(20), DATEADD(mi, 2, proc_matxacts.regtime), 120) AS regtimeTo, proc_items.number, proc_individuals.idcode, proc_individuals.ricountry, proc_individuals.killnumber, CASE WHEN SUBSTRING(CONVERT (varchar(20) , proc_items.number) , 4 , 1) IN ('1' , '3') THEN 'HQ' ELSE 'FQ' END AS Qtr FROM proc_matxacts INNER JOIN proc_items ON proc_matxacts.item = proc_items.id INNER JOIN proc_individuals ON proc_items.individual = proc_individuals.id INNER JOIN proc_lots ON proc_individuals.lot = proc_lots.lot WHERE (CONVERT (varchar(10), proc_matxacts.prday, 120) =CONVERT(varchar(10), convert(datetime,@SelectDate, 103),120) ) AND (proc_matxacts.xactpath = 5) AND (proc_matxacts.artype = 3) AND (proc_individuals.killnumber = 9999)">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="TextBox_Date" Name="SelectDate" PropertyName="Text" />
                                </SelectParameters>
                            </asp:SqlDataSource>

                  
                            <asp:Label ID="Label_ItemNo" runat="server" Text=""></asp:Label>
                             <br />
                             <asp:Label ID="Label_RegTime" runat="server" Text=""></asp:Label>
                            <br />
                            <asp:Label ID="Label_RegTime_From" runat="server" Text=""></asp:Label>
                             <br />
                             <asp:Label ID="Label_RegTime_To" runat="server" Text=""></asp:Label>
                            <br />
                            <asp:Label ID="Label_Qtr" runat="server" Text=""></asp:Label>

                  
                        </asp:Panel>            
                    
                 
            </td>
        
      <td>
     
                        <asp:Panel ID="Panel2" runat="server" Height="300px" Style="margin-left:50px" BorderColor="#009900" BorderStyle="Solid" BorderWidth="2px" ScrollBars="Auto">
                            <asp:GridView ID="GridView_KillNoSelected" runat="server" CellPadding="4" DataSourceID="SqlDataSource_MissingItem" ForeColor="#333333" GridLines="None" AutoGenerateColumns="False" AllowSorting="True" DataKeyNames="item,killnumber">
                                <AlternatingRowStyle BackColor="White" />
                                <Columns>
                                    <asp:CommandField ShowSelectButton="True" />
                                    <asp:BoundField DataField="killnumber" HeaderText="KillNo" SortExpression="killnumber" />
                                    <asp:BoundField DataField="weight" HeaderText="Wgt" SortExpression="weight" />
                                    <asp:BoundField DataField="regtime" HeaderText="Regtime" ReadOnly="True" SortExpression="regtime" />
                                    <asp:BoundField DataField="Qtr" HeaderText="Qtr" ReadOnly="True" SortExpression="Qtr" />
                                    <asp:BoundField DataField="idcode" HeaderText="Eartag" SortExpression="idcode" />
                                </Columns>
                                <EditRowStyle BackColor="#7C6F57" />
                                <EmptyDataTemplate>
                                    No Records Found
                                </EmptyDataTemplate>
                                <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                                <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                                <RowStyle BackColor="#E3EAEB" />
                                <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                                <SortedAscendingCellStyle BackColor="#F8FAFA" />
                                <SortedAscendingHeaderStyle BackColor="#246B61" />
                                <SortedDescendingCellStyle BackColor="#D4DFE1" />
                                <SortedDescendingHeaderStyle BackColor="#15524A" />
                            </asp:GridView>
                            <asp:SqlDataSource ID="SqlDataSource_MissingItem" runat="server" ConnectionString="<%$ ConnectionStrings:PrimalInnovaConnectionString %>" SelectCommand="">
                          
                               
                            </asp:SqlDataSource>
                        </asp:Panel>

            </td>
        </tr>
        <tr><td></td><td style="height: 10px"></td></tr>
         <tr><td colspan="2">
             <asp:Button ID="Button_Merge" runat="server" Text="Merge Selected Carcases" class="btn btn-success" /> 
             </td></tr>
    </table>
           </ContentTemplate>
                </asp:UpdatePanel>
    </div>
</asp:Content>

