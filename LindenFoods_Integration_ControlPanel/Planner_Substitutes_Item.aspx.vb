﻿Imports System.Data.SqlClient

Partial Class Planner_Substitutes_Item
    Inherits System.Web.UI.Page

    <System.Web.Script.Services.ScriptMethod(), System.Web.Services.WebMethod()>
    Public Shared Function SearchProducts(ByVal prefixText As String, ByVal count As Integer) As List(Of String)
        Dim Products As List(Of String) = New List(Of String)
        Using cmd As SqlCommand = New SqlCommand("Select distinct top 50 [No_] as No, Description FROM [Dynamics2009].[dbo].[Linden Foods Retail$Item] where [No_] Like '" & prefixText.Replace("'", "") & "%'", New SqlConnection(ConfigurationManager.ConnectionStrings("Dynamics2009ConnectionString").ToString))
            cmd.Connection.Open()
            Using reader As SqlDataReader = cmd.ExecuteReader
                While reader.Read
                    Products.Add(reader("No").ToString & " - " & reader("Description").ToString)
                End While
            End Using

            cmd.Connection.Close()
            cmd.Dispose()
        End Using
        Return Products

    End Function


    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

    End Sub
    Protected Sub Button_LoadBomProduct_Click(sender As Object, e As EventArgs) Handles Button_LoadBomProduct.Click
        Dim finishedProduct As String() = TextBox_FinishedProduct.Text.Split("-")
        Dim sqlstring As String = "SELECT [No_], [No_]+'-'+[Description] as Description, [Production Type] AS Production_Type, [Production BOM No_] AS Production_BOM_No_ FROM [Linden Foods Retail$Production BOM Line] where [Production BOM No_]='" & finishedProduct(0) & "'"
        SqlDataSource_Bom.SelectCommand = sqlstring
        SqlDataSource_Bom.DataBind()
        DropDownList1.DataBind()
    End Sub
End Class
