﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" CodeFile="Planner_Substitutes.aspx.vb" Inherits="Planner_Substites" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <table>
        <tr>
            <td>Production Date:</td>
            <td>
                <asp:TextBox ID="TextBox_ProductionDate" runat="server" AutoPostBack="True"></asp:TextBox>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="TextBox_ProductionDate" />
            </td>
            <td><asp:Button ID="Button_AddSubstitutes" runat="server" Text="Add New Substitute" /></td>
            <td><asp:Button ID="Button_ReleaseWorksOrders" runat="server" Text="Release Work Orders" /></td>
        </tr>
    </table>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:PlanningConnectionString %>" DeleteCommand="DELETE FROM [Planner_substitutes] WHERE [Id] = @Id" InsertCommand="INSERT INTO [Planner_substitutes] ([ProductCode], [SubstituteCode], [ProductionDate], [Reason], [CreatedByUser], [CreatedDatetime], [LastUpdatedDatetime], [FinishedProduct], [Priority]) VALUES (@ProductCode, @SubstituteCode, @ProductionDate, @Reason, @CreatedByUser, @CreatedDatetime, @LastUpdatedDatetime, @FinishedProduct, @Priority)" SelectCommand="SELECT * FROM [Planner_substitutes] WHERE ([ProductionDate] = @ProductionDate)" UpdateCommand="UPDATE [Planner_substitutes] SET [ProductCode] = @ProductCode, [SubstituteCode] = @SubstituteCode, [ProductionDate] = @ProductionDate, [Reason] = @Reason, [CreatedByUser] = @CreatedByUser, [CreatedDatetime] = @CreatedDatetime, [LastUpdatedDatetime] = @LastUpdatedDatetime, [FinishedProduct] = @FinishedProduct, [Priority] = @Priority WHERE [Id] = @Id">
        <DeleteParameters>
            <asp:Parameter Name="Id" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="ProductCode" Type="String" />
            <asp:Parameter Name="SubstituteCode" Type="String" />
            <asp:Parameter DbType="Date" Name="ProductionDate" />
            <asp:Parameter Name="Reason" Type="String" />
            <asp:Parameter Name="CreatedByUser" Type="String" />
            <asp:Parameter Name="CreatedDatetime" Type="DateTime" />
            <asp:Parameter Name="LastUpdatedDatetime" Type="DateTime" />
            <asp:Parameter Name="FinishedProduct" Type="String" />
            <asp:Parameter Name="Priority" Type="Int32" />
        </InsertParameters>
        <SelectParameters>
            <asp:ControlParameter ControlID="TextBox_ProductionDate" DbType="Date" Name="ProductionDate" PropertyName="Text" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="ProductCode" Type="String" />
            <asp:Parameter Name="SubstituteCode" Type="String" />
            <asp:Parameter DbType="Date" Name="ProductionDate" />
            <asp:Parameter Name="Reason" Type="String" />
            <asp:Parameter Name="CreatedByUser" Type="String" />
            <asp:Parameter Name="CreatedDatetime" Type="DateTime" />
            <asp:Parameter Name="LastUpdatedDatetime" Type="DateTime" />
            <asp:Parameter Name="FinishedProduct" Type="String" />
            <asp:Parameter Name="Priority" Type="Int32" />
            <asp:Parameter Name="Id" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
    <asp:GridView ID="GridView1" runat="server" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="Id" DataSourceID="SqlDataSource1">
        <Columns>
            <asp:CommandField ShowDeleteButton="True" />
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%# String.Format("~/Planner_Substitutes_Item.aspx?ProductionDate={0}&FinishedProduct={1}", Eval("ProductionDate"), Eval("FinishedProduct")) %>'>Edit</asp:HyperLink>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="Id" HeaderText="Id" InsertVisible="False" ReadOnly="True" SortExpression="Id" />
            <asp:BoundField DataField="ProductCode" HeaderText="ProductCode" SortExpression="ProductCode" />
            <asp:BoundField DataField="SubstituteCode" HeaderText="SubstituteCode" SortExpression="SubstituteCode" />
            <asp:BoundField DataField="ProductionDate" HeaderText="ProductionDate" SortExpression="ProductionDate" />
            <asp:BoundField DataField="Reason" HeaderText="Reason" SortExpression="Reason" />
            <asp:BoundField DataField="CreatedByUser" HeaderText="CreatedByUser" SortExpression="CreatedByUser" />
            <asp:BoundField DataField="CreatedDatetime" HeaderText="CreatedDatetime" SortExpression="CreatedDatetime" />
            <asp:BoundField DataField="LastUpdatedDatetime" HeaderText="LastUpdatedDatetime" SortExpression="LastUpdatedDatetime" />
            <asp:BoundField DataField="FinishedProduct" HeaderText="FinishedProduct" SortExpression="FinishedProduct" />
            <asp:BoundField DataField="Priority" HeaderText="Priority" SortExpression="Priority" />
        </Columns>
    </asp:GridView>
    <br />
</asp:Content>

