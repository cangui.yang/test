﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" CodeFile="Planner_Substitutes_Item.aspx.vb" Inherits="Planner_Substitutes_Item" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
<%--    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>--%>
            <table>
        <tr>
            <td>Finished Product</td>
            <td>
                <asp:TextBox ID="TextBox_FinishedProduct" runat="server" ClientIDMode="Static" Columns="50"></asp:TextBox><asp:Button ID="Button_LoadBomProduct" runat="server" Text="Load BOM Product" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator_TextBox_FinishedProduct" runat="server" ControlToValidate="TextBox_FinishedProduct" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                <ajaxToolkit:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server"  behaviorid="TextBox_FinishedProduct_AutoCompleteExtender" completioninterval="100" completionsetcount="10" delimitercharacters="" enablecaching="false" minimumprefixlength="2" servicemethod="SearchProducts" servicepath="" targetcontrolid="TextBox_FinishedProduct"></ajaxToolkit:AutoCompleteExtender>

            </td>
        </tr>
        <tr>
            <td>BOM Product:</td>
            <td>               
                <asp:DropDownList ID="DropDownList1" runat="server" DataSourceID="SqlDataSource_Bom" DataTextField="No_" DataValueField="No_">
                </asp:DropDownList>
                <asp:SqlDataSource ID="SqlDataSource_Bom" runat="server" ConnectionString="<%$ ConnectionStrings:Dynamics2009ConnectionString %>" SelectCommand=""></asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td>Substitute Code:</td>
            <td>
                <asp:TextBox ID="TextBox_SubstituteCode" runat="server" Columns="50"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TextBox_SubstituteCode" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                <ajaxToolkit:AutoCompleteExtender ID="AutoCompleteExtender8" runat="server"  behaviorid="TextBox_SubstituteCode_AutoCompleteExtender" completioninterval="100" completionsetcount="10" delimitercharacters="" enablecaching="false" minimumprefixlength="2" servicemethod="SearchProducts" servicepath="" targetcontrolid="TextBox_SubstituteCode"></ajaxToolkit:AutoCompleteExtender>

            </td>
        </tr>
        <tr>
            <td>Substitute Code1:</td>
            <td>
                <asp:TextBox ID="TextBox_SubstituteCode1" runat="server" Columns="50"></asp:TextBox>
                <autocompleteextender id="AutoCompleteExtender3" runat="server" behaviorid="TextBox_SubstituteCode1_AutoCompleteExtender" completioninterval="100" completionsetcount="10" delimitercharacters="" enablecaching="false" minimumprefixlength="2" servicemethod="SearchProducts" servicepath="" targetcontrolid="TextBox_SubstituteCode1">                  
    </autocompleteextender>
            </td>
        </tr>
        <tr>
            <td>Substitute Code2:</td>
            <td>
                <asp:TextBox ID="TextBox_SubstituteCode2" runat="server" Columns="50"></asp:TextBox>
                <autocompleteextender id="AutoCompleteExtender4" runat="server" behaviorid="TextBox_SubstituteCode2_AutoCompleteExtender" completioninterval="100" completionsetcount="10" delimitercharacters="" enablecaching="false" minimumprefixlength="2" servicemethod="SearchProducts" servicepath="" targetcontrolid="TextBox_SubstituteCode2">                  
    </autocompleteextender>
            </td>
        </tr>
        <tr>
            <td>Substitute Code3:</td>
            <td>
                <asp:TextBox ID="TextBox_SubstituteCode3" runat="server" Columns="50"></asp:TextBox>
                <autocompleteextender id="AutoCompleteExtender5" runat="server" behaviorid="TextBox_SubstituteCode3_AutoCompleteExtender" completioninterval="100" completionsetcount="10" delimitercharacters="" enablecaching="false" minimumprefixlength="2" servicemethod="SearchProducts" servicepath="" targetcontrolid="TextBox_SubstituteCode3">                  
    </autocompleteextender>
            </td>
        </tr>
        <tr>
            <td>Substitute Code4:</td>
            <td>
                <asp:TextBox ID="TextBox_SubstituteCode4" runat="server" Columns="50"></asp:TextBox>
                <autocompleteextender id="AutoCompleteExtender6" runat="server" behaviorid="TextBox_SubstituteCode4_AutoCompleteExtender" completioninterval="100" completionsetcount="10" delimitercharacters="" enablecaching="false" minimumprefixlength="2" servicemethod="SearchProducts" servicepath="" targetcontrolid="TextBox_SubstituteCode4">                  
    </autocompleteextender>
            </td>
        </tr>
        <tr>
            <td colspan="2" style=" font-size:10PX; color:Red ">please note, the sequence of the substitute also is the priority of a substitute in the Finished Product</td>
        </tr>
        <tr>
            <td valign="top">Reason:</td>
            <td rowspan="2">
                <asp:TextBox ID="TextBox_Reasons" runat="server" Columns="50" Rows="5" TextMode="MultiLine"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td>
                <asp:Button ID="Button_Submit" runat="server" Text="Submit" />
            </td>
        </tr>
    </table>
    <%--    </ContentTemplate>
    </asp:UpdatePanel>--%>

</asp:Content>

