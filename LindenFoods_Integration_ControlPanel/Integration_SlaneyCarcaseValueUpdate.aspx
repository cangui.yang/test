﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" CodeFile="Integration_SlaneyCarcaseValueUpdate.aspx.vb" Inherits="Integration_SlaneyCarcaseValueUpdate"  %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    
    <div class="panel panel-Info">
  <div class="panel-heading"><h3 class="panel-title">Integration - Slaney Carcase Value Calculation</h3></div>
  <div class="panel-body">
      <table class="table table-bordered">
          <tr>
              <td style="text-align: justify"><b>Hosting Server:</b></td>
              <td>LF-SQL01\UTILITY1; LF-SQL01\Innova</td>
          </tr>
<%--          <tr>
              <td style="text-align: justify"><b>SSISDB Project:</b></td>
              <td>to be completed</td>
          </tr>
          <tr>
              <td style="text-align: justify"><b>Integration Package:</b></td>
              <td style="text-align: left">
                  <ul>
                      <li>to be completed</li>
                  </ul>
              </td>
          </tr>--%>
          <tr>
              <td style="text-align: justify"><b>SQL Database:</b></td>
              <td>[LF-SQL01\UTILITY1].LindenITWeb; [LF-SQL01\Innova].Innova</td>
          </tr>
          <tr>
              <td style="text-align: justify"><b>Table Ref:</b></td>
              <td>
                  <ul>
                      <li>tbl_SlaneyCarcaseMatrix</li>
                      <li>tbl_SlaneyCarcaseValue</li>
                  </ul>
              </td>
          </tr>
           <tr>
              <td style="text-align: justify"><b>Stored Procedures</b></td>
              <td>
                  <ul>
                      <li>sp_SlaneyCarcaseValueMatrix (LindenITWeb)</li>
                      <li>sp_AutoUpdateSlaneyQtrValue (LindenITWeb)</li>
                      <li>sp_AutoUpdateSlaneyQtrValue (Innova,trigger by Innova Script 64)</li>                 
                  </ul>
               </td>
          </tr>
<%--          <tr>
              <td style="text-align: justify"><b>SQL Server Agent Job:</b></td>
              <td>
                  <ul>
                      <li>to be completed </li>
                  </ul>
              </td>
          </tr>

           <tr>
              <td style="text-align: justify; height: 66px;"><b>Data Source:</b></td>
              <td style="height: 66px">
                  <ul>                    
                      <li></li>
                  </ul>
               </td>
          </tr>--%>
          
          <tr>
              <td><strong>Add New Carcase Value:</strong></td>
              <td>
                  <table style="width: 488px">
                      <tr>
                          <td style="height: 37px; width: 37px">RegFrom:</td>
                          <td style="height: 37px; width: 57px">
                              <asp:TextBox ID="TextBox_DateFrom" runat="server" CausesValidation="True"></asp:TextBox>
                           <ajaxToolkit:CalendarExtender ID="TextBox_DateFrom_CalendarExtender" runat="server" BehaviorID="TextBox_DateFrom_CalendarExtender" Format="yyyy-MM-dd" TargetControlID="TextBox_DateFrom">
                              </ajaxToolkit:CalendarExtender>
                              <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="TextBox_DateFrom" runat="server" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                          </td>
                          <td style="height: 37px; width: 21px">RegTo:</td>
                          <td style="height: 37px; width: 10px">
                              <asp:TextBox ID="TextBox_DateTo" runat="server" CausesValidation="True" ></asp:TextBox>
                              <ajaxToolkit:CalendarExtender ID="TextBox_DateTo_CalendarExtender" runat="server" BehaviorID="TextBox_DateTo_CalendarExtender" Format="yyyy-MM-dd" TargetControlID="TextBox_DateTo">
                              </ajaxToolkit:CalendarExtender>
                              <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="TextBox_DateTo" runat="server" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                          </td>
                          <td style="height: 37px; width: 10px">&nbsp;</td>
                          <td style="width: 144px; height: 37px;">
                              &nbsp;</td>
                      </tr>
                       <tr>
                          <td colspan="6">
                              <asp:GridView ID="GridViewMatrix" runat="server" AllowSorting="True" AutoGenerateColumns="False" DataSourceID="SqlDataSource_Matrix" Width="600px" DataKeyNames="ID" BackColor="White" BorderColor="#E7E7FF" BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Horizontal">
                                  <AlternatingRowStyle BackColor="#F7F7F7" />
                                  <Columns>
                                      <asp:BoundField DataField="Description" HeaderText="Description" SortExpression="Description" />
                                      <asp:BoundField DataField="PriceGroup" HeaderText="PriceGroup" SortExpression="PriceGroup" />
                                      <asp:BoundField DataField="Sex" HeaderText="Sex" SortExpression="Sex" />
                                      <asp:TemplateField HeaderText="U">
                                          <ItemTemplate>
                                              <asp:TextBox ID="TextBox_U" runat="server" Width="50"></asp:TextBox>
                                          </ItemTemplate>
                                      </asp:TemplateField>
                                      <asp:TemplateField HeaderText="R">
                                          <ItemTemplate>
                                              <asp:TextBox ID="TextBox_R" runat="server"  Width="50"></asp:TextBox>
                                          </ItemTemplate>
                                      </asp:TemplateField>
                                      <asp:TemplateField HeaderText="O">
                                          <ItemTemplate>
                                              <asp:TextBox ID="TextBox_O" runat="server"  Width="50"></asp:TextBox>
                                          </ItemTemplate>
                                      </asp:TemplateField>
                                  </Columns>
                                  <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
                                  <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" />
                                  <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
                                  <RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
                                  <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
                                  <SortedAscendingCellStyle BackColor="#F4F4FD" />
                                  <SortedAscendingHeaderStyle BackColor="#5A4C9D" />
                                  <SortedDescendingCellStyle BackColor="#D8D8F0" />
                                  <SortedDescendingHeaderStyle BackColor="#3E3277" />
                              </asp:GridView>
                              <asp:SqlDataSource ID="SqlDataSource_Matrix" runat="server" ConnectionString="<%$ ConnectionStrings:LindenITWebConnectionString %>" SelectCommand="SELECT [ID], [Description], [PriceGroup], [Sex] FROM [tbl_SlaneyCarcaseMatrix] ORDER BY [Sex], [ID]"></asp:SqlDataSource>
                           </td>
                      </tr>
                      <tr>
                          <td colspan="6" style="text-align:center">
                              <asp:Button ID="Button_Submit" runat="server" Text="Submit"/>
                          </td>
                      </tr>
                      </table>

              </td>
          </tr>
          <tr>
              <td style="height: 37px"><strong>Current Carcase Value:</strong></td>
              <td style="height: 37px">
                  <table>
                      <tr><td colspan="3">
                            <asp:GridView ID="GridView_CurrentMatrix" runat="server" AllowSorting="True" AutoGenerateColumns="False" BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" CellPadding="4" DataKeyNames="MatrixId,DateRangeFrom,DateRangeTo" DataSourceID="SqlDataSource_LastSetRecords" ForeColor="Black" GridLines="Vertical" PageSize="8" AllowPaging="True">
                      <AlternatingRowStyle BackColor="White" />
                      <Columns>
                          <asp:BoundField DataField="Description" HeaderText="Description" SortExpression="Description" />
                          <asp:BoundField DataField="PriceGroup" HeaderText="PriceGroup" SortExpression="PriceGroup" />
                          <asp:BoundField DataField="Sex" HeaderText="Sex" SortExpression="Sex" />
                          <asp:BoundField DataField="DateRangeFrom" DataFormatString="{0:dd/MM/yyyy}" HeaderText="DateRangeFrom" ReadOnly="True" SortExpression="DateRangeFrom" />
                          <asp:BoundField DataField="DateRangeTo" DataFormatString="{0:dd/MM/yyyy}" HeaderText="DateRangeTo" ReadOnly="True" SortExpression="DateRangeTo" />
                          <asp:BoundField DataField="U" HeaderText="U" SortExpression="U" />
                          <asp:BoundField DataField="R" HeaderText="R" SortExpression="R" />
                          <asp:BoundField DataField="O" HeaderText="O" SortExpression="O" />
                      </Columns>
       
                      <FooterStyle BackColor="#CCCC99" />
                      <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                      <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                      <RowStyle BackColor="#F7F7DE" />
                      <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                      <SortedAscendingCellStyle BackColor="#FBFBF2" />
                      <SortedAscendingHeaderStyle BackColor="#848384" />
                      <SortedDescendingCellStyle BackColor="#EAEAD3" />
                      <SortedDescendingHeaderStyle BackColor="#575357" />
                  </asp:GridView>
                  <asp:SqlDataSource ID="SqlDataSource_LastSetRecords" runat="server" ConnectionString="<%$ ConnectionStrings:LindenITWebConnectionString %>" SelectCommand="SELECT tbl_SlaneyCarcaseMatrix.Description, tbl_SlaneyCarcaseMatrix.PriceGroup, tbl_SlaneyCarcaseMatrix.Sex, tbl_SlaneyCarcaseValue.MatrixId, tbl_SlaneyCarcaseValue.DateRangeFrom, tbl_SlaneyCarcaseValue.DateRangeTo, tbl_SlaneyCarcaseValue.U, tbl_SlaneyCarcaseValue.R, tbl_SlaneyCarcaseValue.O FROM tbl_SlaneyCarcaseValue INNER JOIN tbl_SlaneyCarcaseMatrix ON tbl_SlaneyCarcaseValue.MatrixId = tbl_SlaneyCarcaseMatrix.ID ORDER BY tbl_SlaneyCarcaseValue.DateRangeFrom DESC, tbl_SlaneyCarcaseMatrix.Sex, tbl_SlaneyCarcaseMatrix.Description"></asp:SqlDataSource>
                          </td></tr>
                      <tr><td class="text-center">
                          <asp:Button ID="Button_Delete" runat="server" Text="Delete Carcase Value" OnClientClick="return confirm('Are you sure you want delete this price set')" />
                          </td><td class="text-center">
                              <asp:Button ID="Button_Apply" runat="server"  Text="Apply Value to Innova" CausesValidation="False" />
<%--                              <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional" >
                                  <ContentTemplate>
                                      
                                       <ajaxToolkit:ConfirmButtonExtender ID="Button_Apply_ConfirmButtonExtender" runat="server" BehaviorID="Button_Apply_ConfirmButtonExtender" ConfirmText="Are you sure you want Apply this price to Innova" TargetControlID="Button_Apply" />
                                       <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                                            <ProgressTemplate>
                                                   Processing...this might take few minutes, Please Wait...                                                                                                                                             
                                            </ProgressTemplate>
                                        </asp:UpdateProgress>
                                   
                                  </ContentTemplate>                               
                              </asp:UpdatePanel>--%>
                                             
                                      
                              </td></tr>
                  </table>
                
                  </td>
          </tr>

          <tr>
              <td style="height: 37px">&nbsp;</td>
              <td style="height: 37px">
                   &nbsp;</td>
          </tr>


      </table>
     </div>
</div>
    <br />
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:LindenITWebConnectionString %>" SelectCommand="
SELECT        tbl_SlaneyCarcaseMatrix.Description, tbl_SlaneyCarcaseMatrix.Sex, tbl_SlaneyCarcaseValue.U, tbl_SlaneyCarcaseValue.R, tbl_SlaneyCarcaseValue.O, tbl_SlaneyCarcaseValue.DateRangeFrom, 
                         tbl_SlaneyCarcaseValue.DateRangeTo
FROM            tbl_SlaneyCarcaseValue RIGHT OUTER JOIN
                         tbl_SlaneyCarcaseMatrix ON tbl_SlaneyCarcaseValue.MatrixId = tbl_SlaneyCarcaseMatrix.ID
"></asp:SqlDataSource>
    
</asp:Content>

