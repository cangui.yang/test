﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" CodeFile="Integration_GranvilleCodeStore.aspx.vb" Inherits="Integration_GranvilleCodeStore" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <div class="panel panel-Info">
  <div class="panel-heading"><h3 class="panel-title">Integration - Import Granville Cold Store Return Pallet</h3></div>
  <div class="panel-body">
      <table class="table table-bordered">
          <tr>
              <td style="text-align: justify"><b>Hosting Server:</b></td>
              <td>LF-SQL01\UTILITY1; LF-SQL01\Innova</td>
          </tr>
<%--          <tr>
              <td style="text-align: justify"><b>SSISDB Project:</b></td>
              <td>to be completed</td>
          </tr>
          <tr>
              <td style="text-align: justify"><b>Integration Package:</b></td>
              <td style="text-align: left">
                  <ul>
                      <li>to be completed</li>
                  </ul>
              </td>
          </tr>--%>
          <tr>
              <td style="text-align: justify"><b>SQL Database:</b></td>
              <td>[LF-SQL01\UTILITY1].LindenBridge; [LF-SQL01\Innova].Innova</td>
          </tr>
          <tr>
              <td style="text-align: justify"><b>Table Ref:</b></td>
              <td>
                  <ul>
                      <li>LindenBridge.dbo.tbl_Granville_ReturnPallets</li>
                  </ul>
              </td>
          </tr>
           <tr>
              <td style="text-align: justify"><b>Stored Procedures</b></td>
              <td>
                  <ul>
                      <li>Innova.dbo.sp_Import_Granville_ReturnPallets</li>
                  </ul>
               </td>
          </tr>
<%--          <tr>
              <td style="text-align: justify"><b>SQL Server Agent Job:</b></td>
              <td>
                  <ul>
                      <li>to be completed </li>
                  </ul>
              </td>
          </tr>

           <tr>
              <td style="text-align: justify; height: 66px;"><b>Data Source:</b></td>
              <td style="height: 66px">
                  <ul>                    
                      <li></li>
                  </ul>
               </td>
          </tr>--%>
          
          <tr>
              <td style="height: 60px"><strong>Serch PalletIDs:</strong></td>
              <td style="height: 60px">
                  <table>
                      <tr>
                          <td>
                                <asp:TextBox ID="TextBox_PalletIDs" runat="server" Width="400px"></asp:TextBox>
                           
                                <ajaxToolkit:TextBoxWatermarkExtender ID="TextBox1_TextBoxWatermarkExtender" runat="server" BehaviorID="TextBox1_TextBoxWatermarkExtender" TargetControlID="TextBox_PalletIDs" WatermarkText ="LN7090261918,LN7090261918">
                                </ajaxToolkit:TextBoxWatermarkExtender>
                          </td>
                          <td>
                              <asp:Button ID="Button_Search" runat="server" Text="Search" /></td>
                      </tr>
                  </table>

              </td>
          </tr>
          <tr>
              <td style="height: 37px"><strong>Resut:</strong></td>
              <td style="height: 37px">
                  <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
                      <ContentTemplate>
                            <asp:GridView ID="GridView1" runat="server" CellPadding="4" DataSourceID="SqlDataSource" ForeColor="#333333" GridLines="None" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="PalletID">
                      <AlternatingRowStyle BackColor="White" />
                      <Columns>
                          <asp:BoundField DataField="PalletID" HeaderText="PalletID" SortExpression="PalletID" />       
                          <asp:BoundField DataField="ImportedDate" HeaderText="ImportedDate" ReadOnly="True" SortExpression="ImportedDate" />
                          <asp:BoundField DataField="SyncToInnova" HeaderText="SyncToInnova" SortExpression="SyncToInnova" />
                          <asp:TemplateField>
                              <ItemTemplate>
                                  <asp:LinkButton ID="LinkButton_Reset" runat="server" OnClick="LinkButton_Reset">Reset</asp:LinkButton>
                                 <ajaxToolkit:ConfirmButtonExtender ID="Button_Apply_ConfirmButtonExtender" runat="server" BehaviorID="Button_Apply_ConfirmButtonExtender" ConfirmText='<%# Eval("PalletID", "Are you sure you want to reset Pallet {0}?") %>' TargetControlID="LinkButton_Reset" />
                              </ItemTemplate>
                          </asp:TemplateField>
                      </Columns>
                      <EditRowStyle BackColor="#7C6F57" />                              
                                <EmptyDataTemplate>
                                    PalletIDs not Founds
                                </EmptyDataTemplate>
                      <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                      <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                      <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                      <RowStyle BackColor="#E3EAEB" />
                      <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                      <SortedAscendingCellStyle BackColor="#F8FAFA" />
                      <SortedAscendingHeaderStyle BackColor="#246B61" />
                      <SortedDescendingCellStyle BackColor="#D4DFE1" />
                      <SortedDescendingHeaderStyle BackColor="#15524A" />
                  </asp:GridView>
                            <asp:SqlDataSource ID="SqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:LindenBridgeConnectionString %>" SelectCommand="SELECT DISTINCT PalletID, SyncToInnova, convert(varchar(10),ImportedDateTime,120) as ImportedDate FROM tbl_Granville_ReturnPallets WHERE (PalletID IN (@PalletID)) ORDER BY PalletID">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="TextBox_PalletIDs" Name="PalletID" PropertyName="Text" Type="String" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                          <asp:UpdateProgress ID="UpdateProgress1" runat="server"><ProgressTemplate>This may take up to 3 minutes to reprocess the Pallet in Innova</ProgressTemplate> </asp:UpdateProgress>
                        </ContentTemplate>               
                    </asp:UpdatePanel>
                
                
                
                  </td>
          </tr>

          <tr>
              <td style="height: 37px">&nbsp;</td>
              <td style="height: 37px">
                   &nbsp;</td>
          </tr>


      </table>
     </div>
</div>
</asp:Content>

