using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Server;
using System.Net;
using SQLIntegrationClassLibrary;

public partial class Integration_ToPrimalSO
{
    [Microsoft.SqlServer.Server.SqlProcedure]
    public static void Assembies_ToPrimalSO_UpdateSaleOrderLine(string domain, string username, string password, string SalesOrderNo , string ProductCode
                                            , string UOMC , decimal Quanlity, decimal ConsignedQuantity
                                            , decimal QtytoSihp, decimal QtytoInvoice,string LocationCode, out string Result )
    {

       
            Sync2PrimalSO sync2SO = new Sync2PrimalSO();
            Result = sync2SO.UpdateSaleOrderLine(domain, username, password, SalesOrderNo, ProductCode, UOMC, Quanlity, ConsignedQuantity
                                            , QtytoSihp, QtytoInvoice, LocationCode);
        //if (Result == string.Empty)
        //{
        //    Result = "Success";
        //} 
       
    }

    [Microsoft.SqlServer.Server.SqlProcedure]
    public static void Assembies_ToPrimalSO_CreateSaleOrderLine(string domain, string username, string password, string SalesOrderNo, string ProductCode
                                            , string UOMC, decimal Quanlity, decimal ConsignedQuantity
                                            , decimal QtytoSihp, decimal QtytoInvoice, string LocationCode, out string Result)
    {

       // string result = string.Empty;
        Sync2PrimalSO sync2SO = new Sync2PrimalSO();
        Result = sync2SO.CreateOrderLine(domain, username, password, SalesOrderNo, ProductCode, UOMC, Quanlity, ConsignedQuantity
                                        , QtytoSihp, QtytoInvoice, LocationCode);
        //if (Result == string.Empty)
        //{
        //    Result = "Success";
        //}
    }
}
